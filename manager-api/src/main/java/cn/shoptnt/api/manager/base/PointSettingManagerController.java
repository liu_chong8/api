/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.base;

import cn.shoptnt.handler.AdminTwoStepAuthentication;
import cn.shoptnt.model.base.SettingGroup;
import cn.shoptnt.client.system.SettingClient;
import cn.shoptnt.model.support.LogClient;
import cn.shoptnt.model.support.validator.annotation.Log;
import cn.shoptnt.model.support.validator.annotation.LogLevel;
import cn.shoptnt.model.system.vo.PointSetting;
import cn.shoptnt.framework.util.JsonUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 积分设置api
 *
 * @author zh
 * @version v7.0
 * @date 18/5/18 下午6:55
 * @since v7.0
 */
@RestController
@RequestMapping("/admin/settings")
@Api(tags= "积分设置")
@Validated
public class PointSettingManagerController {
    @Autowired
    private SettingClient settingClient;

    @Autowired
    private AdminTwoStepAuthentication adminTwoStepAuthentication;

    @GetMapping(value = "/point")
    @ApiOperation(value = "获取积分设置", response = PointSetting.class)
    public PointSetting getPointSetting() {
        String pointSettingJson = settingClient.get( SettingGroup.POINT);
        PointSetting pointSetting = JsonUtil.jsonToObject(pointSettingJson,PointSetting.class);
        if (pointSetting == null) {
            return new PointSetting();
        }
        return pointSetting;
    }

    @PutMapping(value = "/point")
    @ApiOperation(value = "修改积分设置", response = PointSetting.class)
    @Log(client = LogClient.admin,detail = "修改积分设置参数",level = LogLevel.important)
    public PointSetting editPointSetting(@Valid PointSetting pointSetting) {
        adminTwoStepAuthentication.sensitive();
        settingClient.save(SettingGroup.POINT, pointSetting);
        return pointSetting;
    }

}
