/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.deploy;

import cn.shoptnt.framework.elasticsearch.ElasticJestConfig;
import cn.shoptnt.framework.swagger.SwaggerConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;


/**
 * shoptnt 安装器工程
 *
 * @author 妙贤
 * @version v1.0.0
 * @since v7.0.0
 * 2018年1月22日 上午10:01:32
 * "cn.shoptnt.framework.database," +
 */
@SpringBootApplication
@ComponentScan(basePackages = {
        "cn.shoptnt.framework.database.impl",
        "cn.shoptnt.framework.sncreator",
        "cn.shoptnt.framework.context",
        "cn.shoptnt.framework.exception",
        "cn.shoptnt.framework.redis.configure.builders",
        "cn.shoptnt.framework.elasticsearch",
        "cn.shoptnt.framework.swagger",
        "cn.shoptnt.deploy"},
        excludeFilters = {@ComponentScan.Filter(type=FilterType.ASSIGNABLE_TYPE,value = {ElasticJestConfig.class, SwaggerConfiguration.class})})
public class ShopTntDeployApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        System.setProperty("es.set.netty.runtime.available.processors", "false");
        SpringApplication.run(ShopTntDeployApplication.class, args);

    }
}
