package cn.shoptnt.consumer.core.receiver;

import cn.shoptnt.consumer.core.event.ShopRegisterEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import cn.shoptnt.model.base.rabbitmq.AmqpExchange;
import cn.shoptnt.model.shop.vo.ShopRegisterMsg;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 商家入驻消息接收类
 * @author dmy
 * @version 1.0
 * @since 7.2.3
 * 2022-05-07
 */
@Component
public class ShopRegisterReceiver {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<ShopRegisterEvent> events;

    /**
     * 商家入驻
     *
     * @param shopRegisterMsg
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.SHOP_REGISTER + "_ROUTING"),
            exchange = @Exchange(value = AmqpExchange.SHOP_REGISTER, type = ExchangeTypes.FANOUT)
    ))
    public void shopChange(ShopRegisterMsg shopRegisterMsg) {

        if (events != null) {
            for (ShopRegisterEvent event : events) {
                try {
                    event.shopRegister(shopRegisterMsg);
                } catch (Exception e) {
                    logger.error("处理商家入驻消息出错", e);
                }
            }
        }
    }
}
