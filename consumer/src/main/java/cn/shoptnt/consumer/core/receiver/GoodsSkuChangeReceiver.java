/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.receiver;

import cn.shoptnt.consumer.core.event.GoodsSkuChangeEvent;
import cn.shoptnt.model.base.rabbitmq.AmqpExchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 商品sku变化消费者
 *
 * @author fk
 * @version v2.0
 * @since v7.2.0 2020年2月7日 上午10:29:54
 */
@Component
public class GoodsSkuChangeReceiver {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired(required = false)
	private List<GoodsSkuChangeEvent> events;

	/**
	 * 商品变化
	 *
	 * @param delSkuIds
	 */
	@RabbitListener(bindings = @QueueBinding(
			value = @Queue(value = AmqpExchange.GOODS_SKU_CHANGE + "_QUEUE"),
			exchange = @Exchange(value = AmqpExchange.GOODS_SKU_CHANGE, type = ExchangeTypes.FANOUT)
	))
	public void goodsSkuChange(List<Long> delSkuIds) {

		if (events != null) {
			for (GoodsSkuChangeEvent event : events) {
				try {
					event.goodsSkuChange(delSkuIds);
				} catch (Exception e) {
					logger.error("处理商品sku变化消息出错", e);
				}
			}
		}

	}
}
