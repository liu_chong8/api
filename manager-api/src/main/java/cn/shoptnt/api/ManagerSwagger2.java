/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api;

import cn.shoptnt.framework.swagger.SwaggerConfiguration;
import cn.shoptnt.framework.swagger.SwaggerProperties;
import cn.shoptnt.model.base.context.Region;
import cn.shoptnt.framework.swagger.AbstractSwagger2;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 妙贤 on 2018/3/10.
 *
 * @author 妙贤
 * @version 1.0
 * @since 7.0.0
 * 2018/3/10
 */

@Configuration
@EnableOpenApi
public class ManagerSwagger2 extends SwaggerConfiguration {

    public ManagerSwagger2(SwaggerProperties swaggerProperties) {
        super(swaggerProperties);
    }

    @Bean
    public Docket distributionApi() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("分销")
                // 用于生成API信息
                .apiInfo(apiInfo())

                // 授权信息设置，必要的header token等认证信息
                .securitySchemes(securitySchemes())
                // 授权信息全局应用
                .securityContexts(securityContexts())
                // select()函数返回一个ApiSelectorBuilder实例,用来控制接口被swagger做成文档
                .select()
                // 用于指定扫描哪个包下的接口
                .apis(RequestHandlerSelectors.basePackage("cn.shoptnt.api.manager"))
                // 选择所有的API,如果你想只为部分API生成文档，可以配置这里
                .paths(PathSelectors.ant("/admin/distribution/**"))

                .build();
    }


    @Bean
    public Docket baseApi() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("基础")
                // 用于生成API信息
                .apiInfo(apiInfo())

                // 授权信息设置，必要的header token等认证信息
                .securitySchemes(securitySchemes())
                // 授权信息全局应用
                .securityContexts(securityContexts())
                // select()函数返回一个ApiSelectorBuilder实例,用来控制接口被swagger做成文档
                .select()
                // 用于指定扫描哪个包下的接口
                .apis(RequestHandlerSelectors.basePackage("cn.shoptnt.api.manager"))
                // 选择所有的API,如果你想只为部分API生成文档，可以配置这里
                .paths(PathSelectors.ant("/admin/index/**"))

                .build();
    }

    @Bean
    public Docket afterSaleApi() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("售后")
                // 用于生成API信息
                .apiInfo(apiInfo())

                // 授权信息设置，必要的header token等认证信息
                .securitySchemes(securitySchemes())
                // 授权信息全局应用
                .securityContexts(securityContexts())
                // select()函数返回一个ApiSelectorBuilder实例,用来控制接口被swagger做成文档
                .select()
                // 用于指定扫描哪个包下的接口
                .apis(RequestHandlerSelectors.basePackage("cn.shoptnt.api.manager"))
                // 选择所有的API,如果你想只为部分API生成文档，可以配置这里
                .paths(PathSelectors.ant("/admin/after-sales/**"))

                .build();
    }

    @Bean
    public Docket goodsApi() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("商品")
                // 用于生成API信息
                .apiInfo(apiInfo())

                // 授权信息设置，必要的header token等认证信息
                .securitySchemes(securitySchemes())
                // 授权信息全局应用
                .securityContexts(securityContexts())
                // select()函数返回一个ApiSelectorBuilder实例,用来控制接口被swagger做成文档
                .select()
                // 用于指定扫描哪个包下的接口
                .apis(RequestHandlerSelectors.basePackage("cn.shoptnt.api.manager"))
                // 选择所有的API,如果你想只为部分API生成文档，可以配置这里
                .paths(PathSelectors.ant("/admin/goods/**"))

                .build();

    }

    @Bean
    public Docket goodsSearchApi() {

        return new Docket(DocumentationType.OAS_30)
                .groupName("商品搜索设置")
                // 用于生成API信息
                .apiInfo(apiInfo())

                // 授权信息设置，必要的header token等认证信息
                .securitySchemes(securitySchemes())
                // 授权信息全局应用
                .securityContexts(securityContexts())
                // select()函数返回一个ApiSelectorBuilder实例,用来控制接口被swagger做成文档
                .select()
                // 用于指定扫描哪个包下的接口
                .apis(RequestHandlerSelectors.basePackage("cn.shoptnt.api.manager"))
                // 选择所有的API,如果你想只为部分API生成文档，可以配置这里
                .paths(PathSelectors.ant("/admin/goodssearch/**"))

                .build();
    }

    @Bean
    public Docket membersApi() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("会员")
                // 用于生成API信息
                .apiInfo(apiInfo())

                // 授权信息设置，必要的header token等认证信息
                .securitySchemes(securitySchemes())
                // 授权信息全局应用
                .securityContexts(securityContexts())
                // select()函数返回一个ApiSelectorBuilder实例,用来控制接口被swagger做成文档
                .select()
                // 用于指定扫描哪个包下的接口
                .apis(RequestHandlerSelectors.basePackage("cn.shoptnt.api.manager"))
                // 选择所有的API,如果你想只为部分API生成文档，可以配置这里
                .paths(PathSelectors.ant("/admin/members/**"))

                .build();
    }

    @Bean
    public Docket orderBillApi() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("结算")
                // 用于生成API信息
                .apiInfo(apiInfo())

                // 授权信息设置，必要的header token等认证信息
                .securitySchemes(securitySchemes())
                // 授权信息全局应用
                .securityContexts(securityContexts())
                // select()函数返回一个ApiSelectorBuilder实例,用来控制接口被swagger做成文档
                .select()
                // 用于指定扫描哪个包下的接口
                .apis(RequestHandlerSelectors.basePackage("cn.shoptnt.api.manager"))
                // 选择所有的API,如果你想只为部分API生成文档，可以配置这里
                .paths(PathSelectors.ant("/admin/order/**"))

                .build();
    }

    @Bean
    public Docket pageCreateApi() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("静态页")
                // 用于生成API信息
                .apiInfo(apiInfo())

                // 授权信息设置，必要的header token等认证信息
                .securitySchemes(securitySchemes())
                // 授权信息全局应用
                .securityContexts(securityContexts())
                // select()函数返回一个ApiSelectorBuilder实例,用来控制接口被swagger做成文档
                .select()
                // 用于指定扫描哪个包下的接口
                .apis(RequestHandlerSelectors.basePackage("cn.shoptnt.api.manager"))
                // 选择所有的API,如果你想只为部分API生成文档，可以配置这里
                .paths(PathSelectors.ant("/admin/page-create/**"))

                .build();
    }

    @Bean
    public Docket pageDataApi() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("楼层")
                // 用于生成API信息
                .apiInfo(apiInfo())

                // 授权信息设置，必要的header token等认证信息
                .securitySchemes(securitySchemes())
                // 授权信息全局应用
                .securityContexts(securityContexts())
                // select()函数返回一个ApiSelectorBuilder实例,用来控制接口被swagger做成文档
                .select()
                // 用于指定扫描哪个包下的接口
                .apis(RequestHandlerSelectors.basePackage("cn.shoptnt.api.manager"))
                // 选择所有的API,如果你想只为部分API生成文档，可以配置这里
                .paths(PathSelectors.regex("(/admin/focus-pictures.*)|(/admin/pages.*)"))

                .build();
    }

    @Bean
    public Docket paymentApi() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("支付")
                // 用于生成API信息
                .apiInfo(apiInfo())

                // 授权信息设置，必要的header token等认证信息
                .securitySchemes(securitySchemes())
                // 授权信息全局应用
                .securityContexts(securityContexts())
                // select()函数返回一个ApiSelectorBuilder实例,用来控制接口被swagger做成文档
                .select()
                // 用于指定扫描哪个包下的接口
                .apis(RequestHandlerSelectors.basePackage("cn.shoptnt.api.manager"))
                // 选择所有的API,如果你想只为部分API生成文档，可以配置这里
                .paths(PathSelectors.ant("/admin/payment/**"))

                .build();
    }

    @Bean
    public Docket promotionApi() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("促销")
                // 用于生成API信息
                .apiInfo(apiInfo())

                // 授权信息设置，必要的header token等认证信息
                .securitySchemes(securitySchemes())
                // 授权信息全局应用
                .securityContexts(securityContexts())
                // select()函数返回一个ApiSelectorBuilder实例,用来控制接口被swagger做成文档
                .select()
                // 用于指定扫描哪个包下的接口
                .apis(RequestHandlerSelectors.basePackage("cn.shoptnt.api.manager"))
                // 选择所有的API,如果你想只为部分API生成文档，可以配置这里
                .paths(PathSelectors.ant("/admin/promotion/**"))

                .build();
    }

    @Bean
    public Docket shopApi() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("店铺")
                // 用于生成API信息
                .apiInfo(apiInfo())

                // 授权信息设置，必要的header token等认证信息
                .securitySchemes(securitySchemes())
                // 授权信息全局应用
                .securityContexts(securityContexts())
                // select()函数返回一个ApiSelectorBuilder实例,用来控制接口被swagger做成文档
                .select()
                // 用于指定扫描哪个包下的接口
                .apis(RequestHandlerSelectors.basePackage("cn.shoptnt.api.manager"))
                // 选择所有的API,如果你想只为部分API生成文档，可以配置这里
                .paths(PathSelectors.ant("/admin/shops/**"))

                .build();
    }

    @Bean
    public Docket statisticsApi() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("统计")
                // 用于生成API信息
                .apiInfo(apiInfo())

                // 授权信息设置，必要的header token等认证信息
                .securitySchemes(securitySchemes())
                // 授权信息全局应用
                .securityContexts(securityContexts())
                // select()函数返回一个ApiSelectorBuilder实例,用来控制接口被swagger做成文档
                .select()
                // 用于指定扫描哪个包下的接口
                .apis(RequestHandlerSelectors.basePackage("cn.shoptnt.api.manager"))
                // 选择所有的API,如果你想只为部分API生成文档，可以配置这里
                .paths(PathSelectors.regex("(/admin/statistics.*)|(/admin/index/page.*)|(/admin/services.*)"))

                .build();
    }

    @Bean
    public Docket systemApi() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("系统设置")
                // 用于生成API信息
                .apiInfo(apiInfo())

                // 授权信息设置，必要的header token等认证信息
                .securitySchemes(securitySchemes())
                // 授权信息全局应用
                .securityContexts(securityContexts())
                // select()函数返回一个ApiSelectorBuilder实例,用来控制接口被swagger做成文档
                .select()
                // 用于指定扫描哪个包下的接口
                .apis(RequestHandlerSelectors.basePackage("cn.shoptnt.api.manager"))
                // 选择所有的API,如果你想只为部分API生成文档，可以配置这里
                .paths(PathSelectors.regex("(/admin/task.*)|(/admin/systems.*)|(/admin/settings.*)|(/admin/sensitive-words.*)|(/admin/system-logs.*)"))

                .build();
    }

    @Bean
    public Docket tradeApi() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("交易")
                // 用于生成API信息
                .apiInfo(apiInfo())

                // 授权信息设置，必要的header token等认证信息
                .securitySchemes(securitySchemes())
                // 授权信息全局应用
                .securityContexts(securityContexts())
                // select()函数返回一个ApiSelectorBuilder实例,用来控制接口被swagger做成文档
                .select()
                // 用于指定扫描哪个包下的接口
                .apis(RequestHandlerSelectors.basePackage("cn.shoptnt.api.manager"))
                // 选择所有的API,如果你想只为部分API生成文档，可以配置这里
                .paths(PathSelectors.ant("/admin/trade/**"))

                .build();
    }

    @Bean
    public Docket passportApi() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("安全")
                // 用于生成API信息
                .apiInfo(apiInfo())

                // 授权信息设置，必要的header token等认证信息
                .securitySchemes(securitySchemes())
                // 授权信息全局应用
                .securityContexts(securityContexts())
                // select()函数返回一个ApiSelectorBuilder实例,用来控制接口被swagger做成文档
                .select()
                // 用于指定扫描哪个包下的接口
                .apis(RequestHandlerSelectors.basePackage("cn.shoptnt.api.manager"))
                // 选择所有的API,如果你想只为部分API生成文档，可以配置这里
                .paths(PathSelectors.ant("/admin/passport/**"))

                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("管理中心Api文档")
                .description("管理中心API接口")
                .version("7.3.0")
                .contact(new Contact("ShopTnt", "http://www.shoptnt.cn", "service@shoptnt.cn"))
                .build();
    }
}
