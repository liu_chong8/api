/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api;

import cn.shoptnt.framework.swagger.SwaggerConfiguration;
import cn.shoptnt.framework.swagger.SwaggerProperties;
import cn.shoptnt.model.base.context.Region;
import cn.shoptnt.framework.swagger.AbstractSwagger2;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Created by 妙贤 on 2018/3/10.
 * Swagger2配置
 *
 * @author 妙贤
 * @version 1.0
 * @since 7.0.0
 * 2018/3/10
 */

@Configuration
@EnableOpenApi
public class BuyerSwagger2 extends SwaggerConfiguration {

    public BuyerSwagger2(SwaggerProperties swaggerProperties) {
        super(swaggerProperties);
    }

    @Bean
    public Docket wechat() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("微信")
                // 用于生成API信息
                .apiInfo(apiInfo())

                // 授权信息设置，必要的header token等认证信息
                .securitySchemes(securitySchemes())
                // 授权信息全局应用
                .securityContexts(securityContexts())
                // select()函数返回一个ApiSelectorBuilder实例,用来控制接口被swagger做成文档
                .select()
                // 用于指定扫描哪个包下的接口
                .apis(RequestHandlerSelectors.basePackage("cn.shoptnt.api.buyer"))
                // 选择所有的API,如果你想只为部分API生成文档，可以配置这里
                .paths(PathSelectors.ant("/wechat/**"))

                .build();
    }


    @Bean
    public Docket distributionApi() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("分销")
                // 用于生成API信息
                .apiInfo(apiInfo())

                // 授权信息设置，必要的header token等认证信息
                .securitySchemes(securitySchemes())
                // 授权信息全局应用
                .securityContexts(securityContexts())
                // select()函数返回一个ApiSelectorBuilder实例,用来控制接口被swagger做成文档
                .select()
                // 用于指定扫描哪个包下的接口
                .apis(RequestHandlerSelectors.basePackage("cn.shoptnt.api.buyer"))
                // 选择所有的API,如果你想只为部分API生成文档，可以配置这里
                .paths(PathSelectors.ant("/distribution/**"))

                .build();
    }


    @Bean
    public Docket pintuanApi() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("拼团")
                // 用于生成API信息
                .apiInfo(apiInfo())

                // 授权信息设置，必要的header token等认证信息
                .securitySchemes(securitySchemes())
                // 授权信息全局应用
                .securityContexts(securityContexts())
                // select()函数返回一个ApiSelectorBuilder实例,用来控制接口被swagger做成文档
                .select()
                // 用于指定扫描哪个包下的接口
                .apis(RequestHandlerSelectors.basePackage("cn.shoptnt.api.buyer"))
                // 选择所有的API,如果你想只为部分API生成文档，可以配置这里
                .paths(PathSelectors.ant("/pintuan/**"))

                .build();
    }


    @Bean
    public Docket afterSaleApi() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("售后")
                // 用于生成API信息
                .apiInfo(apiInfo())

                // 授权信息设置，必要的header token等认证信息
                .securitySchemes(securitySchemes())
                // 授权信息全局应用
                .securityContexts(securityContexts())
                // select()函数返回一个ApiSelectorBuilder实例,用来控制接口被swagger做成文档
                .select()
                // 用于指定扫描哪个包下的接口
                .apis(RequestHandlerSelectors.basePackage("cn.shoptnt.api.buyer"))
                // 选择所有的API,如果你想只为部分API生成文档，可以配置这里
                .paths(PathSelectors.regex("(/after-sales.*)|(/buyer/after-sales.*)"))

                .build();
    }
    @Bean
    public Docket sssApi() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("流量")
                // 用于生成API信息
                .apiInfo(apiInfo())

                // 授权信息设置，必要的header token等认证信息
                .securitySchemes(securitySchemes())
                // 授权信息全局应用
                .securityContexts(securityContexts())
                // select()函数返回一个ApiSelectorBuilder实例,用来控制接口被swagger做成文档
                .select()
                // 用于指定扫描哪个包下的接口
                .apis(RequestHandlerSelectors.basePackage("cn.shoptnt.api.buyer"))
                // 选择所有的API,如果你想只为部分API生成文档，可以配置这里
                .paths(PathSelectors.ant("/view/**"))

                .build();
    }

    @Bean
    public Docket goodsApi() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("商品")
                // 用于生成API信息
                .apiInfo(apiInfo())

                // 授权信息设置，必要的header token等认证信息
                .securitySchemes(securitySchemes())
                // 授权信息全局应用
                .securityContexts(securityContexts())
                // select()函数返回一个ApiSelectorBuilder实例,用来控制接口被swagger做成文档
                .select()
                // 用于指定扫描哪个包下的接口
                .apis(RequestHandlerSelectors.basePackage("cn.shoptnt.api.buyer"))
                // 选择所有的API,如果你想只为部分API生成文档，可以配置这里
                .paths(PathSelectors.ant("/goods/**"))

                .build();
    }


    @Bean
    public Docket passportApi() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("会员认证中心")
                // 用于生成API信息
                .apiInfo(apiInfo())

                // 授权信息设置，必要的header token等认证信息
                .securitySchemes(securitySchemes())
                // 授权信息全局应用
                .securityContexts(securityContexts())
                // select()函数返回一个ApiSelectorBuilder实例,用来控制接口被swagger做成文档
                .select()
                // 用于指定扫描哪个包下的接口
                .apis(RequestHandlerSelectors.basePackage("cn.shoptnt.api.buyer"))
                // 选择所有的API,如果你想只为部分API生成文档，可以配置这里
                .paths(PathSelectors.regex("(/passport/.*)"))

                .build();
    }

    @Bean
    public Docket memberApi() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("会员")
                // 用于生成API信息
                .apiInfo(apiInfo())

                // 授权信息设置，必要的header token等认证信息
                .securitySchemes(securitySchemes())
                // 授权信息全局应用
                .securityContexts(securityContexts())
                // select()函数返回一个ApiSelectorBuilder实例,用来控制接口被swagger做成文档
                .select()
                // 用于指定扫描哪个包下的接口
                .apis(RequestHandlerSelectors.basePackage("cn.shoptnt.api.buyer"))
                // 选择所有的API,如果你想只为部分API生成文档，可以配置这里
                .paths(PathSelectors.regex("(/members.*)|(/account-binder.*)"))

                .build();
    }

    @Bean
    public Docket pageDataApi() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("楼层")
                // 用于生成API信息
                .apiInfo(apiInfo())

                // 授权信息设置，必要的header token等认证信息
                .securitySchemes(securitySchemes())
                // 授权信息全局应用
                .securityContexts(securityContexts())
                // select()函数返回一个ApiSelectorBuilder实例,用来控制接口被swagger做成文档
                .select()
                // 用于指定扫描哪个包下的接口
                .apis(RequestHandlerSelectors.basePackage("cn.shoptnt.api.buyer"))
                // 选择所有的API,如果你想只为部分API生成文档，可以配置这里
                .paths(PathSelectors.regex("(/pages/.*)|(/focus-pictures.*)"))

                .build();
    }

    @Bean
    public Docket paymentApi() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("支付")
                // 用于生成API信息
                .apiInfo(apiInfo())

                // 授权信息设置，必要的header token等认证信息
                .securitySchemes(securitySchemes())
                // 授权信息全局应用
                .securityContexts(securityContexts())
                // select()函数返回一个ApiSelectorBuilder实例,用来控制接口被swagger做成文档
                .select()
                // 用于指定扫描哪个包下的接口
                .apis(RequestHandlerSelectors.basePackage("cn.shoptnt.api.buyer"))
                // 选择所有的API,如果你想只为部分API生成文档，可以配置这里
                .paths(PathSelectors.regex("(/order/.*)|(/balance.*)|(/recharge.*)"))

                .build();
    }

    @Bean
    public Docket shopApi() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("店铺")
                // 用于生成API信息
                .apiInfo(apiInfo())

                // 授权信息设置，必要的header token等认证信息
                .securitySchemes(securitySchemes())
                // 授权信息全局应用
                .securityContexts(securityContexts())
                // select()函数返回一个ApiSelectorBuilder实例,用来控制接口被swagger做成文档
                .select()
                // 用于指定扫描哪个包下的接口
                .apis(RequestHandlerSelectors.basePackage("cn.shoptnt.api.buyer"))
                // 选择所有的API,如果你想只为部分API生成文档，可以配置这里
                .paths(PathSelectors.ant("/shops/**"))

                .build();
    }

    @Bean
    public Docket tradeApi() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("交易")
                // 用于生成API信息
                .apiInfo(apiInfo())

                // 授权信息设置，必要的header token等认证信息
                .securitySchemes(securitySchemes())
                // 授权信息全局应用
                .securityContexts(securityContexts())
                // select()函数返回一个ApiSelectorBuilder实例,用来控制接口被swagger做成文档
                .select()
                // 用于指定扫描哪个包下的接口
                .apis(RequestHandlerSelectors.basePackage("cn.shoptnt.api.buyer"))
                // 选择所有的API,如果你想只为部分API生成文档，可以配置这里
                .paths(PathSelectors.regex("(/trade.*)|(/express.*)"))

                .build();
    }

    @Bean
    public Docket promotionApi() {
        return new Docket(DocumentationType.OAS_30)
                .groupName("促销")
                // 用于生成API信息
                .apiInfo(apiInfo())

                // 授权信息设置，必要的header token等认证信息
                .securitySchemes(securitySchemes())
                // 授权信息全局应用
                .securityContexts(securityContexts())
                // select()函数返回一个ApiSelectorBuilder实例,用来控制接口被swagger做成文档
                .select()
                // 用于指定扫描哪个包下的接口
                .apis(RequestHandlerSelectors.basePackage("cn.shoptnt.api.buyer"))
                // 选择所有的API,如果你想只为部分API生成文档，可以配置这里
                .paths(PathSelectors.ant("/promotions/**"))

                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("买家Api文档")
                .description("买家中心API接口")
                .version("7.3.0")
                .contact(new Contact("ShopTnt", "http://www.shoptnt.cn", "service@shoptnt.cn"))
                .build();
    }
}
