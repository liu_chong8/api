/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.receiver;

import cn.shoptnt.consumer.core.event.PaymentBillPaymentMethodChangeEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import cn.shoptnt.model.base.message.PaymentBillPaymentMethodChangeMsg;
import cn.shoptnt.model.base.rabbitmq.AmqpExchange;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 支付账单支付方式改变消费者
 *
 * @author zh
 * @version v2.0
 * @since v7.2.1 2020年3月11日 上午10:31:42
 */
@Component
public class PaymentBillPaymentMethodChangeReceiver {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<PaymentBillPaymentMethodChangeEvent> events;

    /**
     * 订单状态改变
     *
     * @param paymentBillPaymentMethodChangeMsg
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.PAYMENT_BILL_PAYMENT_METHOD_CHANGE + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.PAYMENT_BILL_PAYMENT_METHOD_CHANGE,
                    type = ExchangeTypes.FANOUT)
    ))
    public void billPaymentMethodChange(PaymentBillPaymentMethodChangeMsg paymentBillPaymentMethodChangeMsg) {
        if (events != null) {
            for (PaymentBillPaymentMethodChangeEvent event : events) {
                try {
                    event.billPaymentMethodChange(paymentBillPaymentMethodChangeMsg);
                } catch (Exception e) {
                    logger.error("支付账单支付方式改变消息出错", e);
                }
            }
        }

    }

}
