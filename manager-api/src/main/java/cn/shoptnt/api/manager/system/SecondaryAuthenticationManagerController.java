/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.system;

import cn.shoptnt.framework.context.user.AdminUserContext;
import cn.shoptnt.framework.security.model.Admin;
import cn.shoptnt.model.system.dos.AuthenticationDO;
import cn.shoptnt.service.system.SecondaryAuthenticationManager;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


import javax.validation.Valid;


/**
 * 二次身份验证控制器
 *
 * @author shenyanwu
 * @version v2.0
 * @since v2.0
 * 2021-11-19 18:45:02
 */
@RestController
@RequestMapping("/admin/systems/authentications")
@Api(tags= "二次身份验证相关API")
public class SecondaryAuthenticationManagerController {

    @Autowired
    private SecondaryAuthenticationManager secondaryAuthenticationManager;


    @ApiOperation(value = "添加二次身份验证", response = AuthenticationDO.class)
    @PostMapping
    public AuthenticationDO add(@Valid AuthenticationDO authentication) {
        Admin admin = AdminUserContext.getAdmin();
        authentication.setAdminId(admin.getUid());
        this.secondaryAuthenticationManager.add(authentication);

        return authentication;
    }

    @PutMapping()
    @ApiOperation(value = "修改二次身份验证", response = AuthenticationDO.class)

    public AuthenticationDO edit(@Valid AuthenticationDO authentication) {
        this.secondaryAuthenticationManager.edit(authentication);

        return authentication;
    }


    @DeleteMapping()
    @ApiOperation(value = "删除二次身份验证")

    public String delete() {
        Admin admin = AdminUserContext.getAdmin();
        this.secondaryAuthenticationManager.delete(admin.getUid());

        return "";
    }


    @GetMapping()
    @ApiOperation(value = "查询一个二次身份验证")
    public AuthenticationDO get() {
        Admin admin = AdminUserContext.getAdmin();

        AuthenticationDO authentication = this.secondaryAuthenticationManager.getModelByUid(admin.getUid());
        return authentication;


    }


}