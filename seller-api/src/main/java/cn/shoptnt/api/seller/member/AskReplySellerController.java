/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.member;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.member.dos.AskReplyDO;
import cn.shoptnt.model.member.dto.ReplyQueryParam;
import cn.shoptnt.model.member.enums.AuditEnum;
import cn.shoptnt.service.member.AskReplyManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 会员商品咨询回复API
 *
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-09-17
 */
@RestController
@RequestMapping("/seller/members/reply")
@Api(tags= "会员商品咨询回复API")
@Validated
public class AskReplySellerController {

    @Autowired
    private AskReplyManager askReplyManager;

    @ApiOperation(value = "查询会员商品咨询回复列表", response = AskReplyDO.class)
    @GetMapping
    public WebPage list(@Valid ReplyQueryParam param) {
        //审核状态 WAIT_AUDIT:待审核,PASS_AUDIT:审核通过,REFUSE_AUDIT:审核未通过
        param.setAuthStatus(AuditEnum.PASS_AUDIT.value());

        return this.askReplyManager.list(param);
    }
}
