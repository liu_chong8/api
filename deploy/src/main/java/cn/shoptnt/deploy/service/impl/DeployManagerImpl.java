/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.deploy.service.impl;

import cn.shoptnt.deploy.model.Deploy;
import cn.shoptnt.deploy.service.*;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.util.DateUtil;
import cn.shoptnt.framework.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.shoptnt.framework.database.DaoSupport;


/**
 * 部署业务类
 * @author admin
 * @version v1.0
 * @since v1.0
 * 2018-04-23 14:27:13
 */
@Service
public class DeployManagerImpl implements DeployManager {

	@Autowired
	private	DaoSupport	daoSupport;

	@Autowired
	private DatabaseManager databaseManager;

	@Autowired
	private RedisManager redisManager;

	@Autowired
	private RabbitmqManager rabbitmqManager;

	@Autowired
	private ElasticsearchManager elasticsearchManager;

	@Override
	public WebPage list(long page, long pageSize){

		String sql = "select * from es_deploy order by deploy_id desc ";
		WebPage webPage = this.daoSupport.queryForPage(sql,page, pageSize ,Deploy.class );

		return webPage;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED,rollbackFor=Exception.class)
	public	Deploy  add(Deploy	deploy)	{

		deploy.setCreateTime(DateUtil.getDateline());
		this.daoSupport.insert(deploy);
		Integer deployId = this.daoSupport.queryForInt("select max(deploy_id) from es_deploy");

		//初始化数据库设置
		databaseManager.initDatabase(deployId);

		//初始化elasticsearch
		elasticsearchManager.initElasticsearch(deployId);

		redisManager.initRedis(deployId);
		deploy.setDeployId(deployId);
		return deploy;
	}



	@Override
	@Transactional(propagation = Propagation.REQUIRED,rollbackFor=Exception.class)
	public	Deploy  edit(Deploy	deploy,Long id){
		this.daoSupport.update(deploy, id);
		return deploy;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED,rollbackFor=Exception.class)
	public	void delete( Long id)	{
		this.daoSupport.delete(Deploy.class,	id);
	}

	@Override
	public	Deploy getModel(Long id)	{
		return this.daoSupport.queryForObject(Deploy.class, id);
	}




}
