package cn.shoptnt.model.trade.order.vo;

import cn.shoptnt.model.trade.order.dto.OrderDTO;
import cn.shoptnt.service.trade.order.command.OrderCreateCommand;
import cn.shoptnt.service.trade.order.command.impl.StockDeductCommand;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Objects;

/**
 * 订单创建命令执行结果
 *
 * @author dmy
 * @version 1.0
 * 2022-01-15
 */
@ApiModel(description = "订单创建命令执行结果")
public class CommandResult implements Serializable {

    private static final long serialVersionUID = -1104488142709625159L;

    @ApiModelProperty(name = "result", value = "执行结果")
    private Boolean result;

    @ApiModelProperty(name = "error_message", value = "执行失败信息")
    private String errorMessage;


    @ApiModelProperty(name = "command", value = "执行命令")
    private OrderCreateCommand command;


    @ApiModelProperty(name = "order", value = "执行参数")
    private OrderDTO order;

    /**
     * 默认构造器
     */
    public CommandResult() {

    }

    /**
     * 构造方法 初始化结果信息
     *
     * @param result       执行结果
     * @param errorMessage 执行失败信息
     */
    public CommandResult(Boolean result, String errorMessage) {
        this.result = result;
        this.errorMessage = errorMessage;
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CommandResult that = (CommandResult) o;
        return Objects.equals(result, that.result) &&
                Objects.equals(errorMessage, that.errorMessage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(result, errorMessage);
    }

    @Override
    public String toString() {
        return "CommandResult{" +
                "result=" + result +
                ", errorMessage='" + errorMessage + '\'' +
                '}';
    }

    public void setRollback(OrderCreateCommand command, OrderDTO order) {
        this.command = command;
        this.order = order;
    }

    /**
     * 回滚代码
     */
    public void rollback() {
        if (command != null && order != null) {
            command.rollback(order);
        }
    }
}
