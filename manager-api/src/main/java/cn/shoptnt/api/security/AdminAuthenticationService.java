/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.security;

import cn.shoptnt.framework.auth.AuthUser;
import cn.shoptnt.framework.context.user.AdminUserContext;
import cn.shoptnt.framework.security.impl.AbstractAuthenticationService;
import cn.shoptnt.framework.security.model.Admin;
import cn.shoptnt.framework.security.model.Role;
import cn.shoptnt.framework.security.model.User;
import cn.shoptnt.framework.util.StringUtil;
import cn.shoptnt.framework.util.TokenKeyGenerate;
import org.springframework.stereotype.Component;

/**
 * admin 鉴权管理
 * v2.0: 重构鉴权机制
 * Created by 妙贤 on 2018/3/12.
 *
 * @author 妙贤
 * @version 2.0
 * @since 7.0.0
 * 2018/3/12
 */

@Component
public class AdminAuthenticationService extends AbstractAuthenticationService {


    /**
     * 将token解析为Admin
     * @param token
     * @return
     */
    @Override
    protected AuthUser parseToken(String token) {

        AuthUser authUser=  tokenManager.parse(Admin.class, token);
        User user = (User) authUser;
        checkUserDisable(Role.ADMIN, user.getUid());
        return authUser;

    }

}
