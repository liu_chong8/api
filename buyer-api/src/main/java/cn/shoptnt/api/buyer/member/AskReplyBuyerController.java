/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.member;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.base.CharacterConstant;
import cn.shoptnt.model.member.dos.AskReplyDO;
import cn.shoptnt.model.member.dos.MemberAsk;
import cn.shoptnt.model.member.dto.ReplyQueryParam;
import cn.shoptnt.model.member.enums.AuditEnum;
import cn.shoptnt.model.member.enums.CommonStatusEnum;
import cn.shoptnt.model.member.vo.AskReplyVO;
import cn.shoptnt.service.member.AskReplyManager;
import cn.shoptnt.model.util.sensitiveutil.SensitiveFilter;
import cn.shoptnt.framework.context.user.UserContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * 会员商品咨询回复API
 *
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-09-17
 */
@RestController
@RequestMapping("/members/asks/reply")
@Api(tags= "会员商品咨询回复API")
@Validated
public class AskReplyBuyerController {

    @Autowired
    private AskReplyManager askReplyManager;

    @ApiOperation(value = "查询某条会员商品咨询回复列表", response = AskReplyDO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_no", value = "页码", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "query"),
            @ApiImplicitParam(name = "page_size", value = "每页显示数量", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "query"),
            @ApiImplicitParam(name = "ask_id", value = "会员商品咨询id", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "path"),
            @ApiImplicitParam(name = "reply_id", value = "会员商品咨询回复id", dataType = "int", dataTypeClass = Integer.class, paramType = "path")
    })
    @GetMapping("/list/{ask_id}")
    public WebPage listReply(@ApiIgnore Long pageNo, @ApiIgnore Long pageSize, @PathVariable("ask_id")Long askId, @ApiIgnore Long replyId) {

        //会员商品咨询回复搜索参数实体
        ReplyQueryParam param = new ReplyQueryParam();
        param.setPageNo(pageNo);
        param.setPageSize(pageSize);
        param.setAskId(askId);
        param.setReplyId(replyId);
        param.setAuthStatus(AuditEnum.PASS_AUDIT.value());
        param.setReplyStatus(CommonStatusEnum.YES.value());

        return this.askReplyManager.list(param);
    }

    @ApiOperation(value = "查询会员商品咨询回复列表", response = AskReplyVO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_no", value = "页码", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "query"),
            @ApiImplicitParam(name = "page_size", value = "每页显示数量", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "query"),
            @ApiImplicitParam(name = "reply_status", value = "是否已回复 YES：是，NO：否", dataType = "String", dataTypeClass = String.class, paramType = "query")
    })
    @GetMapping("/list/member")
    public WebPage listMemberReply(@ApiIgnore Long pageNo, @ApiIgnore Long pageSize, @ApiIgnore String replyStatus) {

        //会员商品咨询回复搜索参数实体
        ReplyQueryParam param = new ReplyQueryParam();
        param.setPageNo(pageNo);
        param.setPageSize(pageSize);
        param.setReplyStatus(replyStatus);
        param.setMemberId(UserContext.getBuyer().getUid());

        return this.askReplyManager.listMemberReply(param);
    }

    @ApiOperation(value = "回复会员商品咨询", response = AskReplyDO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ask_id", value = "会员商品咨询id", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "path"),
            @ApiImplicitParam(name = "reply_content", value = "回复内容", required = true, dataType = "String", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "anonymous", value = "是否匿名 YES:是，NO:否", required = true, dataType = "String", dataTypeClass = String.class, paramType = "query", allowableValues = "YES,NO")
    })
    @PostMapping("/{ask_id}")
    public AskReplyDO add(@PathVariable("ask_id")Long askId, @NotEmpty(message = "请输入内容")@ApiIgnore String replyContent, @NotNull(message = "请选择是否匿名") @ApiIgnore String anonymous) {

        //咨询回复敏感词过滤
        replyContent = SensitiveFilter.filter(replyContent, CharacterConstant.WILDCARD_STAR);

        return this.askReplyManager.updateReply(askId, replyContent, anonymous);
    }

    @ApiOperation(value = "删除回复", response = MemberAsk.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "会员商品咨询回复id", dataType = "int", dataTypeClass = Integer.class, paramType = "path"),
    })
    @DeleteMapping("/{id}")
    public String delete(@PathVariable("id") Long id) {

        this.askReplyManager.delete(id);

        return "";
    }
}
