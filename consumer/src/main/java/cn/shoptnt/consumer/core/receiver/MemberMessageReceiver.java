/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.receiver;

import java.util.List;

import cn.shoptnt.model.base.rabbitmq.AmqpExchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cn.shoptnt.consumer.core.event.MemberMessageEvent;

/**
 * 站内消息
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0 2018年3月23日 上午10:31:13
 */
@Component
public class MemberMessageReceiver {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<MemberMessageEvent> events;

    /**
     * 站内消息
     *
     * @param messageid
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.MEMBER_MESSAGE + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.MEMBER_MESSAGE, type = ExchangeTypes.FANOUT)
    ))
    public void memberMessage(Long messageid) {

        if (events != null) {
            for (MemberMessageEvent event : events) {
                try {
                    event.memberMessage(messageid);
                } catch (Exception e) {
                    logger.error("站内消息发送出错", e);
                }
            }
        }
    }
}
