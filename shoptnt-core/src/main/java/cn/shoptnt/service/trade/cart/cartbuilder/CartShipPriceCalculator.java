/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.trade.cart.cartbuilder;

import cn.shoptnt.model.trade.cart.vo.CartVO;

import java.util.List;

/**
 * Created by 妙贤 on 2018/12/19.
 * 运费计算器
 * @author 妙贤
 * @version 1.0
 * @since 7.0.0
 * 2018/12/19
 */
public interface CartShipPriceCalculator  {

    void countShipPrice(List<CartVO>cartList) ;

}
