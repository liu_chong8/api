/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.security.impl;

import cn.shoptnt.framework.ShopTntConfig;
import cn.shoptnt.framework.util.DateUtil;
import cn.shoptnt.framework.util.StringUtil;
import cn.shoptnt.mapper.security.SecurityModuleMapper;
import cn.shoptnt.model.security.ScanSateEnum;
import cn.shoptnt.model.security.SecurityModule;
import cn.shoptnt.service.security.SignScanManager;
import cn.shoptnt.service.security.SignScanTask;
import cn.shoptnt.model.security.SecurityModuleEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author 妙贤
 * @version 1.0
 * @data 2021/11/20 11:54
 **/
@Service
public class SignScanManagerImpl implements SignScanManager {

    @Autowired
    private Map<String, SignScanTask> signScanTaskMap;

    @Autowired
    private SecurityModuleMapper securityModuleMapper;

    @Autowired
    protected RedisTemplate redisTemplate;

    @Autowired
    private ShopTntConfig shoptntConfig;


    @Override
    public void scan() {

        //如果不需要签名功能，则直接返回
        if(!shoptntConfig.isSign()){
            return;
        }


        List<SecurityModule> securityModules = securityModuleMapper.selectList(null);
        for (SecurityModule securityModule : securityModules) {
            SecurityModuleEnum securityModuleEnum = SecurityModuleEnum.valueOf(securityModule.getModuleName());
            SignScanTask scanTask =signScanTaskMap.get(securityModuleEnum.getScanTaskName());
            try {
                String rounds = securityModule.getRounds();
                String state = securityModule.getState();
                if (StringUtil.isEmpty(rounds)){
                    state= ScanSateEnum.finished.name();
                }

                //已经扫描完成，或者轮次是空的，说明没有没有扫描过，切换天数为轮次，并标记为未完成
                //否则就是没扫描完成，继续使用原轮次扫描
                if ( ScanSateEnum.finished.name().equals(state)){
                    rounds = DateUtil.toString(new Date(), "yyyydd");
                    securityModule.setRounds(rounds);
                    securityModule.setState(ScanSateEnum.unfinished.name());
                    securityModuleMapper.updateById(securityModule);
                }
                scanTask.scan(rounds,securityModule.getModuleName());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 对所有模块重新签名
     */
    @Override
    public void reSign() {


        List<SecurityModule> securityModules = securityModuleMapper.selectList(null);
        for (SecurityModule securityModule : securityModules) {
            SecurityModuleEnum securityModuleEnum = SecurityModuleEnum.valueOf(securityModule.getModuleName());
            SignScanTask scanTask =signScanTaskMap.get(securityModuleEnum.getScanTaskName());
            scanTask.reSign();
        }

    }

}
