/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.shop;

import cn.shoptnt.model.shop.dos.NavigationDO;
import cn.shoptnt.service.shop.NavigationManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 店铺导航管理控制器
 *
 * @author zjp
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-28 20:44:54
 */
@RestController
@RequestMapping("/shops/navigations")
@Api(tags = "店铺导航管理相关API")
public class NavigationBuyerController {

    @Autowired
    private NavigationManager navigationManager;


    @ApiOperation(value = "查询店铺导航管理列表", response = NavigationDO.class)
    @ApiImplicitParam(name = "shop_id", value = "店铺id", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "path")
    @GetMapping("/{shop_id}")
    public List<NavigationDO> list(@PathVariable("shop_id") Long shopId) {
        List<NavigationDO> list = this.navigationManager.list(shopId, true);
        return list;
    }

}
