/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.shop.aftersale;

import cn.shoptnt.client.trade.OrderOperateClient;
import cn.shoptnt.consumer.core.event.ASNewOrderEvent;
import cn.shoptnt.model.base.message.OrderStatusChangeMsg;
import cn.shoptnt.model.trade.order.dos.OrderDO;
import cn.shoptnt.model.trade.order.enums.OrderStatusEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 新订单生成商品交易快照
 * 针对的是用户申请换货和补发商品的售后服务，商家审核通过后要生成新订单
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-10-23
 */
@Component
public class NewOrderSnapshotConsumer implements ASNewOrderEvent {

    @Autowired
    private OrderOperateClient orderOperateClient;

    @Override
    public void orderChange(OrderStatusChangeMsg orderStatusChangeMsg) {
        //判断订单的新状态为已付款才创建交易快照
        if(orderStatusChangeMsg.getNewStatus().equals(OrderStatusEnum.PAID_OFF)){
            OrderDO orderDO = orderStatusChangeMsg.getOrderDO();
            if (orderDO == null) {
                return;
            }
            //创建交易快照
            this.orderOperateClient.addGoodsSnapshot(orderDO);
        }
    }
}
