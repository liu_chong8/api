/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.framework.swagger;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 *
 * @author liuyulei
 * @version 1.0
 * @since 7.2.2
 * 2021/1/18  16:44
 */
@Component
@ConfigurationProperties("swagger")
public class SwaggerProperties {
    /**
     * 是否开启swagger，生产环境一般关闭，所以这里定义一个变量
     */
    private Boolean enable;

    /**
     * 项目应用名
     */
    private String applicationName;

    /**
     * 项目版本信息
     */
    private String applicationVersion;

    /**
     * 项目描述信息
     */
    private String applicationDescription;

    /**
     * 接口调试地址
     */
    private String tryHost;

    /**
     * 虚拟路径
     */
    private String basePath;


    public Boolean getEnable() {
        return enable;
    }


    public void setEnable(Boolean enable) {
        this.enable = enable;
    }


    public String getApplicationName() {
        return applicationName;
    }


    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }


    public String getApplicationVersion() {
        return applicationVersion;
    }


    public void setApplicationVersion(String applicationVersion) {
        this.applicationVersion = applicationVersion;
    }


    public String getApplicationDescription() {
        return applicationDescription;
    }


    public void setApplicationDescription(String applicationDescription) {
        this.applicationDescription = applicationDescription;
    }


    public String getTryHost() {
        return tryHost;
    }


    public void setTryHost(String tryHost) {
        this.tryHost = tryHost;
    }

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }
}
