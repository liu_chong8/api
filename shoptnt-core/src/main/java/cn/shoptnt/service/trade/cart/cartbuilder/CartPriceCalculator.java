/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.trade.cart.cartbuilder;

import cn.shoptnt.model.trade.cart.vo.CartVO;
import cn.shoptnt.model.trade.cart.vo.PriceDetailVO;

import java.util.List;

/**
 * 购物车价格计算器<br/>
 * 对购物车中的{@link cn.shoptnt.model.trade.cart.vo.PromotionRule}进行计算
 * 形成{@link PriceDetailVO}
 * 文档请参考：<br>
 * <a href="http://doc.shoptnt.cn/current/achitecture/jia-gou/ding-dan/cart-and-checkout.html" >购物车架构</a>
 * @author 妙贤
 * @version 1.0
 * @since 7.0.0
 * 2018/12/10
 */
public interface CartPriceCalculator {

    /**
     * 计算购物车价格
     * @param cartList  购物车列表
     * @param includeCoupon  是否包含优惠券
     * @return  PriceDetailVO 购物车价格对象
     */
    PriceDetailVO countPrice(List<CartVO> cartList, Boolean includeCoupon);


}
