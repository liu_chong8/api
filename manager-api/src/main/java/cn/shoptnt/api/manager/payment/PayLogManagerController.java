/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.payment;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.trade.order.dos.PayLog;
import cn.shoptnt.model.trade.order.dto.PayLogQueryParam;
import cn.shoptnt.service.payment.PayLogManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * 付款单相关API
 * @author Snow create in 2018/7/18
 * @version v2.0
 * @since v7.0.0
 */
@Api(tags= "付款单相关API")
@RestController
@RequestMapping("/admin/trade/orders")
@Validated
public class PayLogManagerController {

    @Autowired
    private PayLogManager payLogManager;

    @ApiOperation(value = "查询付款单列表")
    @GetMapping("/pay-log")
    public WebPage<PayLog> list(PayLogQueryParam param) {

        WebPage page = this.payLogManager.list(param);

        return page;
    }


    @ApiOperation(value = "收款单导出Excel")
    @GetMapping(value = "/pay-log/list")
    public List<PayLog> excel(PayLogQueryParam param) {



        return payLogManager.exportExcel(param);
    }


}
