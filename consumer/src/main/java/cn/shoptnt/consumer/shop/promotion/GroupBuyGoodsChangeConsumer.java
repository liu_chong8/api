/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.shop.promotion;

import cn.shoptnt.client.promotion.PromotionGoodsClient;
import cn.shoptnt.consumer.core.event.GoodsChangeEvent;
import cn.shoptnt.model.base.message.GoodsChangeMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
* @author liuyulei
 * @version 1.0
 * @Description:  修改团购商品信息
 * @date 2019/5/15 9:50
 * @since v7.0
 */
@Component
public class GroupBuyGoodsChangeConsumer implements GoodsChangeEvent {

    @Autowired
    private PromotionGoodsClient promotionGoodsClient;

    @Override
    public void goodsChange(GoodsChangeMsg goodsChangeMsg) {
        //判断商品变化类型为修改
        if (GoodsChangeMsg.UPDATE_OPERATION == goodsChangeMsg.getOperationType()) {
            this.promotionGoodsClient.updateGroupbuyGoodsInfo(goodsChangeMsg.getGoodsIds());
        }
    }
}
