/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.job.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import cn.shoptnt.model.base.JobAmqpExchange;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @version v1.0
 * @Description: 每十分钟执行定时任务
 * @Author: gy
 * @Date: 2020/6/10 0010 9:50
 */
@Component
public class EveryTenMinutesExecuteJobHandler{



    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private AmqpTemplate amqpTemplate;

    @XxlJob("everyTenMinutesExecuteJobHandler")
    public ReturnT<String> execute(String param) throws Exception {
        try {
            amqpTemplate.convertAndSend(JobAmqpExchange.EVERY_TEN_MINUTES_EXECUTE,
                    JobAmqpExchange.EVERY_TEN_MINUTES_EXECUTE + "_ROUTING",
                    "");
        } catch (Exception e) {
            this.logger.error("每分钟任务AMQP消息发送异常：", e);
            return ReturnT.FAIL;
        }
        return ReturnT.SUCCESS;
    }
}
