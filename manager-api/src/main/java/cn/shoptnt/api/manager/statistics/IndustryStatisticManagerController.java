/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.statistics;

import cn.shoptnt.model.base.SearchCriteria;
import cn.shoptnt.model.statistics.vo.SimpleChart;
import cn.shoptnt.service.statistics.IndustryStatisticManager;
import cn.shoptnt.framework.database.WebPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;


/**
 * 行业分析
 *
 * @author chopper
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2018/5/8 下午6:47
 */

@RestController
@Api(tags= "统计 行业分析")
@RequestMapping("/admin/statistics/industry")
public class IndustryStatisticManagerController {

    @Autowired
    private IndustryStatisticManager industryAnalysisManager;

    @ApiOperation("按分类统计下单量")
    @GetMapping(value = "/order/quantity")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "cycle_type", value = "日期类型", dataType = "String", dataTypeClass = String.class, paramType = "query", required = true, allowableValues = "YEAR,MONTH"),
            @ApiImplicitParam(name = "year", value = "年份", dataType = "int", dataTypeClass = Integer.class, paramType = "query", required = true),
            @ApiImplicitParam(name = "month", value = "月份", dataType = "int", dataTypeClass = Integer.class, paramType = "query"),
            @ApiImplicitParam(name = "seller_id",value = "店铺id",dataType = "int", dataTypeClass = Integer.class,paramType = "query")
    })
    public SimpleChart getOrderQuantity(@ApiIgnore SearchCriteria searchCriteria) {
        return this.industryAnalysisManager.getOrderQuantity(searchCriteria);
    }

    @ApiOperation("按分类统计下单商品数量")
    @GetMapping(value = "/goods/num")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "cycle_type", value = "日期类型", dataType = "String", dataTypeClass = String.class, paramType = "query", required = true, allowableValues = "YEAR,MONTH"),
            @ApiImplicitParam(name = "year", value = "年份", dataType = "int", dataTypeClass = Integer.class, paramType = "query", required = true),
            @ApiImplicitParam(name = "month", value = "月份", dataType = "int", dataTypeClass = Integer.class, paramType = "query"),
            @ApiImplicitParam(name = "seller_id",value = "店铺id",dataType = "int", dataTypeClass = Integer.class,paramType = "query")
    })
    public SimpleChart getGoodsNum(@ApiIgnore SearchCriteria searchCriteria) {
        return this.industryAnalysisManager.getGoodsNum(searchCriteria);
    }

    @ApiOperation("按分类统计下单金额")
    @GetMapping(value = "/order/money")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "cycle_type", value = "日期类型", dataType = "String", dataTypeClass = String.class, paramType = "query", required = true, allowableValues = "YEAR,MONTH"),
            @ApiImplicitParam(name = "year", value = "年份", dataType = "int", dataTypeClass = Integer.class, paramType = "query", required = true),
            @ApiImplicitParam(name = "month", value = "月份", dataType = "int", dataTypeClass = Integer.class, paramType = "query"),
            @ApiImplicitParam(name = "seller_id",value = "店铺id",dataType = "int", dataTypeClass = Integer.class,paramType = "query")
    })
    public SimpleChart getOrderMoney(@ApiIgnore SearchCriteria searchCriteria) {
        return this.industryAnalysisManager.getOrderMoney(searchCriteria);
    }

    @ApiOperation("概括总览")
    @GetMapping(value = "/overview")
    public WebPage getGeneralOverview(SearchCriteria searchCriteria) {
        return this.industryAnalysisManager.getGeneralOverview(searchCriteria);
    }
}
