/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.goods;

import cn.shoptnt.model.errorcode.GoodsErrorCode;
import cn.shoptnt.model.goods.dos.SpecValuesDO;
import cn.shoptnt.model.goods.dos.SpecificationDO;
import cn.shoptnt.model.goods.vo.SpecificationVO;
import cn.shoptnt.service.goods.SpecValuesManager;
import cn.shoptnt.service.goods.SpecificationManager;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.security.model.Seller;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * 规格项控制器
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0 2018-03-20 09:31:27
 */
@RestController
@RequestMapping("/seller/goods")
@Api(tags= "规格项相关API")
@Validated
public class SpecificationSellerController {

    @Autowired
    private SpecificationManager specificationManager;

    @Autowired
    private SpecValuesManager specValuesManager;

    @ApiOperation(value = "根据分类id查询规格包括规格值", notes = "根据分类id查询规格")
    @ApiImplicitParam(name = "category_id", value = "分类id", required = true, paramType = "path", dataType = "int",dataTypeClass = Integer.class)
    @GetMapping("/categories/{category_id}/specs")
    public List<SpecificationVO> sellerQuerySpec(@PathVariable("category_id") Long categoryId) {

        return this.specificationManager.querySellerSpec(categoryId);
    }

    @ApiOperation(value = "商家自定义某分类的规格项", response = SpecificationDO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "category_id", value = "分类id", required = true, paramType = "path", dataType = "int",dataTypeClass = Integer.class),
            @ApiImplicitParam(name = "spec_name", value = "规格项名称", required = true, paramType = "query", dataType = "String",dataTypeClass = String.class)})
    @PostMapping("/categories/{category_id}/specs")
    public SpecificationDO add(@PathVariable("category_id") Long categoryId,
                               @ApiIgnore @NotEmpty(message = "规格名称不能为空") @Length(max = 50, message = "规格名称不能超过50个字符") String specName) {
        // 商家的规格
        SpecificationDO specification = this.specificationManager.addSellerSpec(categoryId, specName);

        return specification;
    }

    @ApiOperation(value = "商家自定义某规格的规格值")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "spec_id", value = "规格id", required = true, paramType = "path", dataType = "int",dataTypeClass = Integer.class),
            @ApiImplicitParam(name = "value_name", value = "规格值", required = true, paramType = "query", dataType = "String",dataTypeClass = String.class)})
    @PostMapping("/specs/{spec_id}/values")
    public SpecValuesDO addValue(@PathVariable("spec_id") Long specId,
                                 @ApiIgnore @NotEmpty(message = "规格值名称不能为空") @Length(max = 50,message = "规格值名称不能超过50个字符") String valueName) {
        // 商家的规格
        Seller seller = UserContext.getSeller();
        SpecificationDO spec = specificationManager.getModel(specId);
        boolean flag = spec.getSellerId() == 0 || spec.getSellerId().equals(seller.getSellerId());
        if (spec == null || !flag) {
            throw new ServiceException(GoodsErrorCode.E306.code(), "无权操作");
        }
        if (specificationManager.flagSellerSpec(seller.getSellerId(), specId, valueName)) {
            throw new ServiceException(GoodsErrorCode.E306.code(), "该规格值已存在 ！");
        }
        SpecValuesDO value = new SpecValuesDO(specId, valueName, seller.getSellerId());

        value.setSpecName(spec.getSpecName());

        this.specValuesManager.add(value);

        return value;
    }

}
