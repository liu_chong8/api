/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.mapper.promotion.fulldiscount;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.shoptnt.framework.cache.MybatisRedisCache;
import cn.shoptnt.model.promotion.fulldiscount.dos.FullDiscountGiftDO;
import org.apache.ibatis.annotations.CacheNamespace;

/**
 * 满减满赠促销活动赠品Mapper接口
 * @author duanmingyu
 * @version v1.0
 * @since v7.2.2
 * 2020-08-10
 */
//@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface FullDiscountGiftMapper extends BaseMapper<FullDiscountGiftDO> {
}
