/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.system;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.system.dos.ValidatorPlatformDO;
import cn.shoptnt.model.system.vo.UploaderVO;
import cn.shoptnt.model.system.vo.ValidatorPlatformVO;
import cn.shoptnt.service.system.ValidatorPlatformManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 滑块验证平台相关API
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.6
 * 2019-12-18
 */
@Api(tags= "滑块验证平台相关API")
@RestController
@RequestMapping("/admin/systems/validator")
@Validated
public class ValidatorPlatformManagerController {

    @Autowired
    private ValidatorPlatformManager validatorPlatformManager;

    @ApiOperation(value = "查询验证平台列表", response = ValidatorPlatformDO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_no", value = "页码", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "query"),
            @ApiImplicitParam(name = "page_size", value = "每页显示数量", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "query")
    })
    @GetMapping
    public WebPage list(@ApiIgnore Long pageNo, @ApiIgnore Long pageSize) {

        return this.validatorPlatformManager.list(pageNo, pageSize);
    }

    @ApiOperation(value = "修改验证平台信息", response = ValidatorPlatformDO.class)
    @PutMapping(value = "/{pluginId}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "plugin_id", value = "验证平台插件ID", required = true, dataType = "String", dataTypeClass = String.class, paramType = "path"),
            @ApiImplicitParam(name = "slider_verify_platform", value = "验证平台对象", required = true, dataType = "ValidatorPlatformVO",dataTypeClass = ValidatorPlatformVO.class, paramType = "body")
    })
    public ValidatorPlatformVO edit(@PathVariable String pluginId, @RequestBody @ApiIgnore ValidatorPlatformVO validatorPlatformVO) {
        validatorPlatformVO.setPluginId(pluginId);
        return this.validatorPlatformManager.edit(validatorPlatformVO);
    }

    @ApiOperation(value = "获取验证平台的配置", response = String.class)
    @GetMapping("/{pluginId}")
    @ApiImplicitParam(name = "plugin_id", value = "验证平台插件ID", required = true, dataType = "String", dataTypeClass = String.class, paramType = "path")
    public ValidatorPlatformVO getUploadSetting(@PathVariable String pluginId) {
        return this.validatorPlatformManager.getConfig(pluginId);
    }

    @ApiOperation(value = "开启某个验证平台", response = String.class)
    @PutMapping("/{pluginId}/open")
    @ApiImplicitParam(name = "plugin_id", value = "验证平台插件ID", required = true, dataType = "String", dataTypeClass = String.class, paramType = "path")
    public String open(@PathVariable String pluginId) {
        this.validatorPlatformManager.open(pluginId);
        return null;
    }
}
