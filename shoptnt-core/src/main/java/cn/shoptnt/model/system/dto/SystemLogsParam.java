/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.system.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;

/**
 * 系统日志查询条件
 * @author fk
 * @version v2.0
 * @since v2.0
 * 2021-03-22 16:05:59
 */
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SystemLogsParam {

    @ApiModelProperty(value = "页码", name = "page_no")
    private Long pageNo;

    @ApiModelProperty(value = "分页数", name = "page_size")
    private Long pageSize;

    @ApiModelProperty(value = "模糊查询的关键字", name = "keyword")
    private String keyword;

    @ApiModelProperty(value = "开始时间", name = "start_time")
    private Long startTime;

    @ApiModelProperty(value = "起止时间", name = "end_time")
    private Long endTime;

    @ApiModelProperty(value = "日志级别,normal:一般,important重要", name = "level",allowableValues = "normal,important" )
    private String level;

    /**
     * 商家id
     */
    @ApiModelProperty(hidden = true)
    private Long sellerId;

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Long getPageNo() {
        return pageNo;
    }

    public void setPageNo(Long pageNo) {
        this.pageNo = pageNo;
    }

    public Long getPageSize() {
        return pageSize;
    }

    public void setPageSize(Long pageSize) {
        this.pageSize = pageSize;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }
}
