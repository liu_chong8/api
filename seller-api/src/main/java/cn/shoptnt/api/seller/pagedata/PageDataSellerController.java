/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.pagedata;

import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.security.model.Seller;
import cn.shoptnt.model.errorcode.MemberErrorCode;
import cn.shoptnt.model.pagedata.PageData;
import cn.shoptnt.model.pagedata.PageQueryParam;
import cn.shoptnt.service.pagedata.PageDataManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


/**
 * @Author shen
 * @Date 2021/10/8 9:00
 */
@RestController
@RequestMapping("/seller/pages")
@Api(tags = "微页面相关API")
@Validated
public class PageDataSellerController {
    @Autowired
    private PageDataManager pageManager;


    @GetMapping()
    @ApiOperation(value = "查询页面列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "client_type", value = "PC/MOBILE", required = true, dataType = "String", dataTypeClass = String.class,  paramType = "path")
    })
    public WebPage getList(PageQueryParam param) {
        Seller seller = UserContext.getSeller();
        if (seller == null) {
            throw new ServiceException(MemberErrorCode.E137.code(), "当前商家登录信息已经失效");
        }
        param.setSellerId(seller.getSellerId());
        WebPage pageList = this.pageManager.getPageList(param);

        return pageList;
    }


    @PostMapping()
    @ApiOperation(value = "添加微页面", response = PageData.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_name", value = "微页面标题", required = true, dataType = "String", dataTypeClass = String.class,  paramType = "query"),
            @ApiImplicitParam(name = "page_data", value = "页面元素", required = true, dataType = "String", dataTypeClass = String.class,  paramType = "query")
    })
    public PageData add(@Valid @RequestBody PageData page) {
        Seller seller = UserContext.getSeller();
        if (seller == null) {
            throw new ServiceException(MemberErrorCode.E137.code(), "当前商家登录信息已经失效");
        }
        page.setSellerId(seller.getSellerId());
        this.pageManager.add(page);
        return page;
    }

    @PutMapping(value = "/{id}/publish")
    @ApiOperation(value = "修改发布状态", response = PageData.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "long", dataTypeClass = Long.class, paramType = "path"),
    })
    public PageData updatePublish( @PathVariable("id") Long id) {
        return pageManager.updatePublish(id);
    }

    @PutMapping(value = "/{id}")
    @ApiOperation(value = "修改楼层", response = PageData.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "long", dataTypeClass = Long.class,  paramType = "path"),
            @ApiImplicitParam(name = "page_name", value = "微页面标题", required = true, dataType = "String", dataTypeClass = String.class,  paramType = "query"),
            @ApiImplicitParam(name = "page_data", value = "页面元素", required = true, dataType = "String", dataTypeClass = String.class, paramType = "query")
    })
    public PageData edit(@Valid @RequestBody PageData page, @PathVariable("id") Long id) {

        return this.pageManager.edit(page, id);
    }


    @GetMapping(value = "/{id}")
    @ApiOperation(value = "查询一个楼层")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "要查询的楼层主键", required = true, dataType = "long", dataTypeClass = Long.class, paramType = "path")
    })
    public PageData get(@PathVariable("id") Long id) {

        return this.pageManager.getModel(id);
    }


    @DeleteMapping(value = "/{ids}")
    @ApiOperation(value = "删除微页面")
    @ApiImplicitParams({@ApiImplicitParam(name = "ids", value = "ID集合", required = true, dataType = "long", dataTypeClass = Long.class,  paramType = "path", allowMultiple = true)})

    public void delete(@PathVariable("ids") Long[] ids) {
        this.pageManager.delete(ids);
    }


    @PutMapping(value = "/{id}/index")
    @ApiOperation(value = "设为首页", response = PageData.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "long", dataTypeClass = Long.class,  paramType = "path")
    })
    public PageData editIndex(@PathVariable("id") Long id) {
        Seller seller = UserContext.getSeller();
        if (seller == null) {
            throw new ServiceException(MemberErrorCode.E137.code(), "当前商家登录信息已经失效");
        }
        PageData pageData = this.pageManager.editIndex(seller.getSellerId(), id);

        return pageData;
    }


}
