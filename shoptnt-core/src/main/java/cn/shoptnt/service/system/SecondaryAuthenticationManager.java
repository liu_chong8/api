/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.system;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.system.dos.AuthenticationDO;


/**
 * 二次身份验证业务层
 *
 * @author shenyanwu
 * @version v2.0
 * @since v2.0
 * 2021-11-19 18:45:02
 */
public interface SecondaryAuthenticationManager {

    /**
     * 查询二次身份验证列表
     *
     * @param page     页码
     * @param pageSize 每页数量
     * @return WebPage
     */
    WebPage list(Long page, Long pageSize);

    /**
     * 添加二次身份验证
     *
     * @param authentication 二次身份验证
     * @return Authentication 二次身份验证
     */
    AuthenticationDO add(AuthenticationDO authentication);

    /**
     * 修改二次身份验证
     *
     * @param authentication 二次身份验证
     * @return Authentication 二次身份验证
     */
    AuthenticationDO edit(AuthenticationDO authentication);

    /**
     * 删除二次身份验证
     *
     * @param id 二次身份验证主键
     */
    void delete(Long id);

    /**
     * 获取二次身份验证
     *
     * @param id 二次身份验证主键
     * @return Authentication  二次身份验证
     */
    AuthenticationDO getModel(Long id);
    /**
     * 获取二次身份验证
     *
     * @param uid 管理员id
     * @return Authentication  二次身份验证
     */
    AuthenticationDO getModelByUid(Long uid);

}