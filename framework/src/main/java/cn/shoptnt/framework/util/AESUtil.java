/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.framework.util;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import cn.shoptnt.framework.security.model.TokenConstant;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * @author zjp
 * @version v7.0
 * @Description AES加密工具
 * @ClassName AESUtil
 * @since v7.0 下午10:53 2018/6/26
 */
public class AESUtil {

    private static final Logger logger = LoggerFactory.getLogger(AESUtil.class);

    /**
     * 加密方法
     * @param data  要加密的数据
     * @param key 加密key
     * @return 加密的结果
     * @throws Exception
     */
    public static String encrypt(String data, String key){
        if(data == null){
            logger.debug("要加密的数据为空，key："+key);
            return null;
        }
        try {

            String iv = key;
            //"算法/模式/补码方式"NoPadding PkcsPadding
            Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
            int blockSize = cipher.getBlockSize();

            byte[] dataBytes = data.getBytes("utf-8");
            int plaintextLength = dataBytes.length;
            if (plaintextLength % blockSize != 0) {
                plaintextLength = plaintextLength + (blockSize - (plaintextLength % blockSize));
            }

            byte[] plaintext = new byte[plaintextLength];
            System.arraycopy(dataBytes, 0, plaintext, 0, dataBytes.length);

            SecretKeySpec keyspec = new SecretKeySpec(key.getBytes(), "AES");
            IvParameterSpec ivspec = new IvParameterSpec(iv.getBytes());

            cipher.init(Cipher.ENCRYPT_MODE, keyspec, ivspec);
            byte[] encrypted = cipher.doFinal(plaintext);

            return Base64.encode(encrypted);

        } catch (Exception e) {
            logger.debug("加密异常，key："+key+"；data："+data);
            return null;
        }
    }

    /**
     * 解密方法
     * @param data 要解密的数据
     * @param key  解密key
     * @return 解密的结果
     * @throws Exception
     */
    public static String decrypt(String data, String key) {
        if(data == null){
            logger.debug("要解密的数据为空，key："+key);
            return null;
        }
        try {
            String iv = key;
            byte[] encrypted1 = Base64.decode(data);

            Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
            SecretKeySpec keyspec = new SecretKeySpec(key.getBytes(), "AES");
            IvParameterSpec ivspec = new IvParameterSpec(iv.getBytes());

            cipher.init(Cipher.DECRYPT_MODE, keyspec, ivspec);

            byte[] original = cipher.doFinal(encrypted1);
            String originalString = new String(original);
            return originalString.trim();
        } catch (Exception e) {
            logger.debug("解密异常，key："+key+"；data："+data);
            return "";
        }
    }

    /**
     * 测试
     */
    public static void main(String args[]) throws Exception {
//        long b = DateUtil.getDateline();
        String data = "231124199104152123";
        String data2 = AESUtil.encrypt(data, TokenConstant.SECRET);
//        
        String data1 = "qwL/jwgQxh3pKOJ0ebsJaBQLirByo5ef+jqAdcAmhQw=";
        
//        long a = DateUtil.getDateline();
//        String res = AESUtil.decrypt(data1, TokenConstant.SECRET);
//        
        String res = AESUtil.decrypt(data1, "11158dd8b464f8ef");
        
    }
}
