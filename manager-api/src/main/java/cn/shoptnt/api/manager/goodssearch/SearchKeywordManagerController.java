/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.goodssearch;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.goodssearch.SearchKeywordDO;
import cn.shoptnt.service.goodssearch.SearchKeywordManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
* @author liuyulei
 * @version 1.0
 * @Description: 关键字搜索历史相关API
 * @date 2019/5/27 11:33
 * @since v7.0
 */
@RestController
@RequestMapping("/admin/goodssearch/keywords")
@Api(tags= "关键字搜索历史相关API")
public class SearchKeywordManagerController {


    @Autowired
    private SearchKeywordManager searchKeywordManager;

    @ApiOperation(value = "查询关键字历史列表", response = SearchKeywordDO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_no", value = "页码", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "query"),
            @ApiImplicitParam(name = "page_size", value = "每页显示数量", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "query"),
            @ApiImplicitParam(name = "keywords", value = "关键字", required = false, dataType = "String", dataTypeClass = String.class, paramType = "query")
    })
    @GetMapping
    public WebPage list(@ApiIgnore Long pageNo, @ApiIgnore Long pageSize, @ApiIgnore String keywords){
        return this.searchKeywordManager.list(pageNo,pageSize,keywords);
    }
}
