/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.pagedata;

import cn.shoptnt.model.pagedata.PageData;
import cn.shoptnt.model.pagedata.validator.ClientAppType;
import cn.shoptnt.model.pagedata.validator.PageType;
import cn.shoptnt.service.pagedata.PageDataManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 楼层控制器
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-05-21 16:39:22
 */
@RestController
@RequestMapping("/pages")
@Api(tags = "楼层相关API")
@Validated
public class PageDataBuyerController {

    @Autowired
    private PageDataManager pageManager;

    @GetMapping(value = "/{client_type}/index")
    @ApiOperation(value = "查询首页（pc/移动端）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "client_type", value = "要查询的客户端类型 MOBILE/PC", required = true, dataType = "String",  dataTypeClass = String.class, paramType = "path"),
    })
    public PageData getIndex(@PathVariable("client_type") String clientType) {
        PageData page = this.pageManager.getIndex(0L, clientType);

        return page;
    }

    @GetMapping(value = "/{client_type}/{seller_id}/index")
    @ApiOperation(value = "查询店铺首页（pc/移动端）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "client_type", value = "要查询的客户端类型 MOBILE/PC", required = true, dataTypeClass = String.class, paramType = "path"),
            @ApiImplicitParam(name = "seller_id", value = "商家id", required = true,  dataTypeClass = Long.class, paramType = "path"),
    })
    public PageData getIndex(@PathVariable("client_type")String clientType, @PathVariable ("seller_id")Long sellerId) {
        PageData page = this.pageManager.getIndex(sellerId, clientType);

        return page;
    }

    @GetMapping(value = "/{id}")
    @ApiOperation(value = "查询某个页面")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "微页面id", required = true, dataType = "long", dataTypeClass = Long.class,  paramType = "path"),
    })
    public PageData get(@PathVariable("id") Long id) {
        PageData page = this.pageManager.getModel(id);

        return page;
    }


}