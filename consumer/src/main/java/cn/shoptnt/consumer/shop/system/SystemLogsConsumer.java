/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.shop.system;

import cn.shoptnt.client.system.SystemLogsClient;
import cn.shoptnt.consumer.core.event.SystemLogsEvent;
import cn.shoptnt.model.system.dos.SystemLogs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 系统日志消费者
 *
 * @author fk
 * @version v2.0
 * @since v7.3.0
 * 2021年03月23日16:59:23
 */
@Service
public class SystemLogsConsumer implements SystemLogsEvent {


    @Autowired
    private SystemLogsClient systemLogsClient;


    @Override
    public void add(SystemLogs systemLogs) {

        systemLogsClient.add(systemLogs);
    }
}
