/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.pagedata;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

/**
 * @Author shen
 * @Date 2021/3/15 9:46
 */
public class PageQueryParam {

    @ApiModelProperty(value = "客户端类型",name = "client_type", required = true)
    @NotNull(message = "客户端类型不能为空")
    private String clientType;

    @ApiModelProperty(value = "关键词",name = "keyword", required = false)
    private String keyword;

    @ApiModelProperty(value = "发布状态",name = "publish_status", required = false)
    private String publishStatus;

    @ApiModelProperty(value = "商家id",name = "seller_id", required = false)
    private Long sellerId;

    private Long pageNo;

    private Long pageSize;

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getPublishStatus() {
        return publishStatus;
    }

    public void setPublishStatus(String publishStatus) {
        this.publishStatus = publishStatus;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public Long getPageNo() {
        return pageNo;
    }

    public void setPageNo(Long pageNo) {
        this.pageNo = pageNo;
    }

    public Long getPageSize() {
        return pageSize;
    }

    public void setPageSize(Long pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return "PageQueryParam{" +
                "clientType='" + clientType + '\'' +
                ", keyword='" + keyword + '\'' +
                ", publishStatus='" + publishStatus + '\'' +
                ", sellerId=" + sellerId +
                ", pageNo=" + pageNo +
                ", pageSize=" + pageSize +
                '}';
    }
}
