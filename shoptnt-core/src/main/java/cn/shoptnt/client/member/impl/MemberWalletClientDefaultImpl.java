/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.member.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.shoptnt.client.member.MemberWalletClient;
import cn.shoptnt.model.member.dos.MemberWalletDO;
import cn.shoptnt.model.security.ScanModuleDTO;
import cn.shoptnt.service.member.MemberWalletManager;
import cn.shoptnt.model.security.ScanResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * 会员预存款默认实现
 *
 * @author zs
 * @version v7.0
 * @date 2021-12-21
 * @since v7.2.3
 */
@Service
@ConditionalOnProperty(value = "shoptnt.product", havingValue = "stand")
public class MemberWalletClientDefaultImpl implements MemberWalletClient {

    @Autowired
    private MemberWalletManager memberWalletManager;

    @Override
    public MemberWalletDO getByMemberId(Long memberId) {
        return memberWalletManager.getByMemberId(memberId);
    }

    @Override
    public ScanResult scanModule(ScanModuleDTO scanModuleDTO) {
        return memberWalletManager.scanModule(scanModuleDTO);
    }

    @Override
    public void reSign() {
        memberWalletManager.reSign();
    }

    @Override
    public void repair(Long memberWalletId) {
        memberWalletManager.repair(memberWalletId);
    }
}
