/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.receiver;

import cn.shoptnt.consumer.core.event.AskReplyEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import cn.shoptnt.model.base.message.AskReplyMessage;
import cn.shoptnt.model.base.rabbitmq.AmqpExchange;
import org.apache.shardingsphere.transaction.annotation.ShardingTransactionType;
import org.apache.shardingsphere.transaction.core.TransactionType;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 会员商品咨询回复消息接收者
 *
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-09-17
 */
@Component
public class TxReceiver {

    /**
     * 会员回复商品咨询
     *
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value =  "tx_QUEUE"),
            exchange = @Exchange(value = "tx", type = ExchangeTypes.FANOUT)
    ))

    public void goodsAsk(String msg) {
         
        try {
            ask();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    @Transactional(value = "transactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    @ShardingTransactionType(TransactionType.XA)
    public void ask() {
        
        throw new RuntimeException("abc");
    }
}
