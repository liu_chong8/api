/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.receiver;

import cn.shoptnt.consumer.core.event.SystemLogsEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import cn.shoptnt.model.base.rabbitmq.AmqpExchange;
import cn.shoptnt.model.system.dos.SystemLogs;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 系统日志消息接受者
 *
 * @author fk
 * @version v2.0
 * @since v7.3.0
 * 2021年03月23日17:01:09
 */
@Component
public class SystemLogsReceiver {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<SystemLogsEvent> events;

    /**
     * 系统日志
     *
     * @param log
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.LOGS + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.LOGS, type = ExchangeTypes.FANOUT)
    ))
    public void add(SystemLogs log) {
        if (events != null) {
            for (SystemLogsEvent event : events) {
                try {
                    event.add(log);
                } catch (Exception e) {
                    logger.error("处理系统日志出错",e);
                }
            }
        }
    }
}
