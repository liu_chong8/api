/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.shop;


import cn.shoptnt.framework.security.model.TokenConstant;
import cn.shoptnt.framework.util.DbSecretUtil;
import cn.shoptnt.model.base.context.Region;
import cn.shoptnt.model.base.context.RegionFormat;
import cn.shoptnt.client.goods.TagClient;
import cn.shoptnt.model.member.dos.Member;
import cn.shoptnt.model.support.LogClient;
import cn.shoptnt.model.support.validator.annotation.Log;
import cn.shoptnt.model.support.validator.annotation.LogLevel;
import cn.shoptnt.service.member.MemberManager;
import cn.shoptnt.model.errorcode.ShopErrorCode;
import cn.shoptnt.model.shop.dos.ClerkDO;
import cn.shoptnt.model.shop.enums.ShopStatusEnum;
import cn.shoptnt.model.shop.vo.ShopParamsVO;
import cn.shoptnt.model.shop.vo.ShopVO;
import cn.shoptnt.model.shop.vo.operator.AdminEditShop;
import cn.shoptnt.service.shop.ClerkManager;
import cn.shoptnt.service.shop.ShopManager;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.util.DateUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 店铺相关API
 *
 * @author zhangjiping
 * @version v7.0.0
 * @since v7.0.0
 * 2018年3月30日 上午10:58:45
 */
@Api(tags = "店铺相关API")
@RestController
@RequestMapping("/admin/shops")
@Validated
public class ShopManagerController {
    @Autowired
    private ShopManager shopManager;


    @ApiOperation(value = "分页查询店铺列表", response = ShopVO.class)
    @GetMapping()
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_no", value = "页码", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "query"),
            @ApiImplicitParam(name = "page_size", value = "分页数", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "query")
    })
    public WebPage listPage(ShopParamsVO shopParams, @ApiIgnore @NotNull(message = "页码不能为空") Long pageNo, @ApiIgnore @NotNull(message = "每页数量不能为空") Long pageSize) {
        shopParams.setPageNo(pageNo);
        shopParams.setPageSize(pageSize);
        return shopManager.list(shopParams);
    }

    @ApiOperation(value = "分页查询列表", response = ShopVO.class)
    @GetMapping(value = "/list")
    public List<ShopVO> list() {
        return this.shopManager.list();
    }

    @ApiOperation(value = "管理员禁用店铺", response = AdminEditShop.class)
    @PutMapping(value = "/disable/{shop_id}")
    @ApiImplicitParam(name = "shop_id", value = "店铺id", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "path")
    public AdminEditShop disShop(@ApiIgnore @PathVariable("shop_id") Long shopId) {
        //管理员对店铺进行操作
        AdminEditShop adminEditShop = new AdminEditShop();
        adminEditShop.setSellerId(shopId);
        adminEditShop.setOperator("管理员禁用店铺");
        shopManager.disShop(shopId);
        return adminEditShop;
    }

    @ApiOperation(value = "管理员恢复店铺使用", response = AdminEditShop.class)
    @PutMapping(value = "/enable/{shop_id}")
    @ApiImplicitParam(name = "shop_id", value = "店铺id", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "path")
    public AdminEditShop useShop(@ApiIgnore @PathVariable("shop_id") Long shopId) {
        //管理员对店铺进行操作
        AdminEditShop adminEditShop = new AdminEditShop();
        adminEditShop.setSellerId(shopId);
        adminEditShop.setOperator("管理员恢复店铺");
        shopManager.useShop(shopId);
        return adminEditShop;
    }

    @ApiOperation(value = "管理员获取店铺详细", response = ShopVO.class)
    @GetMapping("/{shop_id}")
    @ApiImplicitParam(name = "shop_id", value = "店铺id", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "path")
    public ShopVO getShopVO(@ApiIgnore @PathVariable("shop_id") @NotNull(message = "店铺id不能为空") Long shopId) {
        ShopVO shop = shopManager.getShop(shopId);

        if (shop == null) {
            throw new ServiceException(ShopErrorCode.E206.name(), "店铺不存在");
        }
        //服务器秘钥解密，会话秘钥加密
        if (shop.getCompanyPhone() != null) {
            String plainValue = DbSecretUtil.decrypt(shop.getCompanyPhone(), TokenConstant.SECRET);
            shop.setCompanyPhone(plainValue);
        }
        //服务器秘钥解密，会话秘钥加密
        if (shop.getLinkPhone() != null) {
            String plainValue = DbSecretUtil.decrypt(shop.getLinkPhone(), TokenConstant.SECRET);
            shop.setLinkPhone(plainValue);
        }

        //服务器秘钥解密，会话秘钥加密
        if (shop.getLegalId() != null) {
            shop.setLegalId(DbSecretUtil.decrypt(shop.getLegalId(), TokenConstant.SECRET));
        }

        //服务器秘钥解密，会话秘钥加密
        if (shop.getBankNumber() != null) {
            shop.setBankNumber(DbSecretUtil.decrypt(shop.getBankNumber(), TokenConstant.SECRET));
        }

        return shop;
    }

    @ApiOperation(value = "管理员修改审核店铺信息", response = ShopVO.class)
    @PutMapping("/{shop_id}")
    @Log(client = LogClient.admin, detail = "审核店铺，店铺id：${shopId}", level = LogLevel.important)
    @ApiImplicitParam(name = "pass", value = "是否通过审核 1 通过 0 拒绝 编辑操作则不需传递", paramType = "query", dataType = "int", dataTypeClass = Integer.class)
    public ShopVO edit(@Valid ShopVO shop, Integer pass, @ApiIgnore @PathVariable("shop_id") Long shopId,
                       @RegionFormat @RequestParam("license_region") Region licenseRegion,
                       @RegionFormat @RequestParam("bank_region") Region bankRegion,
                       @RegionFormat @RequestParam("shop_region") Region shopRegion) {
        return this.shopManager.auditShopInfo(shop, pass, shopId, licenseRegion, bankRegion, shopRegion);
    }

    @ApiOperation(value = "后台添加店铺", response = ShopVO.class)
    @PostMapping()
    public ShopVO save(@Valid ShopVO shop,
                       @RegionFormat @RequestParam("license_region") Region licenseRegion,
                       @RegionFormat @RequestParam("bank_region") Region bankRegion,
                       @RegionFormat @RequestParam("shop_region") Region shopRegion) {

        //店铺信息
        shop = this.shopManager.buildRegionInfo(shop, licenseRegion, bankRegion, shopRegion);
        //后台注册店铺
        this.shopManager.registStore(shop);
        return shop;
    }
}
