/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.member;

import cn.shoptnt.model.member.enums.ConnectTypeEnum;
import cn.shoptnt.model.member.vo.ConnectVO;
import cn.shoptnt.service.member.AbstractConnectLoginPlugin;
import cn.shoptnt.service.member.ConnectManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author zjp
 * @version v7.0
 * @Description 会员信任登录API
 * @ClassName MemberConnectController
 * @since v7.0 上午10:26 2018/6/14
 */
@Api(tags= "会员信任登录API")
@RestController
@RequestMapping("/account-binder")
public class MemberConnectBuyerController {

    @Autowired
    private ConnectManager connectManager;


    @GetMapping("/pc/{type}")
    @ApiOperation(value = "发起账号绑定")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type", value = "登录方式:QQ,微博,微信,支付宝",dataTypeClass = String.class,allowableValues = "QQ,WEIBO,WECHAT,ALIPAY", paramType = "path")
    })
    public String initiate(@PathVariable("type") @ApiIgnore String type) throws IOException {
        //信任登录类型枚举类
        ConnectTypeEnum connectTypeEnum = ConnectTypeEnum.valueOf(type);
        //信任登录插件基类
        AbstractConnectLoginPlugin connectionLogin = connectManager.getConnectionLogin(connectTypeEnum);
        return connectionLogin.getLoginUrl();
    }

    @ApiOperation(value = "会员解绑操作")
    @PostMapping("/unbind/{type}")
    @ApiImplicitParam(name = "type", value = "登录方式:QQ,微博,微信,支付宝",dataTypeClass = String.class,allowableValues = "QQ,WEIBO,WECHAT,ALIPAY", paramType = "path")
    public void unbind(@PathVariable("type") String type) {
        if (ConnectTypeEnum.WECHAT.name().equals(type)) {
            //PC端解绑操作后，清除微信保存相关信息
            //解除绑定
            connectManager.unbind(ConnectTypeEnum.WECHAT.value());
            //解除绑定需要清空OPENID,否则微信服务消息在解除绑定之后会继续推送,并且分销提现会到账(在解绑之前的微信账号内)  add by liuyulei 2020-12-22
            connectManager.unbind(ConnectTypeEnum.WECHAT_OPENID.value());
            //解除小程序自动登录
            connectManager.unbind(ConnectTypeEnum.WECHAT_MINI.value());
        } else {
            connectManager.unbind(type);
        }
    }

    @ApiOperation(value = "微信退出解绑操作")
    @PostMapping("/unbind/out")
    public void wechatOut() {
        connectManager.wechatOut();
    }


    @ApiOperation(value = "登录绑定openid")
    @PostMapping("/login/{uuid}")
    @ApiImplicitParam(name = "uuid", value = "客户端唯一标识", required = true, dataType = "String", dataTypeClass = String.class, paramType = "path")
    public Map openidBind(@PathVariable("uuid") @ApiIgnore String uuid) {
        return connectManager.openidBind(uuid);
    }

    @ApiOperation(value = "注册绑定openid")
    @PostMapping("/register/{uuid}")
    @ApiImplicitParam(name = "uuid", value = "客户端唯一标识", required = true, dataType = "String", dataTypeClass = String.class, paramType = "path")
    public void registerBind(@PathVariable("uuid") @ApiIgnore String uuid) {
        connectManager.registerBind(uuid);
    }

    @ApiOperation(value = "获取绑定列表API")
    @GetMapping("/list")
    public List<ConnectVO> get() {
        return connectManager.get();
    }

}
