/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.trade.pintuan.impl;

import cn.shoptnt.client.goods.GoodsClient;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.elasticsearch.ElasticJestConfig;
import cn.shoptnt.framework.elasticsearch.ElasticOperationUtil;
import cn.shoptnt.framework.elasticsearch.EsSettings;
import cn.shoptnt.framework.elasticsearch.core.ElasticSearchResult;
import cn.shoptnt.framework.logs.Debugger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import cn.shoptnt.framework.util.DateUtil;
import cn.shoptnt.framework.util.HexUtils;
import cn.shoptnt.framework.util.StringUtil;
import cn.shoptnt.mapper.trade.pintuan.PintuanGoodsMapper;
import cn.shoptnt.model.goods.SkuNameUtil;
import cn.shoptnt.model.goods.dos.CategoryDO;
import cn.shoptnt.model.goods.vo.CacheGoods;
import cn.shoptnt.model.goods.vo.GoodsSkuVO;
import cn.shoptnt.model.promotion.pintuan.PinTuanGoodsVO;
import cn.shoptnt.model.promotion.pintuan.PtGoodsDoc;
import cn.shoptnt.service.trade.pintuan.PinTuanSearchManager;
import io.searchbox.client.JestClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.data.elasticsearch.core.query.IndexQueryBuilder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 妙贤 on 2019-01-21.
 * 拼团搜索业务实现者
 *
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2019-01-21
 */

@Service
public class PinTuanSearchManagerImpl implements PinTuanSearchManager {

    @Autowired
    private ElasticsearchRestTemplate elasticsearchRestTemplate;

    @Autowired
    private ElasticJestConfig elasticJestConfig;

    @Autowired
    private GoodsClient goodsClient;

    @Autowired
    private PintuanGoodsMapper pintuanGoodsMapper;

    @Autowired
    private JestClient jestClient;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public PinTuanSearchManagerImpl() {
    }

    public PinTuanSearchManagerImpl(ElasticsearchRestTemplate elasticsearchRestTemplate, ElasticJestConfig esConfig) {
        this.elasticsearchRestTemplate = elasticsearchRestTemplate;
        this.elasticJestConfig = esConfig;

    }

    /**
     * 搜索拼团商品
     * @param categoryId 商品分类id
     * @param pageNo 页码
     * @param pageSize 每页显示条数
     * @return 拼团商品集合
     */
    @SuppressWarnings("Duplicates")
    @Override
    public List<PtGoodsDoc> search(Long categoryId, Integer pageNo, Integer pageSize) {

        //sourceBuilder 是用来构建查询条件
        SearchSourceBuilder sourceBuilder = getSearchSourceBuilder(categoryId);
        String indexName = elasticJestConfig.getIndexName() + "_" + EsSettings.PINTUAN_INDEX_NAME;
        ElasticSearchResult elasticSearchResult = ElasticOperationUtil.search(jestClient,indexName,sourceBuilder.toString());

        return elasticSearchResult.getSourceAsObjectList(PtGoodsDoc.class,false);
    }

    /**
     * 向es写入索引
     * @param goodsDoc 拼团商品
     */
    @Override
    public void addIndex(PtGoodsDoc goodsDoc) {

        //对cat path特殊处理
        CategoryDO categoryDO = goodsClient.getCategory(goodsDoc.getCategoryId());
        goodsDoc.setCategoryPath(HexUtils.encode(categoryDO.getCategoryPath()));
        String indexName = elasticJestConfig.getIndexName() + "_" + EsSettings.PINTUAN_INDEX_NAME;
        ElasticOperationUtil.insert(jestClient,indexName,goodsDoc);
        logger.debug("将拼团商品将拼团商品ID[" + goodsDoc.getGoodsId() + "] " + goodsDoc + " 写入索引");
    }

    @Autowired
    private Debugger debugger;

    /**
     * 向es写入索引
     * @param pintuanGoods 拼团商品
     * @return  是否生成成功
     */
    @Override
    public boolean addIndex(PinTuanGoodsVO pintuanGoods) {

        String goodsName = pintuanGoods.getGoodsName() + " " + SkuNameUtil.createSkuName(pintuanGoods.getSpecs());

        try {

            CacheGoods cacheGoods = goodsClient.getFromCache(pintuanGoods.getGoodsId());
            PtGoodsDoc ptGoodsDoc = new PtGoodsDoc();
            ptGoodsDoc.setCategoryId(cacheGoods.getCategoryId());
            ptGoodsDoc.setThumbnail(pintuanGoods.getThumbnail());
            ptGoodsDoc.setSalesPrice(pintuanGoods.getSalesPrice());
            ptGoodsDoc.setOriginPrice(pintuanGoods.getPrice());
            ptGoodsDoc.setGoodsName(goodsName);
            ptGoodsDoc.setBuyCount(pintuanGoods.getSoldQuantity());
            ptGoodsDoc.setGoodsId(pintuanGoods.getGoodsId());
            ptGoodsDoc.setSkuId(pintuanGoods.getSkuId());
            ptGoodsDoc.setPinTuanId(pintuanGoods.getPintuanId());

            this.addIndex(ptGoodsDoc);

            return true;
        } catch (Exception e) {
            logger.error("为拼团商品[" + goodsName + "]生成索引报错", e);
            debugger.log("为拼团商品[" + goodsName + "]生成索引报错", StringUtil.getStackTrace(e));
            return false;
        }


    }

    /**
     * 删除一个sku的索引
     * @param skuId 商品sku id
     */
    @Override
    public void delIndex(Long skuId) {
        TermQueryBuilder termQueryBuilder = new TermQueryBuilder("skuId", skuId);
        dq(termQueryBuilder);
        logger.debug("将拼团商品ID[" + skuId + "]删除索引");
    }

    /**
     * 删除某个商品的所有的索引
     * @param goodsId 商品id
     */
    @Override
    public void deleteByGoodsId(Long goodsId) {
        TermQueryBuilder termQueryBuilder = new TermQueryBuilder("goodsId", goodsId);
        dq(termQueryBuilder);
    }

    /**
     * 删除某个拼团的所有索引
     * @param pinTuanId 拼团id
     */
    @Override
    public void deleteByPintuanId(Long pinTuanId) {
        TermQueryBuilder termQueryBuilder = new TermQueryBuilder("pinTuanId", pinTuanId);
        dq(termQueryBuilder);
    }

    /**
     * 根据拼团id同步es中的拼团商品<br/>
     * 当拼团活动商品发生变化时调用此方法
     * @param pinTuanId 拼团活动id
     */
    @Override
    public void syncIndexByPinTuanId(Long pinTuanId) {
        //查询出数据库中的拼团商品
        List<PinTuanGoodsVO> dbList = this.pintuanGoodsMapper.selectPinTuanGoodsVOList(pinTuanId);
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        //查询出索引出的商品
        BoolQueryBuilder bqb = QueryBuilders.boolQuery();
        bqb.must(QueryBuilders.termQuery("pinTuanId", pinTuanId));

        String indexName = elasticJestConfig.getIndexName() + "_" + EsSettings.PINTUAN_INDEX_NAME;
        sourceBuilder.query(bqb);
        ElasticSearchResult elasticSearchResult = ElasticOperationUtil.search(jestClient,indexName,sourceBuilder.toString());

        //同步数据
        sync(dbList, elasticSearchResult.getSourceAsObjectList(PtGoodsDoc.class,false));

    }

    /**
     * 根据商品id同步es中的拼团商品<br>
     * @param goodsId 商品id
     */
    @Override
    public void syncIndexByGoodsId(Long goodsId) {
        CacheGoods goods = goodsClient.getFromCache(goodsId);
        List<GoodsSkuVO> skuList = goods.getSkuList();

        Long now = DateUtil.getDateline();

        //查询出数据库中的拼团商品
        List<PinTuanGoodsVO> dbTempList = this.pintuanGoodsMapper.selectPintuanGoodsByGoodsId(goodsId, now, now);

        List<PinTuanGoodsVO> dbList = new ArrayList<>();

        for (PinTuanGoodsVO ptGoods : dbTempList) {
            if (findSku(ptGoods.getSkuId(), skuList)) {
                dbList.add(ptGoods);
            }
        }
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();

        //根据商品id查询出索引中的商品
        BoolQueryBuilder bqb = QueryBuilders.boolQuery();
        bqb.must(QueryBuilders.termQuery("goodsId", goodsId));
        String indexName = elasticJestConfig.getIndexName() + "_" + EsSettings.PINTUAN_INDEX_NAME;
        sourceBuilder.query(bqb);
        ElasticSearchResult elasticSearchResult = ElasticOperationUtil.search(jestClient,indexName,sourceBuilder.toString());
        //同步数据
        sync(dbList, elasticSearchResult.getSourceAsObjectList(PtGoodsDoc.class,false));

    }

    /**
     * 查询分页的拼团商品
     * @param categoryId 商品分类id
     * @param pageNo 页码
     * @param pageSize 每页显示条数
     * @return 拼团商品分页数据
     */
    @Override
    public WebPage searchPage(Long categoryId, Integer pageNo, Integer pageSize) {

        SearchSourceBuilder sourceBuilder = getSearchSourceBuilder(categoryId);
        //设置分页信息  设置是否按查询匹配度排序
        sourceBuilder.from((pageNo - 1) * pageSize).size(pageSize).explain(true);
        // 构建索引名称
        String indexName = elasticJestConfig.getIndexName() + "_" + EsSettings.PINTUAN_INDEX_NAME;
        ElasticSearchResult elasticSearchResult = ElasticOperationUtil.search(jestClient,indexName,sourceBuilder.toString());
        return new WebPage(pageNo.longValue(), elasticSearchResult.getTotal(),pageSize.longValue(),  elasticSearchResult.getSourceAsObjectList(PtGoodsDoc.class,false));
    }

    /**
     * 判断集合中是否存在某个sku
     * @param skuId skuID
     * @param skuList sku集合
     * @return
     */
    private boolean findSku(Long skuId, List<GoodsSkuVO> skuList) {
        for (GoodsSkuVO sku : skuList) {
            if (sku.getSkuId().equals(skuId)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 删除
     * @param termQueryBuilder
     * @
     */
    private void dq(TermQueryBuilder termQueryBuilder) {
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(termQueryBuilder);
        //配置文件中定义的索引名字
        String indexName = elasticJestConfig.getIndexName() + "_" + EsSettings.PINTUAN_INDEX_NAME;
        ElasticOperationUtil.deleteByQuery(jestClient,indexName,searchSourceBuilder.toString());
    }

    /**
     * 构建查询条件
     * @param categoryId
     * @return
     */
    private SearchSourceBuilder getSearchSourceBuilder(Long categoryId) {
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        // 分类检索
        if (categoryId != null) {
            CategoryDO category = goodsClient.getCategory(categoryId);
            if (category != null) {
                boolQueryBuilder.must(QueryBuilders.wildcardQuery("categoryPath", HexUtils.encode(category.getCategoryPath()) + "*"));
            }
        }
        //sourceBuilder 是用来构建查询条件
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.query(boolQueryBuilder);
        return sourceBuilder;
    }

    /**
     * 对比数据库和es中的两个集合，以数据库的为准，同步es中的数据
     *
     * @param dbList 数据库集合
     * @param esList es中的集合
     */
    private void sync(List<PinTuanGoodsVO> dbList, List<PtGoodsDoc> esList) {

        //按数据库中的来循环
        dbList.forEach(dbGoods -> {

            PtGoodsDoc goodsDoc = findFromList(esList, dbGoods.getSkuId());

            //在索引中没找到说明新增了
            if (goodsDoc == null) {
                this.addIndex(dbGoods);

            } else {

                //看看售价变没变
                Double salesPrice = goodsDoc.getSalesPrice();
                Double dbPrice = dbGoods.getSalesPrice();

                //价格发生变化了
                if (!salesPrice.equals(dbPrice)) {
                    goodsDoc.setSalesPrice(dbPrice);
                    String indexName = elasticJestConfig.getIndexName() + "_" + EsSettings.PINTUAN_INDEX_NAME;
                    IndexQuery indexQuery = new IndexQueryBuilder().withId("" + goodsDoc.getSkuId()).withObject(goodsDoc).build();
                    elasticsearchRestTemplate.index(indexQuery, IndexCoordinates.of(indexName));
                }
            }

        });

        //遍历查看删除了的商品
        esList.forEach(goodsDoc -> {
            boolean result = findFormList(dbList, goodsDoc.getSkuId());

            //没找到，说明删除了
            if (!result) {
                delIndex(goodsDoc.getSkuId());
            }

        });


    }

    /**
     * 查询 数据库的列表中没有
     *
     * @param dbList 拼团商品集合
     * @param skuId 商品sku id
     * @return true：有 false：没有
     */
    private boolean findFormList(List<PinTuanGoodsVO> dbList, Long skuId) {
        for (PinTuanGoodsVO goodsVO : dbList) {
            if (goodsVO.getSkuId().equals(skuId)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 查找索引列表中有没有
     *
     * @param list 拼团商品集合
     * @param skuId 商品sku id
     * @return 拼团商品
     */
    private PtGoodsDoc findFromList(List<PtGoodsDoc> list, Long skuId) {
        for (PtGoodsDoc goodsDoc : list) {
            if (goodsDoc.getSkuId().equals(skuId)) {
                return goodsDoc;
            }
        }

        return null;
    }
}
