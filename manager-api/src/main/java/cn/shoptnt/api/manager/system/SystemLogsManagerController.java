/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.system;

import cn.shoptnt.model.system.dos.SystemLogs;
import cn.shoptnt.model.system.dto.SystemLogsParam;
import cn.shoptnt.service.system.SystemLogsManager;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import cn.shoptnt.framework.database.WebPage;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;


/**
 * 系统日志控制器
 * @author fk
 * @version v2.0
 * @since v2.0
 * 2021-03-22 16:05:59
 */
@RestController
@RequestMapping("/admin/system-logs")
@Api(tags= "系统日志相关API")
public class SystemLogsManagerController	{
	
	@Autowired
	private SystemLogsManager systemLogsManager;
				

	@ApiOperation(value	= "查询系统日志列表", response = SystemLogs.class)
	@ApiImplicitParams({
		 @ApiImplicitParam(name	= "page_no",	value =	"页码",	required = true, dataType = "long", dataTypeClass = Long.class,	paramType =	"query"),
		 @ApiImplicitParam(name	= "page_size",	value =	"每页显示数量",	required = true, dataType = "long", dataTypeClass = Long.class,	paramType =	"query")
	})
	@GetMapping
	public WebPage list(SystemLogsParam param, @ApiIgnore Long pageNo, @ApiIgnore Long pageSize)	{

		param.setPageNo(pageNo);
		param.setPageSize(pageSize);


		return	this.systemLogsManager.list(param,"admin");
	}
				
	
	@GetMapping(value =	"/{id}")
	@ApiOperation(value	= "查询一个系统日志")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "id",	value = "要查询的系统日志主键",required = true, dataType = "long", dataTypeClass = Long.class,	paramType = "path")
	})
	public	SystemLogs get(@PathVariable Long id)	{
		
		SystemLogs systemLogs = this.systemLogsManager.getModel(id);
		
		return	systemLogs;
	}
				
}