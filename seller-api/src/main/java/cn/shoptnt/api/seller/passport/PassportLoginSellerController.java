/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.passport;

import cn.shoptnt.model.base.SceneType;
import cn.shoptnt.client.system.SmsClient;
import cn.shoptnt.client.system.ValidatorClient;
import cn.shoptnt.model.errorcode.MemberErrorCode;
import cn.shoptnt.model.member.vo.MemberVO;
import cn.shoptnt.model.member.vo.SellerInfoVO;
import cn.shoptnt.model.support.LogClient;
import cn.shoptnt.model.support.validator.annotation.Log;
import cn.shoptnt.service.member.MemberManager;
import cn.shoptnt.service.passport.PassportManager;
import cn.shoptnt.service.shop.ClerkManager;
import cn.shoptnt.framework.ShopTntConfig;
import cn.shoptnt.framework.exception.ServiceException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.constraints.NotEmpty;


/**
 * 会员登录注册API
 *
 * @author zh
 * @version v7.0
 * @since v7.0
 * 2018年3月23日 上午10:12:12
 */
@RestController
@RequestMapping("/seller/login")
@Api(tags= "商家登录API")
@Validated
public class PassportLoginSellerController {

    @Autowired
    private PassportManager passportManager;
    @Autowired
    private ValidatorClient validatorClient;
    @Autowired
    private MemberManager memberManager;
    @Autowired
    private ClerkManager clerkManager;
    @Autowired
    private SmsClient smsClient;
    @Autowired
    private ShopTntConfig shoptntConfig;

    @PostMapping(value = "/smscode/{mobile}")
    @ApiOperation(value = "发送验证码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "mobile", value = "手机号码", required = true, dataType = "String", dataTypeClass = String.class, paramType = "path")
    })
    public String sendSmsCode(@PathVariable("mobile") String mobile) {

        //参数验证（验证图片验证码或滑动验证参数等）
        this.validatorClient.validate();

        passportManager.sendLoginSmsCode(mobile);
        return shoptntConfig.getSmscodeTimout() / 60 + "";
    }

    @GetMapping()
    @ApiOperation(value = "用户名（手机号）/密码登录API")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "用户名", required = true, dataType = "String", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "password", value = "密码", required = true, dataType = "String", dataTypeClass = String.class, paramType = "query")
    })
    public SellerInfoVO login(@NotEmpty(message = "用户名不能为空") String username, @NotEmpty(message = "密码不能为空") String password) {
        //参数验证（验证图片验证码或滑动验证参数等）
        this.validatorClient.validate();
        return clerkManager.login(username, password);
    }

    @GetMapping("/login/{mobile}")
    @ApiOperation(value = "手机号码登录API")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "mobile", value = "手机号", required = true, dataType = "String", dataTypeClass = String.class, paramType = "path"),
            @ApiImplicitParam(name = "sms_code", value = "手机验证码", required = true, dataType = "String", dataTypeClass = String.class, paramType = "query")
    })
    @Log(client = LogClient.seller,detail = "${mobile}登陆")
    public MemberVO mobileLogin(@PathVariable String mobile, @ApiIgnore @NotEmpty(message = "短信验证码不能为空") String smsCode) {
        return memberManager.mobileLogin(mobile,smsCode, 2);

    }
}
