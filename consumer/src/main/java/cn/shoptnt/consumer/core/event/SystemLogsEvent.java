/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.event;


import cn.shoptnt.model.system.dos.SystemLogs;

/**
 * 系统日志事件
 *
 * @author fk
 * @version v2.0
 * @since v7.3.0
 * 2021年03月23日16:59:23
 */
public interface SystemLogsEvent {

    /**
     * 添加系统日志
     * @param systemLogs
     */
    void add(SystemLogs systemLogs);


}
