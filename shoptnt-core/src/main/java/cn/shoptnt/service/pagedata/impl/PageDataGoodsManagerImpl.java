/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.pagedata.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.shoptnt.mapper.system.PageDataGoodsMapper;
import cn.shoptnt.model.pagedata.PageDataGoods;
import cn.shoptnt.service.pagedata.PageDataGoodsManager;
import org.springframework.stereotype.Service;


/**
 * 楼层商品中间
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2021年12月10日17:30:15
 */
@Service
public class PageDataGoodsManagerImpl extends ServiceImpl<PageDataGoodsMapper,PageDataGoods> implements PageDataGoodsManager {


}
