/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.receiver;

import java.util.List;

import cn.shoptnt.model.base.rabbitmq.AmqpExchange;
import cn.shoptnt.model.member.vo.MemberLoginMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cn.shoptnt.consumer.core.event.MemberLoginEvent;

/**
 * 会员登录消费者
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0 2018年3月23日 上午10:31:06
 */
@Component
public class MemberLoginReceiver {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<MemberLoginEvent> events;

    /**
     * 会员登录
     *
     * @param memberLoginMsg
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.MEMEBER_LOGIN + "LOGIN_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.MEMEBER_LOGIN, type = ExchangeTypes.FANOUT)
    ))
    public void memberLogin(MemberLoginMsg memberLoginMsg) {

        if (events != null) {
            for (MemberLoginEvent event : events) {
                try {
                    event.memberLogin(memberLoginMsg);
                } catch (Exception e) {
                    logger.error("会员登录出错", e);
                }
            }
        }
    }
}
