/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.aftersale;

import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.security.model.Seller;
import cn.shoptnt.model.aftersale.dto.AfterSaleQueryParam;
import cn.shoptnt.model.aftersale.vo.AfterSaleExportVO;
import cn.shoptnt.model.aftersale.vo.AfterSaleRecordVO;
import cn.shoptnt.model.aftersale.vo.ApplyAfterSaleVO;
import cn.shoptnt.model.aftersale.vo.PutInWarehouseVO;
import cn.shoptnt.model.errorcode.TradeErrorCode;
import cn.shoptnt.model.goods.enums.Permission;
import cn.shoptnt.model.support.LogClient;
import cn.shoptnt.model.support.validator.annotation.Log;
import cn.shoptnt.service.aftersale.AfterSaleManager;
import cn.shoptnt.service.aftersale.AfterSaleQueryManager;
import cn.shoptnt.service.aftersale.SellerCreateTradeManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;

/**
 * 售后服务相关API
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-10-24
 */
@Api(tags= "售后服务相关API")
@RestController
@RequestMapping("/seller/after-sales")
@Validated
public class AfterSaleSellerController {

    @Autowired
    private AfterSaleManager afterSaleManager;

    @Autowired
    private AfterSaleQueryManager afterSaleQueryManager;

    @Autowired
    private SellerCreateTradeManager sellerCreateTradeManager;

    @ApiOperation(value = "获取申请售后服务记录列表", response = AfterSaleRecordVO.class)
    @GetMapping()
    public WebPage list(@Valid AfterSaleQueryParam param){
        param.setSellerId(UserContext.getSeller().getSellerId());

        return afterSaleQueryManager.list(param);
    }

    @ApiOperation(value = "获取售后服务详细信息", response = ApplyAfterSaleVO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "service_sn", value = "售后服务单号", required = true, dataType = "String", dataTypeClass = String.class, paramType = "path")
    })
    @GetMapping(value = "/detail/{service_sn}")
    public ApplyAfterSaleVO detail(@PathVariable("service_sn") String serviceSn){
        ApplyAfterSaleVO detail = afterSaleQueryManager.detail(serviceSn, Permission.SELLER);

        Seller seller = UserContext.getSeller();
        if (!seller.getSellerId().equals(detail.getSellerId())){
            throw new ServiceException(TradeErrorCode.E460.code(), "操作订单无权限");
        }
        return detail;
    }

    @ApiOperation(value = "商家审核售后服务申请")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "service_sn", value = "售后服务单号", required = true, dataType = "String", dataTypeClass = String.class, paramType = "path"),
            @ApiImplicitParam(name = "audit_status", value = "审核状态 PASS：审核通过，REFUSE：审核未通过", required = true, dataType = "String", dataTypeClass = String.class, paramType = "query", allowableValues = "PASS,REFUSE"),
            @ApiImplicitParam(name = "refund_price", value = "退款金额", dataType = "Double", dataTypeClass = Double.class, paramType = "query"),
            @ApiImplicitParam(name = "return_addr", value = "退货地址信息", dataType = "String", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "audit_remark", value = "商家审核备注", dataType = "String", dataTypeClass = String.class, paramType = "query")
    })
    @PostMapping(value = "/audit/{service_sn}")
    @Log(client = LogClient.seller,detail = "审核售后服务单，单号：${serviceSn}")
    public void audit(@PathVariable("service_sn") String serviceSn, @ApiIgnore String auditStatus, @ApiIgnore Double refundPrice, @ApiIgnore String returnAddr, @ApiIgnore String auditRemark){

        this.afterSaleManager.audit(serviceSn, auditStatus, refundPrice, returnAddr, auditRemark);
    }


    @ApiOperation(value = "导出售后服务信息", response = AfterSaleExportVO.class)
    @GetMapping(value = "/export")
    public List<AfterSaleExportVO> export(@Valid AfterSaleQueryParam param){
        param.setSellerId(UserContext.getSeller().getSellerId());
        return afterSaleQueryManager.exportAfterSale(param);
    }

    @ApiOperation(value = "商家为售后服务手动创建新订单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "service_sn", value = "售后服务单号", required = true, dataType = "String", dataTypeClass = String.class, paramType = "path")
    })
    @PostMapping(value = "/create-order/{service_sn}")
    public void createOrder(@PathVariable("service_sn") String serviceSn){

        this.sellerCreateTradeManager.sellerCreateTrade(serviceSn);
    }

    @ApiOperation(value = "商家关闭售后服务单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "service_sn", value = "售后服务单号", required = true, dataType = "String", dataTypeClass = String.class, paramType = "path"),
            @ApiImplicitParam(name = "reason", value = "关闭原因", dataType = "String", dataTypeClass = String.class, paramType = "query")
    })
    @PostMapping(value = "/close/{service_sn}")
    public void close(@PathVariable("service_sn") String serviceSn, @ApiIgnore String reason){

        this.afterSaleManager.closeAfterSale(serviceSn, reason);
    }

    @ApiOperation(value = "商家将申请售后服务退还的商品入库")
    @PostMapping(value = "/put-in/warehouse")
    @Log(client = LogClient.seller,detail = "售后服务单入库，单号：${putInWarehouseVO.serviceSn}")
    public void putInWarehouse(@Valid @RequestBody PutInWarehouseVO putInWarehouseVO){

        this.afterSaleManager.putInWarehouse(putInWarehouseVO.getServiceSn(), putInWarehouseVO.getStorageList(), putInWarehouseVO.getRemark());
    }
}
