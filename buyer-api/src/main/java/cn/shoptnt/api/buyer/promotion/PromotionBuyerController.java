/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.promotion;

import cn.shoptnt.model.promotion.tool.vo.PromotionVO;
import cn.shoptnt.service.promotion.tool.PromotionGoodsManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * 促销活动控制器
 *
 * @author Snow create in 2018/7/10
 * @version v2.0
 * @since v7.0.0
 */

@RestController
@RequestMapping("/promotions")
@Api(tags= "促销活动相关API")
@Validated
public class PromotionBuyerController {

    @Autowired
    private PromotionGoodsManager promotionGoodsManager;

    @ApiOperation(value = "根据商品读取参与的所有活动")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "goods_id", value = "商品ID", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "path")
    })
    @GetMapping("/{goods_id}")
    public List<PromotionVO> getGoods(@ApiIgnore @PathVariable("goods_id") Long goodsId){
        List<PromotionVO> promotionVOList = this.promotionGoodsManager.getPromotion(goodsId);
        return promotionVOList;
    }
}
