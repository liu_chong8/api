/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.shop.goods;

import cn.shoptnt.consumer.core.event.GoodsCommentEvent;
import cn.shoptnt.model.base.message.GoodsCommentMsg;
import cn.shoptnt.client.goods.GoodsClient;
import cn.shoptnt.model.member.dos.MemberComment;
import cn.shoptnt.model.member.enums.CommentTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author fk
 * @version v1.0
 * @Description: 更改商品的评论数量
 * @date 2018/6/25 10:23
 * @since v7.0.0
 */
@Service
public class GoodsCommentNumConsumer implements GoodsCommentEvent {

    @Autowired
    private GoodsClient goodsClient;

    @Override
    public void goodsComment(GoodsCommentMsg goodsCommentMsg) {

        if (goodsCommentMsg.getComment() == null || goodsCommentMsg.getComment().isEmpty()) {
            return;
        }

        //获取操作类型
        int operaType = goodsCommentMsg.getOperaType();
        //获取商品评论集合
        List<MemberComment> commentList = goodsCommentMsg.getComment();

        for (MemberComment comment : commentList) {
            //如果评论不为空并且是初评
            if (comment != null && CommentTypeEnum.INITIAL.name().equals(comment.getCommentsType())) {
                // 如果是新增评论操作则商品评论数量加1，如果是删除评论操作则商品评论数量减1
                if (GoodsCommentMsg.ADD == operaType) {
                    this.goodsClient.updateCommentCount(comment.getGoodsId(), 1);
                } else if (GoodsCommentMsg.DELETE == operaType) {
                    this.goodsClient.updateCommentCount(comment.getGoodsId(), -1);
                }
            }
        }
    }
}
