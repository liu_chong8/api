/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api;

import cn.shoptnt.framework.swagger.AbstractSwagger2;
import cn.shoptnt.framework.swagger.SwaggerConfiguration;
import cn.shoptnt.framework.swagger.SwaggerProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Created by 妙贤 on 2018/3/10.
 * Swagger2配置
 *
 * @author 妙贤
 * @version 1.0
 * @since 7.0.0
 * 2018/3/10
 */

@Configuration
@EnableOpenApi
public class BaseSwagger2 extends SwaggerConfiguration {


    public BaseSwagger2(SwaggerProperties swaggerProperties) {
        super(swaggerProperties);
    }
}
