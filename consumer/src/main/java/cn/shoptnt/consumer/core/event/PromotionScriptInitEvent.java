package cn.shoptnt.consumer.core.event;

/**
 * 促销活动脚本初始化消息接口事件
 * @author dmy
 * 2022-09-23
 */
public interface PromotionScriptInitEvent {

    /**
     * 促销活动脚本初始化
     */
    void scriptInit();

}
