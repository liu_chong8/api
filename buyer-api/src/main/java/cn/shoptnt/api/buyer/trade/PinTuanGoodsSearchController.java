/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.trade;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.promotion.pintuan.PtGoodsDoc;
import cn.shoptnt.service.trade.pintuan.PinTuanSearchManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * Created by 妙贤 on 2019-01-21.
 * 搜索拼团商品
 * @author 妙贤
 * @version 1.0
 * @since 7.0.0
 * 2019-01-21
 */
@Api(tags= "拼团搜索API")
@RestController
@RequestMapping("/pintuan/goods")
public class PinTuanGoodsSearchController {


    @Autowired
    private PinTuanSearchManager pinTuanSearchManager;

    @GetMapping("/skus")
    @ApiOperation(value = "搜索拼团商品")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "category_id", value = "分类id", required = false, dataType = "int", dataTypeClass = Integer.class, paramType = "query"),
            @ApiImplicitParam(name = "page_no", value = "页码", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "query"),
            @ApiImplicitParam(name = "page_size", value = "页大小", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "query")


    })
    public List<PtGoodsDoc> search(@ApiIgnore Long categoryId, @ApiIgnore Integer pageNo, @ApiIgnore Integer pageSize) {
        List<PtGoodsDoc> list  = pinTuanSearchManager.search(categoryId, pageNo, pageSize);
        return  list;
    }


    @GetMapping("/skus/uniapp")
    @ApiOperation(value = "uniapp搜索拼团商品")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "category_id", value = "分类id", required = false, dataType = "int", dataTypeClass = Integer.class, paramType = "query"),
            @ApiImplicitParam(name = "page_no", value = "页码", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "query"),
            @ApiImplicitParam(name = "page_size", value = "页大小", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "query")
    })
    public WebPage searchPage(@ApiIgnore Long categoryId, @ApiIgnore Integer pageNo, @ApiIgnore Integer pageSize) {
        WebPage list  = pinTuanSearchManager.searchPage(categoryId, pageNo, pageSize);
        return  list;
    }

}
