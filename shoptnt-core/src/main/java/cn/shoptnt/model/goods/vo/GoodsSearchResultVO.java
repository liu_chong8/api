package cn.shoptnt.model.goods.vo;

import cn.shoptnt.framework.database.WebPage;
import io.swagger.annotations.ApiModel;

import java.util.Map;

/**
 * 商品搜索回显数据vo
 * @author zhangsong
 * @version v1.0
 * @since v7.3.0
 * 2021-03-17
 */
@ApiModel
public class GoodsSearchResultVO {

    /**
     * 商品分页数据
     */
    private WebPage goodsData;

    /**
     * 商品选择器数据
     */
    private Map selectorData;

    public WebPage getGoodsData() {
        return goodsData;
    }

    public void setGoodsData(WebPage goodsData) {
        this.goodsData = goodsData;
    }

    public Map getSelectorData() {
        return selectorData;
    }

    public void setSelectorData(Map selectorData) {
        this.selectorData = selectorData;
    }

}
