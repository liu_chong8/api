/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.statistics;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.base.SearchCriteria;
import cn.shoptnt.model.statistics.vo.SimpleChart;
import cn.shoptnt.service.statistics.GoodsStatisticManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 商品统计控制器，后台
 *
 * @author chopper
 * @version v1.0
 * @since v7.0
 * 2018-03-23 上午5:19
 */

@RestController
@Api(tags= "后台-》商品统计")
@RequestMapping("/admin/statistics/goods")
public class GoodsStatisticManagerController {

    @Autowired
    protected GoodsStatisticManager goodsStatisticsService;


    @ApiOperation("价格销量统计")
    @GetMapping(value = "/price/sales")
    @ApiImplicitParam(name = "prices", value = "价格区间", required = false, paramType = "query", dataType = "Double", dataTypeClass = Double.class, allowMultiple = true)
    public SimpleChart getPriceSales(SearchCriteria searchCriteria,@RequestParam(required = false) Integer[] prices) {
        return this.goodsStatisticsService.getPriceSales(searchCriteria, prices);
    }

    @ApiOperation("热卖商品按金额统计")
    @GetMapping(value = "/hot/money")
    public SimpleChart getHotSalesMoney(SearchCriteria searchCriteria) {
        return this.goodsStatisticsService.getHotSalesMoney(searchCriteria);
    }

    @ApiOperation("热卖商品按金额统计")
    @GetMapping(value = "/hot/money/page")
    public WebPage getHotSalesMoneyPage(SearchCriteria searchCriteria) {
        return this.goodsStatisticsService.getHotSalesMoneyPage(searchCriteria);
    }

    @ApiOperation("热卖商品按数量统计")
    @GetMapping(value = "/hot/num")
    public SimpleChart getHotSalesNum(SearchCriteria searchCriteria) {
        return this.goodsStatisticsService.getHotSalesNum(searchCriteria);
    }


    @ApiOperation("热卖商品按数量统计")
    @GetMapping(value = "/hot/num/page")
    public WebPage getHotSalesNumPage(SearchCriteria searchCriteria) {
        return this.goodsStatisticsService.getHotSalesNumPage(searchCriteria);
    }

    @ApiOperation("商品销售明细")
    @GetMapping(value = "/sale/details")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "goods_name", value = "商品名字", required = false, paramType = "query", dataType = "String", dataTypeClass = String.class, allowMultiple = false),
            @ApiImplicitParam(name = "page_size", value = "页码大小", required = false, paramType = "query", dataType = "int", dataTypeClass = Integer.class, allowMultiple = false),
            @ApiImplicitParam(name = "page_no", value = "页码", required = false, paramType = "query", dataType = "int", dataTypeClass = Integer.class, allowMultiple = false),
    })
    public WebPage getSaleDetails(SearchCriteria searchCriteria, @ApiIgnore String goodsName, @ApiIgnore Long pageSize, @ApiIgnore Long pageNo) {
        return this.goodsStatisticsService.getSaleDetails(searchCriteria, goodsName, pageSize, pageNo);
    }

    @ApiOperation("商品收藏排行")
    @GetMapping(value = "/collect")
    public SimpleChart getGoodsCollect(SearchCriteria searchCriteria) {
        return this.goodsStatisticsService.getGoodsCollect(searchCriteria);
    }

    @ApiOperation("商品收藏排行")
    @GetMapping(value = "/collect/page")
    public WebPage getGoodsCollectPage(SearchCriteria searchCriteria) {
        return this.goodsStatisticsService.getGoodsCollectPage(searchCriteria);
    }

}
