package cn.shoptnt.api.manager.promotion;

import cn.shoptnt.framework.rabbitmq.MessageSender;
import cn.shoptnt.framework.rabbitmq.MqMessage;
import cn.shoptnt.model.base.rabbitmq.AmqpExchange;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 促销活动脚本初始化相关API
 * @author dmy
 * 2022-09-23
 */
@RestController
@RequestMapping("/admin/promotion/script-init")
@Api(tags= "促销活动脚本初始化相关API")
@Validated
public class PromotionScriptInitManagerController {

    @Autowired
    private MessageSender messageSender;

    @PostMapping
    @ApiOperation(value = "促销活动脚本初始化")
    public void scriptInit() {
        /** 发送促销活动脚本初始化消息 */
        messageSender.send(new MqMessage(AmqpExchange.PROMOTION_SCRIPT_INIT, AmqpExchange.PROMOTION_SCRIPT_INIT+"_ROUTING","1"));
    }
}
