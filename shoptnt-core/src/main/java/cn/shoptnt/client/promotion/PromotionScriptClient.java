/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.promotion;

import cn.shoptnt.model.promotion.coupon.dos.CouponDO;
import cn.shoptnt.model.promotion.fulldiscount.dos.FullDiscountDO;
import cn.shoptnt.model.promotion.groupbuy.dos.GroupbuyActiveDO;
import cn.shoptnt.model.promotion.halfprice.dos.HalfPriceDO;
import cn.shoptnt.model.promotion.minus.dos.MinusDO;
import cn.shoptnt.model.promotion.pintuan.Pintuan;
import cn.shoptnt.model.promotion.seckill.dos.SeckillApplyDO;
import cn.shoptnt.model.promotion.seckill.dos.SeckillDO;
import cn.shoptnt.model.promotion.tool.dos.PromotionGoodsDO;

import java.util.List;

/**
 * 促销脚本客户端
 *
 * @author fk
 * @version v7.0
 * @date 19/3/28 上午11:10
 * @since v7.0
 */
public interface PromotionScriptClient {

    /**
     * 创建参与团购促销活动商品的脚本数据信息
     * @param promotionId 团购促销活动ID
     * @param goodsList 参与团购促销活动的商品集合
     */
    void createGroupBuyCacheScript(Long promotionId, List<PromotionGoodsDO> goodsList);

    /**
     * 删除商品存放在缓存中的团购促销活动相关的脚本数据信息
     * @param promotionId 团购促销活动ID
     * @param goodsList 参与团购促销活动的商品集合
     */
    void deleteGroupBuyCacheScript(Long promotionId, List<PromotionGoodsDO> goodsList);

    /**
     * 删除商品存放在缓存中的限时抢购促销活动相关的脚本数据信息
     * @param promotionId 限时抢购促销活动ID
     * @param goodsList 参与限时抢购促销活动的商品集合
     */
    void deleteCacheScript(Long promotionId, List<SeckillApplyDO> goodsList);

    /**
     * 获取所有未开始和进行中的满减促销活动
     * @return
     */
    List<FullDiscountDO> selectNoEndFullDiscount();

    /**
     * 获取所有未开始和进行中的单品立减活动信息
     * @return
     */
    List<MinusDO> selectNoEndMinus();

    /**
     * 获取所有未开始和进行中的第二件半价活动信息
     * @return
     */
    List<HalfPriceDO> selectNoEndHalfPrice();

    /**
     * 获取所有未开始和进行中的团购活动信息
     * @return
     */
    List<GroupbuyActiveDO> selectNoEndGroupbuy();

    /**
     * 获取所有未开始和进行中的限时抢购促销活动信息
     * @return
     */
    List<SeckillDO> selectNoEndSeckill();

    /**
     * 获取参与限时抢购并且已审核通过的商品信息
     * @param seckillId 限时抢购ID
     * @return
     */
    List<SeckillApplyDO> selectGoodsBySeckillId(Long seckillId);

    /**
     * 创建参与限时抢购促销活动商品的脚本数据信息
     * @param seckillId 限时抢购活动ID
     * @param goodsList 参与限时抢购促销活动的商品集合
     */
    void createCacheScript(Long seckillId, List<SeckillApplyDO> goodsList);

    /**
     * 获取所有未开始和进行中的拼团促销活动信息
     * @return
     */
    List<Pintuan> selectNoEndPintuan();

    /**
     * 获取所有未过期的优惠券信息
     * @return
     */
    List<CouponDO> selectNoEndCoupon();
}
