/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.shop.member;

import cn.shoptnt.client.member.MemberHistoryReceiptClient;
import cn.shoptnt.client.trade.OrderClient;
import cn.shoptnt.consumer.core.event.OrderStatusChangeEvent;
import cn.shoptnt.consumer.core.event.TradeIntoDbEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import cn.shoptnt.framework.util.BeanUtil;
import cn.shoptnt.framework.util.DateUtil;
import cn.shoptnt.framework.util.JsonUtil;
import cn.shoptnt.model.base.message.OrderStatusChangeMsg;
import cn.shoptnt.model.member.dos.ReceiptHistory;
import cn.shoptnt.model.member.enums.ReceiptTypeEnum;
import cn.shoptnt.model.member.vo.ReceiptHistoryVO;
import cn.shoptnt.model.trade.order.dos.OrderDO;
import cn.shoptnt.model.trade.order.dto.OrderDTO;
import cn.shoptnt.model.trade.order.enums.OrderStatusEnum;
import cn.shoptnt.model.trade.order.vo.ConsigneeVO;
import cn.shoptnt.model.trade.order.vo.TradeVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 修改会员开票历史记录消费者
 *
 * @author duanmingyu
 * @version v7.1.4
 * @since v7.0.0
 * 2019-06-24
 */
@Service
public class MemberReceiptStatusConsumer implements OrderStatusChangeEvent,TradeIntoDbEvent{

    @Autowired
    private MemberHistoryReceiptClient memberHistoryReceiptClient;

    @Autowired
    private OrderClient orderClient;

    private final Logger logger = LoggerFactory.getLogger(getClass());


    @Override
    public void orderChange(OrderStatusChangeMsg orderMessage) {

        //获取订单信息
        OrderDO order = orderMessage.getOrderDO();

        //发货
        if (orderMessage.getNewStatus().name().equals(OrderStatusEnum.SHIPPED.name())) {

            //如果订单需要开具发票
            if (order.getNeedReceipt().intValue() == 1) {
                ReceiptHistoryVO receiptHistoryVO = memberHistoryReceiptClient.getReceiptHistory(order.getSn());
                //如果订单开具的是增值税普通发票
                if (ReceiptTypeEnum.VATORDINARY.value().equals(receiptHistoryVO.getReceiptType())) {
                    ReceiptHistory receiptHistory = new ReceiptHistory();
                    BeanUtil.copyProperties(receiptHistoryVO, receiptHistory);

                    //设置普通发票物流信息并将发票记录状态设置为已开票
                    receiptHistory.setLogiId(order.getLogiId());
                    receiptHistory.setLogiName(order.getLogiName());
                    receiptHistory.setLogiCode(order.getShipNo());
                    receiptHistory.setStatus(1);
                    this.memberHistoryReceiptClient.edit(receiptHistory);

                    logger.debug("发票信息开票完成");
                }
            }
        }

    }

    @Override
    public void onTradeIntoDb(TradeVO tradeVO) {
        //从交易中获取订单列表
        List<OrderDTO> orderDTOS = tradeVO.getOrderList();
        //循环订单取出发票信息
        for (OrderDTO orderDTO : orderDTOS) {
            //如果订单需要开具发票
            if (orderDTO.getNeedReceipt().intValue() == 1) {
                ReceiptHistory receiptHistory = orderDTO.getReceiptHistory();
                if (receiptHistory != null && receiptHistory.getReceiptTitle() != null && receiptHistory.getReceiptType() != null) {
                    receiptHistory.setHistoryId(null);
                    receiptHistory.setAddTime(DateUtil.getDateline());
                    receiptHistory.setMemberId(orderDTO.getMemberId());
                    receiptHistory.setUname(orderDTO.getMemberName());
                    receiptHistory.setOrderSn(orderDTO.getSn());
                    receiptHistory.setOrderPrice(orderDTO.getOrderPrice());
                    receiptHistory.setSellerId(orderDTO.getSellerId());
                    receiptHistory.setSellerName(orderDTO.getSellerName());
                    //绑定此发票的订单的出库状态
                    receiptHistory.setOrderStatus(OrderStatusEnum.CONFIRM.value());
                    receiptHistory.setStatus(0);
                    //订单中的商品数据
                    receiptHistory.setGoodsJson(JsonUtil.objectToJson(orderDTO.getOrderSkuList()));
                    //如果订单开具的是增值税普通发票,则设置发票的地址
                    if (ReceiptTypeEnum.VATORDINARY.value().equals(receiptHistory.getReceiptType())) {

                        ConsigneeVO consignee = orderDTO.getConsignee();
                        receiptHistory.setMemberName(consignee.getName());
                        receiptHistory.setMemberMobile(consignee.getMobile());
                        receiptHistory.setProvinceId(consignee.getProvinceId());
                        receiptHistory.setCityId(consignee.getCityId());
                        receiptHistory.setCountyId(consignee.getCountyId());
                        receiptHistory.setTownId(consignee.getTownId());
                        receiptHistory.setProvince(consignee.getProvince());
                        receiptHistory.setCity(consignee.getCity());
                        receiptHistory.setCounty(consignee.getCounty());
                        receiptHistory.setTown(consignee.getTown());
                        receiptHistory.setDetailAddr(consignee.getAddress());
                    }
                    memberHistoryReceiptClient.add(receiptHistory);
                    logger.debug("发票信息入库完成");
                }
            }
        }
    }
}
