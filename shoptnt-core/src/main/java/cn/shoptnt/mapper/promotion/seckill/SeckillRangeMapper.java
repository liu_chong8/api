/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.mapper.promotion.seckill;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.shoptnt.framework.cache.MybatisRedisCache;
import cn.shoptnt.model.promotion.seckill.dos.SeckillDO;
import cn.shoptnt.model.promotion.seckill.dos.SeckillRangeDO;
import cn.shoptnt.model.promotion.seckill.dto.SeckillQueryParam;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 限时抢购时刻mapper
 * @author zs
 * @version v1.0
 * @since v7.2.2
 * 2020-08-11
 */
//@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface SeckillRangeMapper extends BaseMapper<SeckillRangeDO> {

    /**
     * 查询秒杀时刻列表
     * @param today 今天的时间
     * @return
     */
    List<SeckillRangeDO> selectReadTimeList(long today);
}
