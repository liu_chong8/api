/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.member;


import cn.shoptnt.client.system.ValidatorClient;
import cn.shoptnt.framework.ShopTntConfig;
import cn.shoptnt.model.base.CharacterConstant;
import cn.shoptnt.model.base.rabbitmq.AmqpExchange;
import cn.shoptnt.model.errorcode.MemberErrorCode;
import cn.shoptnt.model.member.dos.Member;
import cn.shoptnt.model.member.dto.MemberEditDTO;
import cn.shoptnt.model.member.dto.MemberStatisticsDTO;
import cn.shoptnt.service.member.MemberManager;
import cn.shoptnt.model.util.sensitiveutil.SensitiveFilter;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.exception.ResourceNotFoundException;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.security.model.Buyer;
import cn.shoptnt.framework.util.BeanUtil;
import cn.shoptnt.framework.util.StringUtil;
import cn.shoptnt.service.passport.PassportManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import cn.shoptnt.framework.rabbitmq.MessageSender;
import cn.shoptnt.framework.rabbitmq.MqMessage;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


/**
 * 会员控制器
 *
 * @author zh
 * @version v2.0
 * @since v7.0.0
 * 2018-03-16 11:33:56
 */
@RestController
@RequestMapping("/members")
@Api(tags= "会员相关API")
public class MemberBuyerController {

    @Autowired
    private MemberManager memberManager;
    @Autowired
    private MessageSender messageSender;
    @Autowired
    private ValidatorClient validatorClient;

    @Autowired
    private PassportManager passportManager;

    @Autowired
    private ShopTntConfig shopTntConfig;


    @PutMapping
    @ApiOperation(value = "完善会员细信息", response = Member.class)
    public Member perfectInfo(@Valid MemberEditDTO memberEditDTO) {
        Buyer buyer = UserContext.getBuyer();
        Member member = memberManager.getModel(buyer.getUid());
        //判断数据库是否存在此会员
        if (member == null) {
            throw new ResourceNotFoundException("此会员不存在");
        }
        //过滤用户昵称敏感词汇
        memberEditDTO.setNickname(SensitiveFilter.filter(memberEditDTO.getNickname(), CharacterConstant.WILDCARD_STAR));
        //复制属性
        BeanUtil.copyProperties(memberEditDTO, member);
        if (memberEditDTO.getRegion() != null) {
            if (StringUtil.isEmpty(memberEditDTO.getRegion().getCounty())) {
                throw new ServiceException(MemberErrorCode.E141.code(), "地区不合法");
            }
            BeanUtil.copyProperties(memberEditDTO.getRegion(), member);
        }
        //判断会员是修改资料还是完善资料
        if (member.getInfoFull() != null && !member.getInfoFull().equals(1)) {
            member.setInfoFull(1);
            this.messageSender.send(new MqMessage(AmqpExchange.MEMBER_INFO_COMPLETE, "member-info-complete-routingkey", member.getMemberId()));
        }
        member.setFace(memberEditDTO.getFace());
        member.setTel(memberEditDTO.getTel());
        //修改会员信息
        this.memberManager.edit(member, buyer.getUid());
        //发送会员资料变化消息
        this.messageSender.send(new MqMessage(AmqpExchange.MEMBER_INFO_CHANGE, AmqpExchange.MEMBER_INFO_CHANGE + "_ROUTING", member.getMemberId()));
        return member;
    }


    @GetMapping
    @ApiOperation(value = "查询当前会员信息")
    public Member get() {
        return this.memberManager.getMember(UserContext.getBuyer().getUid());
    }

    @ApiOperation(value = "注销会员登录")
    @PostMapping(value = "/logout")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "uid", value = "会员id", dataType = "int", dataTypeClass = Integer.class, paramType = "query", required = true)
    })
    public String loginOut(@NotNull(message = "会员id不能为空") Long uid) {
        this.memberManager.logout();
        return "";
    }

    @GetMapping("/statistics")
    @ApiOperation(value = "统计当前会员的一些数据")
    public MemberStatisticsDTO getMemberStatistics() {
        return this.memberManager.getMemberStatistics();
    }


    @PostMapping(value = "/log-off/send")
    @Operation(summary =  "发送注销申请短信验证码")
    public String sendLogOffCode() {
        // 参数验证（验证图片验证码或滑动验证参数等）
        this.validatorClient.validate();
        Buyer buyer = UserContext.getBuyer();
        passportManager.sendLogOffCode(buyer.getUid());
        return shopTntConfig.getSmscodeTimout() / 60 + "";
    }

    @PostMapping("/log-off")
    @Operation(summary =  "用户注销")
    @Parameters({
            @Parameter(name = "sms_code", description = "手机验证码",  required = true ,  in= ParameterIn.QUERY)
    })
    public void logOff( @Parameter(hidden = true) @NotEmpty(message = "短信验证码不能为空") String smsCode) {
        memberManager.logOff(smsCode);
    }
}
