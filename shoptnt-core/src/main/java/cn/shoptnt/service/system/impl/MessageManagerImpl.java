/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.system.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.util.PageConvert;
import cn.shoptnt.mapper.system.MessageMapper;
import cn.shoptnt.model.base.rabbitmq.AmqpExchange;
import cn.shoptnt.model.errorcode.MemberErrorCode;
import cn.shoptnt.model.system.dos.Message;
import cn.shoptnt.model.system.dto.MessageDTO;
import cn.shoptnt.model.system.dto.MessageQueryParam;
import cn.shoptnt.model.system.vo.MessageVO;
import cn.shoptnt.service.system.MessageManager;
import cn.shoptnt.framework.context.user.AdminUserContext;
import cn.shoptnt.framework.database.DaoSupport;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.util.BeanUtil;
import cn.shoptnt.framework.util.DateUtil;
import cn.shoptnt.framework.util.StringUtil;
import cn.shoptnt.framework.rabbitmq.MessageSender;
import cn.shoptnt.framework.rabbitmq.MqMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 站内消息业务类
 *
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-07-04 21:50:52
 */
@Service
public class MessageManagerImpl implements MessageManager {

    @Autowired
    private MessageSender messageSender;
    @Autowired
    private MessageMapper messageMapper;

    /**
     * 查询站内消息列表
     *
     * @param param 搜索条件
     * @return WebPage
     */
    @Override
    public WebPage list(MessageQueryParam param) {
        IPage<MessageDTO> webPage = messageMapper.queryMessageDTOList(new Page<>(param.getPageNo(), param.getPageSize()),param);
        return PageConvert.convert(webPage);
    }

    /**
     * 添加站内消息
     *
     * @param messageVO 站内消息
     * @return Message 站内消息
     */
    @Override
    @Transactional(value = "systemTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public Message add(MessageVO messageVO) {
        if (messageVO.getSendType().equals(1)) {
            if (StringUtil.isEmpty(messageVO.getMemberIds())) {
                throw new ServiceException(MemberErrorCode.E122.code(), "请指定发送会员");
            }
        }
        Message message = new Message();
        BeanUtil.copyProperties(messageVO, message);
        message.setAdminId(AdminUserContext.getAdmin().getUid());
        message.setAdminName(AdminUserContext.getAdmin().getUsername());
        message.setSendTime(DateUtil.getDateline());
        message.setDisabled(0);

        messageMapper.insert(message);
        Long id = message.getId();
        this.messageSender.send(new MqMessage(AmqpExchange.MEMBER_MESSAGE, AmqpExchange.MEMBER_MESSAGE + "_ROUTING", id));
        return message;
    }

    /**
     * 通过id查询站内消息
     *
     * @param id 消息id
     * @return 站内消息对象
     */
    @Override
    public Message get(Long id) {
        return messageMapper.selectById(id);
    }
}
