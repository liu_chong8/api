/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.receiver;


import cn.shoptnt.consumer.core.event.PintuanSuccessEvent;
import cn.shoptnt.model.base.rabbitmq.AmqpExchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 拼团消息
 *
 * @author fk
 * @version v2.0
 * @since v7.1.4 2019年6月20日 上午10:31:49
 */
@Component
public class PintuanReceiver {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<PintuanSuccessEvent> events;

    /**
     * 拼团成功消息
     * @param pintuanOrderId
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.PINTUAN_SUCCESS + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.PINTUAN_SUCCESS, type = ExchangeTypes.FANOUT)
    ))
    public void  receiveMessage(Long pintuanOrderId){

        if (events != null) {
            for (PintuanSuccessEvent event : events) {
                try {
                    event.success(pintuanOrderId);
                } catch (Exception e) {
                    logger.error("拼团成功消息", e);
                }
            }
        }

    }

}
