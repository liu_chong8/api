<#--
 验证促销活动是否在有效期内
 @param promotionActive 活动信息对象(内置常量)
        .startTime 获取开始时间
        .endTime 活动结束时间
 @param $currentTime 当前时间(变量)
 @returns {boolean}
 -->
function validTime(){
    if (${promotionActive.startTime} <= $currentTime && $currentTime <= ${promotionActive.endTime}) {
        return true;
    }
    return false;
}

<#--
 第二件半价促销活动金额计算
 @param $sku 商品SKU信息对象(变量)
        .$price 商品SKU单价
        .$num 商品数量
 @returns {*}
 -->
function countPrice() {
    var price = $sku.$price;
    <#--获取半价的数量-->
    var halfNum = parseInt($sku.$num / 2) ;
    <#--获取全价的数量-->
    var fullNum = $sku.$num - halfNum;
    <#--第二件半价金额 = 半价数量*半价 + 全价数量*全价-->
    return halfNum * (price / 2) + fullNum * price;
}

