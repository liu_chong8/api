/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.statistics;

import cn.shoptnt.model.base.SearchCriteria;
import cn.shoptnt.model.statistics.vo.SimpleChart;
import cn.shoptnt.service.statistics.PageViewStatisticManager;
import cn.shoptnt.framework.context.user.UserContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;

/**
 * 商家中心 流量分析
 *
 * @author mengyuanming
 * @version 2.0
 * @since 7.0
 * 2018年3月19日上午8:35:47
 */

@Api(tags= "商家统计 流量分析")
@RestController
@RequestMapping("/seller/statistics/page_view")
public class PageViewStatisticSellerController {

    @Autowired
    private PageViewStatisticManager pageViewStatisticManager;

    @ApiOperation(value = "获取店铺访问量数据", response = SimpleChart.class)
    @GetMapping("/shop")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "cycle_type", value = "周期类型", dataType = "String", dataTypeClass = String.class, paramType = "query",required = true,allowableValues = "YEAR,MONTH"),
            @ApiImplicitParam(name = "year", value = "年份，默认当前年份", dataType = "int", dataTypeClass = Integer.class, paramType = "query"),
            @ApiImplicitParam(name = "month", value = "月份，默认当前月份", dataType = "int", dataTypeClass = Integer.class, paramType = "query")})
    public SimpleChart getShop(@Valid @ApiIgnore SearchCriteria searchCriteria) {

        // 获取商家id
        searchCriteria.setSellerId(UserContext.getSeller().getSellerId());
        return this.pageViewStatisticManager.countShop(searchCriteria);
    }

    @ApiOperation(value = "获取商品访问量数据，只取前30", response = SimpleChart.class)
    @GetMapping("/goods")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "cycle_type", value = "周期类型", dataType = "String", dataTypeClass = String.class, paramType = "query",required = true,allowableValues = "YEAR,MONTH"),
            @ApiImplicitParam(name = "year", value = "年份，默认当前年份", dataType = "int", dataTypeClass = Integer.class, paramType = "query"),
            @ApiImplicitParam(name = "month", value = "月份，默认当前月份", dataType = "int", dataTypeClass = Integer.class, paramType = "query")})
    public SimpleChart getGoods(@Valid @ApiIgnore SearchCriteria searchCriteria) {

        // 获取商家id
        searchCriteria.setSellerId(UserContext.getSeller().getSellerId());

        return this.pageViewStatisticManager.countGoods(searchCriteria);
    }

}
