/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.member;

import cn.shoptnt.model.member.dos.ReceiptAddressDO;
import cn.shoptnt.service.member.ReceiptAddressManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 会员收票地址API
 *
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-09-16
 */
@Api(tags= "会员收票地址API")
@RestController
@RequestMapping("/members/receipt/address")
@Validated
public class ReceiptAddressBuyerController {

    @Autowired
    private ReceiptAddressManager receiptAddressManager;

    @ApiOperation(value = "新增会员收票地址", response = ReceiptAddressDO.class)
    @PostMapping
    public ReceiptAddressDO add(@Valid ReceiptAddressDO receiptAddressDO) {

        return receiptAddressManager.add(receiptAddressDO);
    }

    @ApiOperation(value = "修改会员收票地址", response = ReceiptAddressDO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "path")
    })
    @PutMapping(value = "/{id}")
    public ReceiptAddressDO edit(@Valid ReceiptAddressDO receiptAddressDO, @PathVariable Long id) {

        return receiptAddressManager.edit(receiptAddressDO, id);
    }

    @ApiOperation(value = "查看会员收票地址详细")
    @GetMapping(value = "/detail")
    public ReceiptAddressDO get() {
        ReceiptAddressDO receiptAddressDO = receiptAddressManager.get();
        return receiptAddressDO;
    }
}
