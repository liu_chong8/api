/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.security;

/**
 * 篡改状态枚举
 * @author 妙贤
 * @data 2021/11/20 11:03
 * @version 1.0
 **/
public enum FalsifyStatusEnum {

    /**
     * 已修复
     */
    repaired("已修复"),
    /**
     * 未修复
     */
    unrepaired("未修复");

    private String statusName;

    FalsifyStatusEnum(String name) {
        this.statusName=name;
    }

}
