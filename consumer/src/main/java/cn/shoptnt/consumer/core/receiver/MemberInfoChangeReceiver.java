/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.receiver;

import cn.shoptnt.consumer.core.event.MemberInfoChangeEvent;
import cn.shoptnt.model.base.rabbitmq.AmqpExchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 修改会员资料
 *
 * @author zh
 * @version v7.0
 * @date 18/12/26 下午4:17
 * @since v7.0
 */
@Component
public class MemberInfoChangeReceiver {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<MemberInfoChangeEvent> events;


    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.MEMBER_INFO_CHANGE + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.MEMBER_INFO_CHANGE, type = ExchangeTypes.FANOUT)
    ))
    public void createIndexPage(Long memberId) {
        if (events != null) {
            for (MemberInfoChangeEvent event : events) {
                try {
                    event.memberInfoChange(memberId);
                } catch (Exception e) {
                    logger.error("处理会员资料变化消息出错", e);
                }
            }
        }

    }
}
