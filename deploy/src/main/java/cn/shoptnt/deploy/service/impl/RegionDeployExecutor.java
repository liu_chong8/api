/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.deploy.service.impl;

import cn.shoptnt.deploy.service.DeployExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by 妙贤 on 2018-12-28.
 * 地区数据部署
 * @author 妙贤
 * @version 1.0
 * @since 7.0.0
 * 2018-12-28
 */
@Service
public class RegionDeployExecutor implements DeployExecutor {

    @Autowired
    private  DataBaseDeployExecutor dataBaseDeployExecutor;

    @Override
    public void deploy(Long deployId) {

        dataBaseDeployExecutor.importRegionSQl(deployId);

    }

    @Override
    public String getType() {
        return "region";
    }
}
