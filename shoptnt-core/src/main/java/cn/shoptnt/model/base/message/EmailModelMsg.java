/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.base.message;

import java.io.Serializable;

/**
 * 邮件内容
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018年3月23日 上午10:37:22
 */
public class EmailModelMsg implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 652862137182690060L;

}
