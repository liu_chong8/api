/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.shop.member;

import cn.shoptnt.consumer.core.event.MemberRegisterEvent;
import cn.shoptnt.model.base.message.MemberRegisterMsg;
import cn.shoptnt.client.member.DepositeClient;
import cn.shoptnt.model.member.dos.Member;
import cn.shoptnt.model.member.dos.MemberWalletDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @description: 会员钱包消费者
 * @author: liuyulei
 * @create: 2019-12-31 11:08
 * @version:1.0
 * @since:7.1.4
 **/
@Service
public class MemberWalletConsumer implements MemberRegisterEvent {

    @Autowired
    private DepositeClient depositeClient;

    /**
     * 新注册会员，默认添加会员钱包信息
     * @param memberRegisterMsg
     */
    @Override
    public void memberRegister(MemberRegisterMsg memberRegisterMsg) {
        this.checkWallet(memberRegisterMsg.getMember());
    }

    /**
     * 检测会员钱包是否存在，如果不存在，则添加会员钱包信息
     * @param member
     */
    private void checkWallet(Member member) {
        //检测会员钱包是否存在
        MemberWalletDO walletDO = this.depositeClient.getModel(member.getMemberId());
        if(walletDO == null){
            //如果不存在则添加会员钱包信息
            walletDO = new MemberWalletDO(member.getMemberId(),member.getUname());
            this.depositeClient.add(walletDO);
        }
    }
}
