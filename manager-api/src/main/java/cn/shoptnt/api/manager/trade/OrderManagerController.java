/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.trade;

import cn.shoptnt.client.member.MemberHistoryReceiptClient;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.util.CurrencyUtil;
import cn.shoptnt.handler.AdminTwoStepAuthentication;
import cn.shoptnt.model.support.LogClient;
import cn.shoptnt.model.support.validator.annotation.Log;
import cn.shoptnt.model.trade.cart.dos.OrderPermission;
import cn.shoptnt.model.trade.order.dos.OrderLogDO;
import cn.shoptnt.model.trade.order.dto.OrderQueryParam;
import cn.shoptnt.model.trade.order.vo.CancelVO;
import cn.shoptnt.model.trade.order.vo.OrderDetailVO;
import cn.shoptnt.model.trade.order.vo.OrderLineVO;
import cn.shoptnt.service.trade.order.OrderLogManager;
import cn.shoptnt.service.trade.order.OrderOperateManager;
import cn.shoptnt.service.trade.order.OrderQueryManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * 平台订单API
 *
 * @author Snow create in 2018/6/13
 * @version v2.0
 * @since v7.0.0
 */
@Api(tags= "平台订单API")
@RestController
@RequestMapping("/admin/trade/orders")
@Validated
public class OrderManagerController {

    @Autowired
    private OrderQueryManager orderQueryManager;

    @Autowired
    private OrderOperateManager orderOperateManager;

    @Autowired
    private OrderLogManager orderLogManager;

    @Autowired
    private MemberHistoryReceiptClient memberHistoryReceiptClient;

    @Autowired
    private AdminTwoStepAuthentication adminTwoStepAuthentication;


    @ApiOperation(value = "查询订单列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "order_sn", value = "订单编号", dataType = "String", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "ship_name", value = "收货人", dataType = "String", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "goods_name", value = "商品名称", dataType = "String", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "buyer_name", value = "买家名字", dataType = "String", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "start_time", value = "开始时间", dataType = "long", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "end_time", value = "结束时间", dataType = "long", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "seller_id", value = "店铺ID", dataType = "int", dataTypeClass = Integer.class, paramType = "query"),
            @ApiImplicitParam(name = "order_status", value = "订单状态", dataType = "String", dataTypeClass = String.class, paramType = "query",
                    allowableValues = "ALL,WAIT_PAY,WAIT_SHIP,WAIT_ROG,CANCELLED,COMPLETE,WAIT_COMMENT,REFUND",
                    example = "ALL:所有订单,WAIT_PAY:待付款,WAIT_SHIP:待发货,WAIT_ROG:待收货," +
                            "CANCELLED:已取消,COMPLETE:已完成,WAIT_COMMENT:待评论,REFUND:售后中"),
            @ApiImplicitParam(name = "member_id", value = "会员ID", dataType = "int", dataTypeClass = Integer.class, paramType = "query"),
            @ApiImplicitParam(name = "page_no", value = "页数", dataType = "int", dataTypeClass = Integer.class, paramType = "query"),
            @ApiImplicitParam(name = "page_size", value = "条数", dataType = "int", dataTypeClass = Integer.class, paramType = "query"),
            @ApiImplicitParam(name = "payment_type", value = "支付方式", dataType = "String", dataTypeClass = String.class, paramType = "query",
                    allowableValues = "ONLINE,COD", example = "ONLINE:在线支付,COD:货到付款"),
            @ApiImplicitParam(name = "client_type", value = "订单来源", dataType = "String", dataTypeClass = String.class, paramType = "query",
                    allowableValues = "PC,WAP,NATIVE,REACT,MINI", example = "PC:pc客户端,WAP:WAP客户端,NATIVE:原生APP,REACT:RNAPP,MINI:小程序")
    })
    @GetMapping()
    public WebPage list(@ApiIgnore String orderSn, @ApiIgnore String shipName, @ApiIgnore String goodsName, @ApiIgnore String buyerName,
                        @ApiIgnore Long startTime, @ApiIgnore Long endTime, @ApiIgnore String orderStatus,
                        @ApiIgnore Long pageNo, @ApiIgnore Long pageSize, @ApiIgnore Long memberId, @ApiIgnore Long sellerId,
                        @ApiIgnore String paymentType, @ApiIgnore String clientType) {

        OrderQueryParam param = new OrderQueryParam();
        param.setOrderSn(orderSn);
        param.setShipName(shipName);
        param.setGoodsName(goodsName);
        param.setBuyerName(buyerName);
        param.setOrderStatus(orderStatus);
        param.setStartTime(startTime);
        param.setEndTime(endTime);
        param.setPageNo(pageNo);
        param.setPageSize(pageSize);
        param.setMemberId(memberId);
        param.setSellerId(sellerId);
        param.setPaymentType(paymentType);
        param.setClientType(clientType);

        WebPage page = this.orderQueryManager.list(param);
        return page;
    }


    @ApiOperation(value = "导出订单列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "order_sn", value = "订单编号", dataType = "String", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "ship_name", value = "收货人", dataType = "String", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "goods_name", value = "商品名称", dataType = "String", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "buyer_name", value = "买家名字", dataType = "String", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "start_time", value = "开始时间", dataType = "long", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "end_time", value = "结束时间", dataType = "long", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "seller_id", value = "店铺ID", dataType = "int", dataTypeClass = Integer.class, paramType = "query"),
            @ApiImplicitParam(name = "order_status", value = "订单状态", dataType = "String", dataTypeClass = String.class, paramType = "query",
                    allowableValues = "ALL,WAIT_PAY,WAIT_SHIP,WAIT_ROG,CANCELLED,COMPLETE,WAIT_COMMENT,REFUND",
                    example = "ALL:所有订单,WAIT_PAY:待付款,WAIT_SHIP:待发货,WAIT_ROG:待收货," +
                            "CANCELLED:已取消,COMPLETE:已完成,WAIT_COMMENT:待评论,REFUND:售后中"),
            @ApiImplicitParam(name = "payment_type", value = "支付方式", dataType = "String", dataTypeClass = String.class, paramType = "query",
                    allowableValues = "ONLINE,COD", example = "ONLINE:在线支付,COD:货到付款"),
            @ApiImplicitParam(name = "client_type", value = "订单来源", dataType = "String", dataTypeClass = String.class, paramType = "query",
                    allowableValues = "PC,WAP,NATIVE,REACT,MINI", example = "PC:pc客户端,WAP:WAP客户端,NATIVE:原生APP,REACT:RNAPP,MINI:小程序")
    })
    @GetMapping("/export")
    public List<OrderLineVO> export(@ApiIgnore String orderSn, @ApiIgnore String shipName, @ApiIgnore String goodsName, @ApiIgnore String buyerName,
                       @ApiIgnore Long startTime, @ApiIgnore Long endTime, @ApiIgnore String orderStatus, @ApiIgnore Long sellerId,
                       @ApiIgnore String paymentType, @ApiIgnore String clientType) {
        //调用二次验证
        this.adminTwoStepAuthentication.sensitive();

        OrderQueryParam param = new OrderQueryParam();
        param.setOrderSn(orderSn);
        param.setShipName(shipName);
        param.setGoodsName(goodsName);
        param.setBuyerName(buyerName);
        param.setOrderStatus(orderStatus);
        param.setStartTime(startTime);
        param.setEndTime(endTime);
        param.setSellerId(sellerId);
        param.setPaymentType(paymentType);
        param.setClientType(clientType);

        List<OrderLineVO> lineList = this.orderQueryManager.export(param);
        double pay = 0.0;
        for (OrderLineVO orderLineVO : lineList) {
            if (orderLineVO.getBalance() != null) {
                pay = 0.0;
                if (orderLineVO.getPayMoney() == null) {
                    pay = orderLineVO.getBalance();
                } else {
                    pay = CurrencyUtil.sub(orderLineVO.getPayMoney(), orderLineVO.getBalance());
                }
                orderLineVO.setPayMoney(pay);
            }
        }
        return lineList;
    }


    @ApiOperation(value = "查询单个订单明细")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "order_sn", value = "订单编号", required = true, dataType = "String", dataTypeClass = String.class, paramType = "path")
    })
    @GetMapping(value = "/{order_sn}")
    public OrderDetailVO get(@ApiIgnore @PathVariable("order_sn") String orderSn) {
        OrderDetailVO detailVO = this.orderQueryManager.getModel(orderSn, null);
        if (detailVO.getNeedReceipt().intValue() == 1) {
            detailVO.setReceiptHistory(memberHistoryReceiptClient.getReceiptHistory(orderSn));
        }

        return detailVO;
    }


    @ApiOperation(value = "确认收款")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "order_sn", value = "订单编号", required = true, dataType = "String", dataTypeClass = String.class, paramType = "path"),
            @ApiImplicitParam(name = "pay_price", value = "付款金额", dataType = "Double", dataTypeClass = Double.class, paramType = "query")
    })
    @PostMapping(value = "/{order_sn}/pay")
    @Log(client = LogClient.admin,detail = "订单收款，订单号：${orderSn}")
    public String payOrder(@ApiIgnore @PathVariable("order_sn") String orderSn, @ApiIgnore Double payPrice) {
        this.orderOperateManager.payOrder(orderSn, payPrice, "",OrderPermission.admin);
        return "";
    }


    @ApiOperation(value = "取消订单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "order_sn", value = "订单编号", required = true, dataType = "String", dataTypeClass = String.class, paramType = "path"),
    })
    @PostMapping(value = "/{order_sn}/cancelled")
    @Log(client = LogClient.admin,detail = "订单取消，订单号：${orderSn}")
    public String cancelledOrder(@ApiIgnore @PathVariable("order_sn") String orderSn) {

        CancelVO cancelVO = new CancelVO();
        cancelVO.setReason("管理员取消");
        cancelVO.setOrderSn(orderSn);
        cancelVO.setOperator("平台管理员");

        this.orderOperateManager.cancel(cancelVO, OrderPermission.admin);
        return "";
    }


    @ApiOperation(value = "查询订单日志")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "order_sn", value = "订单编号", required = true, dataType = "String", dataTypeClass = String.class, paramType = "path")
    })
    @GetMapping(value = "/{order_sn}/log")
    public List<OrderLogDO> getList(@ApiIgnore @PathVariable("order_sn") String orderSn) {
        List<OrderLogDO> logDOList = this.orderLogManager.listAll(orderSn);
        return logDOList;
    }

}
