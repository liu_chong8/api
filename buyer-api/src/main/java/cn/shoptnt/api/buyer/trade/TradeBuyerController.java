/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.trade;

import cn.shoptnt.client.system.SettingClient;
import cn.shoptnt.framework.util.JsonUtil;
import cn.shoptnt.model.base.SettingGroup;
import cn.shoptnt.model.trade.cart.enums.CheckedWay;
import cn.shoptnt.model.trade.order.vo.OrderSettingVO;
import cn.shoptnt.model.trade.order.vo.TradeVO;
import cn.shoptnt.service.trade.order.TradeManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 交易控制器
 *
 * @author Snow create in 2018/5/8
 * @version v2.0
 * @since v7.0.0
 */
@Api(tags= "交易接口模块")
@RestController
@RequestMapping("/trade")
@Validated
public class TradeBuyerController {

    @Autowired
    @Qualifier("tradeManagerImpl")
    private TradeManager tradeManager;
    @Autowired
    private SettingClient settingClient;


    @ApiOperation(value = "创建交易")
    @PostMapping(value = "/create")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "client", value = "客户端类型", required = false, dataType = "String", dataTypeClass = String.class, paramType = "query",allowableValues = "PC,WAP,NATIVE,REACT,MINI"),
            @ApiImplicitParam(name = "way", value = "检查获取方式，购物车还是立即购买", required = true, dataType = "String", dataTypeClass = String.class, paramType = "query",allowableValues = "BUY_NOW,CART"),
    })
    public TradeVO create(String client, String way) {
        return this.tradeManager.createTrade(client, CheckedWay.valueOf(way));
    }

    @GetMapping("/setting")
    @ApiOperation(value = "获取订单任务设置信息")
    public OrderSettingVO getOrderSetting(){

        String json = this.settingClient.get(SettingGroup.TRADE);

        return JsonUtil.jsonToObject(json,OrderSettingVO.class);
    }



}
