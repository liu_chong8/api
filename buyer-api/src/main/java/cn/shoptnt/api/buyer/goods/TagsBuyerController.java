/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.goods;

import cn.shoptnt.model.support.validator.annotation.MarkType;
import cn.shoptnt.model.goods.dos.TagsDO;
import cn.shoptnt.model.goods.vo.BuyCountVO;
import cn.shoptnt.model.goods.vo.GoodsSelectLine;
import cn.shoptnt.model.goods.vo.TagGoodsNum;
import cn.shoptnt.service.goods.GoodsQueryManager;
import cn.shoptnt.service.goods.TagsManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 商品标签控制器
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018-03-28 14:49:36
 */
@RestController
@RequestMapping("/goods")
@Api(tags= "标签商品相关API")
@Validated
public class TagsBuyerController {

    @Autowired
    private TagsManager tagsManager;

    @Autowired
    private GoodsQueryManager goodsQueryManager;

    @ApiOperation(value = "查询标签商品列表", response = TagsDO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "seller_id", value = "卖家id", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "query"),
            @ApiImplicitParam(name = "mark", value = "hot热卖 new新品 recommend推荐", required = true, dataType = "String", dataTypeClass = String.class, paramType = "path"),
            @ApiImplicitParam(name = "num", value = "查询数量", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "query")
    })
    @GetMapping("/tags/{mark}/goods")
    public List<GoodsSelectLine> list(@ApiIgnore @NotNull(message = "店铺不能为空") Long sellerId, Integer num, @MarkType @PathVariable String mark) {

        if (num == null) {
            num = 5;
        }

        return tagsManager.queryTagGoods(sellerId, num, mark);
    }



    @ApiOperation(value = "查询商品销量", response = TagsDO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "goods_id", value = "商品id", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "query",allowMultiple = true)
    })
    @GetMapping("/tags/count")
    public List<BuyCountVO> list(@ApiIgnore @RequestParam("goods_id") Long[] goodsId) {
        return goodsQueryManager.queryBuyCount(goodsId);
    }

    @ApiOperation(value = "查询某店铺标签商品的数量", notes = "查询标签商品的数量")
    @ApiImplicitParam(name = "shop_id", value = "店铺id", required = true, paramType = "path", dataType = "int",dataTypeClass = Integer.class)
    @GetMapping(value = "/tags/{shop_id}/goods-num")
    public TagGoodsNum tagGoodsNum(@PathVariable("shop_id") Long shopId){

        return tagsManager.queryGoodsNumByShopId(shopId);
    }

}
