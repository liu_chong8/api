package cn.shoptnt.consumer.core.receiver;

import cn.shoptnt.consumer.core.event.PromotionScriptInitEvent;
import cn.shoptnt.model.base.rabbitmq.AmqpExchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 促销活动脚本初始化消息接收类
 * @author dmy
 * 2022-09-23
 */
@Component
public class PromotionScriptInitReceiver {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<PromotionScriptInitEvent> events;

    /**
     * 促销活动脚本初始化
     *
     * @param str
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.PROMOTION_SCRIPT_INIT + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.PROMOTION_SCRIPT_INIT, type = ExchangeTypes.FANOUT)
    ))
    public void scriptInit(String str) {

        if (events != null) {
            for (PromotionScriptInitEvent event : events) {
                try {
                    event.scriptInit();
                } catch (Exception e) {
                    logger.error("促销活动脚本初始化出错", e);
                }
            }
        }

    }
}
