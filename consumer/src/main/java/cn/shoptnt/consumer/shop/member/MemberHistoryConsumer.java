/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.shop.member;

import cn.shoptnt.consumer.core.event.MemberHistoryEvent;
import cn.shoptnt.client.member.MemberHistoryClient;
import cn.shoptnt.model.member.dto.HistoryDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 会员足迹
 *
 * @author zh
 * @version v7.0
 * @date 18/12/26 下午4:39
 * @since v7.0
 */
@Component
public class MemberHistoryConsumer implements MemberHistoryEvent {

    @Autowired
    private MemberHistoryClient memberHistoryClient;

    @Override
    public void addMemberHistory(HistoryDTO historyDTO) {
        //添加会员历史足迹
        memberHistoryClient.addMemberHistory(historyDTO);
    }
}
