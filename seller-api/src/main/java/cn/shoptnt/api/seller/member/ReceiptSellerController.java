/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.member;

import cn.shoptnt.client.trade.OrderClient;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.errorcode.MemberErrorCode;
import cn.shoptnt.model.member.dto.HistoryQueryParam;
import cn.shoptnt.model.member.vo.ReceiptFileVO;
import cn.shoptnt.model.member.vo.ReceiptHistoryVO;
import cn.shoptnt.service.member.ReceiptHistoryManager;
import cn.shoptnt.model.trade.order.vo.OrderDetailVO;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.framework.security.model.Seller;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;

/**
 * 会员开票历史记录API
 *
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-09-16
 */
@Api(tags= "会员开票历史记录API")
@RestController
@RequestMapping("/seller/members/receipts")
@Validated
public class ReceiptSellerController {

    @Autowired
    private ReceiptHistoryManager receiptHistoryManager;

    @Autowired
    private OrderClient orderClient;

    @ApiOperation(value = "查询会员开票历史记录信息列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_no", value = "页数", dataType = "int", dataTypeClass = Integer.class, paramType = "query"),
            @ApiImplicitParam(name = "page_size", value = "条数", dataType = "int", dataTypeClass = Integer.class, paramType = "query"),
    })
    @GetMapping()
    public WebPage list(@Valid HistoryQueryParam params, @ApiIgnore Long pageNo, @ApiIgnore Long pageSize) {
        Seller seller = UserContext.getSeller();
        if (seller == null) {
            throw new ServiceException(MemberErrorCode.E137.code(), "当前商家登录信息已经失效");
        }

        params.setSellerId(seller.getSellerId());

        WebPage page = this.receiptHistoryManager.list(pageNo, pageSize, params);
        return page;
    }

    @ApiOperation(value = "查询会员开票历史记录详细")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "history_id", value = "主键ID", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "path")
    })
    @GetMapping("/{history_id}")
    public ReceiptHistoryVO get(@PathVariable("history_id") Long historyId) {
        Seller seller = UserContext.getSeller();
        if (seller == null) {
            throw new ServiceException(MemberErrorCode.E137.code(), "当前商家登录信息已经失效");
        }
        //获取会员开票历史记录
        ReceiptHistoryVO receiptHistoryVO = this.receiptHistoryManager.get(historyId);

        if (receiptHistoryVO.getSellerId().intValue() != seller.getSellerId().intValue()) {
            throw new ServiceException(MemberErrorCode.E136.code(), "没有操作权限");
        }

        OrderDetailVO orderDetailVO = this.orderClient.getOrderVO(receiptHistoryVO.getOrderSn());
        receiptHistoryVO.setOrderStatus(orderDetailVO.getOrderStatus());

        return receiptHistoryVO;
    }

    @ApiOperation(value = "商家开具发票-增值税普通发票和增值税专用发票")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "history_id", value = "主键ID", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "path"),
            @ApiImplicitParam(name = "logi_id", value = "物流公司ID", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "query"),
            @ApiImplicitParam(name = "logi_name", value = "物流公司名称", required = true, dataType = "String", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "logi_code", value = "快递单号", required = true, dataType = "String", dataTypeClass = String.class, paramType = "query")
    })
    @PostMapping(value = "/{history_id}/logi")
    public void updateLogi(@PathVariable("history_id") Long historyId, Long logiId, String logiName, String logiCode) {

        this.receiptHistoryManager.updateLogi(historyId, logiId, logiName, logiCode);
    }

    @ApiOperation(value = "商家开具发票-上传电子普通发票附件")
    @PostMapping(value = "/upload/files")
    public void uploadFiles(@Valid @RequestBody ReceiptFileVO receiptFileVO) {

        this.receiptHistoryManager.uploadFiles(receiptFileVO);
    }
}
