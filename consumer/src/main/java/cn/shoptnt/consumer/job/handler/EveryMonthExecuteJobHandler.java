/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.job.handler;

import cn.shoptnt.model.base.JobAmqpExchange;
import com.xxl.job.core.biz.model.ReturnT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import cn.shoptnt.framework.rabbitmq.MessageSender;
import cn.shoptnt.framework.rabbitmq.MqMessage;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 每月执行
 *
 * @author chopper
 * @version v1.0
 * @since v7.0
 * 2018-07-06 上午4:24
 */
@Component
public class EveryMonthExecuteJobHandler{


    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private MessageSender messageSender;

    @XxlJob("everyMonthExecuteJobHandler")
    public ReturnT<String> execute(String param) throws Exception {
        try {
            this.logger.debug("发送每月定时任务");

            messageSender.send(new MqMessage(JobAmqpExchange.EVERY_MONTH_EXECUTE,
                    JobAmqpExchange.EVERY_MONTH_EXECUTE + "_ROUTING",
                    ""));
        } catch (Exception e) {
            this.logger.error("每月任务AMQP消息发送异常：", e);
            return ReturnT.FAIL;
        }
        return ReturnT.SUCCESS;
    }
}
