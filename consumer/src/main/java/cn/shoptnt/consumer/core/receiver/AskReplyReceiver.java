/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.receiver;

import cn.shoptnt.consumer.core.event.AskReplyEvent;
import cn.shoptnt.model.base.message.AskReplyMessage;
import cn.shoptnt.model.base.rabbitmq.AmqpExchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 会员商品咨询回复消息接收者
 *
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-09-17
 */
@Component
public class AskReplyReceiver {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<AskReplyEvent> events;

    /**
     * 会员回复商品咨询
     *
     * @param askReplyMessage
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.MEMBER_GOODS_ASK_REPLY + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.MEMBER_GOODS_ASK_REPLY, type = ExchangeTypes.FANOUT)
    ))
    public void goodsAsk(AskReplyMessage askReplyMessage) {
        if (events != null) {
            for (AskReplyEvent event : events) {
                try {
                    event.askReply(askReplyMessage);
                } catch (Exception e) {
                    logger.error("处理会员回复商品咨询出错",e);
                }
            }
        }
    }
}
