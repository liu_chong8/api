/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.mapper.member;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.shoptnt.framework.cache.MybatisRedisCache;
import cn.shoptnt.model.member.dos.Member;
import cn.shoptnt.model.member.vo.BackendMemberVO;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 会员Mapper接口
 * @author duanmingyu
 * @version v1.0
 * @since v7.2.2
 * 2020-07-27
 */
//不做二级缓存：因为要做会话级别的加解密，用的是会话秘钥，会导致多个秘钥污染同一片缓存数据
//@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface MemberMapper extends BaseMapper<Member> {

    /**
     * 根据用户名获取会员信息
     * @param name 用户名
     * @return
     */
    Member getMemberByUname(@Param("name") String name);

    /**
     * 根据手机号获取会员信息
     * @param mobile 手机号
     * @return
     */
    Member getMemberByMobile(@Param("mobile") String mobile);

    /**
     * 根据邮箱查询会员信息
     * @param email 邮箱
     * @return
     */
    Member getMemberByEmail(@Param("email")String email);
}
