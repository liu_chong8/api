/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.system.impl;

import cn.shoptnt.service.base.service.ValidatorManager;
import cn.shoptnt.client.system.ValidatorClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * 验证方式客户端
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.6
 * 2019-12-23
 */
@Service
@ConditionalOnProperty(value = "shoptnt.product", havingValue = "stand")
public class ValidatorClientImpl implements ValidatorClient {

    @Autowired
    private ValidatorManager validatorManager;

    @Override
    public void validate() {
        this.validatorManager.validate();
    }
}
