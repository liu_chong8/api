/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.mapper.payment;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.shoptnt.framework.cache.MybatisRedisCache;
import cn.shoptnt.model.payment.dos.PaymentMethodDO;
import cn.shoptnt.model.trade.order.dos.PayLog;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Param;

/**
 * 支付方式的Mapper
 * @author zs
 * @version 1.0
 * @since 7.2.2
 * 2020/07/29
 */
//@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface PaymentMethodMapper extends BaseMapper<PaymentMethodDO> {

    /**
     * 根据插件id查询插件配置信息
     * @param clientType 客户端类型
     * @param pluginId 插件id
     * @return
     */
    String selectClientType(@Param("clientType") String clientType, @Param("pluginId") String pluginId);
}
