/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.security;

import io.swagger.annotations.ApiModelProperty;

/**
 * 篡改记录查询参数
 * @author shenyanwu
 * @version 1.0
 * @data 2021/11/20 10:57
 **/
public class FalsifyRecordParams {

    /**
     * 分页
     */
    @ApiModelProperty(name = "page_no", value = "分页")
    private Long pageNo;

    /**
     * 每页显示数量
     */
    @ApiModelProperty(name = "page_size", value = "每页显示数量")
    private Long pageSize;

    /**
     * 篡改模块
     */
    @ApiModelProperty(name = "module", value = "篡改模块")
    private String module;


    /**
     * 状态
     */
    @ApiModelProperty(name = "state", value = "状态")
    private String state;

    public Long getPageNo() {
        return pageNo;
    }

    public void setPageNo(Long pageNo) {
        this.pageNo = pageNo;
    }

    public Long getPageSize() {
        return pageSize;
    }

    public void setPageSize(Long pageSize) {
        this.pageSize = pageSize;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
