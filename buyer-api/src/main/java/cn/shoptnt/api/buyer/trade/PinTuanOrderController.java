/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.trade;

import cn.shoptnt.client.goods.GoodsClient;
import cn.shoptnt.model.goods.vo.GoodsSkuVO;
import cn.shoptnt.model.promotion.pintuan.PintuanOrderDetailVo;
import cn.shoptnt.model.promotion.pintuan.PtGoodsDoc;
import cn.shoptnt.service.trade.pintuan.PinTuanSearchManager;
import cn.shoptnt.service.trade.pintuan.PintuanOrderManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

/**
 * Created by 妙贤 on 2019-01-30.
 *
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2019-01-30
 */
@Api(tags= "拼团订单API")
@RestController
@RequestMapping("/pintuan")
public class PinTuanOrderController {

    @Autowired
    private PintuanOrderManager pintuanOrderManager;

    @Autowired
    private PinTuanSearchManager pinTuanSearchManager;


    @Autowired
    private GoodsClient goodsClient;

    @GetMapping("/orders/{order_sn}")
    @ApiOperation(value = "获取某个拼团的详细")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "order_sn", value = "order_sn", required = true, dataType = "String", dataTypeClass = String.class, paramType = "path")

    })
    public PintuanOrderDetailVo detail(@ApiIgnore @PathVariable(name = "order_sn") String orderSn) {

        PintuanOrderDetailVo pintuanOrder = pintuanOrderManager.getMainOrderBySn(orderSn);

        return pintuanOrder;
    }

    @GetMapping("/orders/{order_sn}/guest")
    @ApiOperation(value = "猜你喜欢")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "order_sn", value = "order_sn", required = true, dataType = "String", dataTypeClass = String.class, paramType = "path"),
            @ApiImplicitParam(name = "num", value = "显示数量", required = true, dataType = "String", dataTypeClass = String.class, paramType = "path")

    })
    public List<PtGoodsDoc> guest(@ApiIgnore @PathVariable(name = "order_sn") String orderSn, Integer num) {

        try {
            PintuanOrderDetailVo pintuanOrder = pintuanOrderManager.getMainOrderBySn(orderSn);
            GoodsSkuVO skuVO = goodsClient.getSkuFromCache(pintuanOrder.getSkuId());

            // 准备默认值
            if(num==null){
                num=10;
            }
            return pinTuanSearchManager.search(skuVO.getCategoryId(), 1, num);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


}
