/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.shop.member;

import cn.shoptnt.consumer.core.event.AskReplyEvent;
import cn.shoptnt.model.base.message.AskReplyMessage;
import cn.shoptnt.client.member.MemberAskClient;
import cn.shoptnt.model.member.dos.AskMessageDO;
import cn.shoptnt.model.member.dos.AskReplyDO;
import cn.shoptnt.model.member.dos.MemberAsk;
import cn.shoptnt.model.member.enums.AskMsgTypeEnum;
import cn.shoptnt.model.member.enums.CommonStatusEnum;
import cn.shoptnt.model.system.enums.DeleteStatusEnum;
import cn.shoptnt.framework.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 会员商品咨询回复消费者
 * 会员回复商品咨询后给提出咨询的会员发送消息
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-09-17
 */
@Service
public class AskReplyConsumer implements AskReplyEvent {

    @Autowired
    private MemberAskClient memberAskClient;

    @Override
    public void askReply(AskReplyMessage askReplyMessage) {

        //获取咨询回复列表
        List<AskReplyDO> askReplyDOList = askReplyMessage.getAskReplyDOList();
        //获取咨询问题
        MemberAsk memberAsk = askReplyMessage.getMemberAsk();

        if (askReplyDOList != null && askReplyDOList.size() != 0 && memberAsk != null) {
            AskMessageDO askMessageDO = new AskMessageDO();
            askMessageDO.setMemberId(memberAsk.getMemberId());
            askMessageDO.setAsk(memberAsk.getContent());
            askMessageDO.setAskId(memberAsk.getAskId());
            askMessageDO.setAskMember(memberAsk.getMemberName());
            askMessageDO.setAskAnonymous(memberAsk.getAnonymous());
            askMessageDO.setGoodsId(memberAsk.getGoodsId());
            askMessageDO.setGoodsName(memberAsk.getGoodsName());
            askMessageDO.setGoodsImg(memberAsk.getGoodsImg());
            askMessageDO.setIsDel(DeleteStatusEnum.NORMAL.value());
            askMessageDO.setIsRead(CommonStatusEnum.NO.value());
            askMessageDO.setSendTime(askReplyMessage.getSendTime());
            askMessageDO.setReceiveTime(DateUtil.getDateline());
            askMessageDO.setMsgType(AskMsgTypeEnum.REPLY.value());

            for (AskReplyDO askReplyDO : askReplyDOList) {
                if (askReplyDO == null) {
                    break;
                }

                askMessageDO.setReplyId(askReplyDO.getId());
                askMessageDO.setReply(askReplyDO.getContent());
                askMessageDO.setReplyMember(askReplyDO.getMemberName());
                askMessageDO.setReplyAnonymous(askReplyDO.getAnonymous());
                //发送会员商品咨询消息
                this.memberAskClient.sendMessage(askMessageDO);
            }
        }

    }
}
