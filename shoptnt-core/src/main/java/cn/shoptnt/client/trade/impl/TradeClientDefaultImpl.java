/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.trade.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.shoptnt.client.trade.TradeClient;
import cn.shoptnt.model.security.ScanModuleDTO;
import cn.shoptnt.model.security.ScanResult;
import cn.shoptnt.model.trade.order.dos.TradeDO;
import cn.shoptnt.service.trade.order.TradeManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * @author zs
 * @version v1.0
 * @Description: 交易
 * @date 2021-12-21
 * @since v7.2.3
 */
@Service
@ConditionalOnProperty(value="shoptnt.product", havingValue="stand")
public class TradeClientDefaultImpl implements TradeClient {

    @Autowired
    private TradeManager tradeManager;

    @Override
    public ScanResult scanModule(ScanModuleDTO scanModuleDTO) {
        return tradeManager.scanModule(scanModuleDTO);
    }

    @Override
    public void reSign() {
        tradeManager.reSign();
    }

    @Override
    public void repair(Long tradeId) {
        tradeManager.repair(tradeId);
    }
}
