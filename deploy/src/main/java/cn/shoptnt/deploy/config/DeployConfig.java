/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.deploy.config;

import cn.shoptnt.framework.ShopTntConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @description:
 * @author: liuyulei
 * @create: 2019-12-25 18:12
 * @version:1.0
 * @since:7.1.4
 **/
@Configuration
public class DeployConfig {

    @Bean
    public ShopTntConfig shoptntConfig(){
        return  new ShopTntConfig();
    }
}
