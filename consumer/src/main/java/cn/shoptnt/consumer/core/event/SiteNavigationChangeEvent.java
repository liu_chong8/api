/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.event;


/**
 * 站点导航变化事件
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018年6月13日 14:36:07
 */
public interface SiteNavigationChangeEvent {

	/**
	 * 站点导航变化后执行的方法
	 * @param clientType PC端 或者 MOBILE端
	 */
	void navigationChange(String clientType);
}
