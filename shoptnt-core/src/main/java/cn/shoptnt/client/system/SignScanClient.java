/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.system;


/**
 * @author zs
 * @version v1.0
 * @Description: 签名数据扫描接口
 * @date 2021-12-21
 * @since v7.2.3
 */
public interface SignScanClient {

    void scan();
}
