/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.shop.sss;

import cn.shoptnt.model.base.rabbitmq.AmqpExchange;
import cn.shoptnt.client.statistics.ShopDataClient;
import cn.shoptnt.model.statistics.dto.ShopData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 商品收藏更新收集
 *
 * @author Chopper
 * @version v1.0
 * @since v7.0
 * 2018-07-23 下午5:50
 */
@Component
public class DataCollectionSellerConsumer {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private ShopDataClient shopDataClient;


    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.SELLER_COLLECTION_CHANGE + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.SELLER_COLLECTION_CHANGE, type = ExchangeTypes.FANOUT)
    ))
    public void sellerCollectionChange(ShopData shopData) {
        //修改店铺收藏数量
        shopDataClient.updateCollection(shopData);

    }


}
