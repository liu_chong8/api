/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.event;

/**
 * 店铺变更
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018年3月23日 上午10:26:32
 */
public interface ShopCollectionEvent {

	/**
	 * 店铺变更的消费
	 * @param shopId
	 */
	void shopChange(Long shopId);
}
