/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.member;

import cn.shoptnt.model.member.dos.ConnectSettingDO;
import cn.shoptnt.model.member.dto.ConnectSettingDTO;
import cn.shoptnt.model.member.vo.ConnectSettingVO;
import cn.shoptnt.model.support.LogClient;
import cn.shoptnt.model.support.validator.annotation.Log;
import cn.shoptnt.model.support.validator.annotation.LogLevel;
import cn.shoptnt.service.member.ConnectManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author zjp
 * @version v7.0
 * @Description 信任登录设置
 * @ClassName ConnectController
 * @since v7.0 下午4:23 2018/6/15
 */
@Api(tags="信任登录设置api")
@RestController
@RequestMapping("/admin/members/connect")
public class MemberConnectManagerController {

    @Autowired
    private ConnectManager connectManager;



    @GetMapping()
    @ApiOperation(value = "获取信任登录配置参数",response = ConnectSettingDO.class)
    public List<ConnectSettingVO> list(){
        return  connectManager.list();
    }

    @PutMapping(value = "/{type}")
    @ApiOperation(value = "修改信任登录参数", response = ConnectSettingDTO.class)
    @Log(client = LogClient.admin,detail = "修改信任登录参数",level = LogLevel.important)
    @ApiImplicitParam(name = "type", value = "用户名", required = true, dataType = "String", dataTypeClass = String.class, paramType = "path",allowableValues = "QQ,ALIPAY,WEIBO,WECHAT")
    public ConnectSettingDTO editConnectSetting(@RequestBody ConnectSettingDTO connectSettingDTO, @PathVariable("type")  String type) {
        connectManager.save(connectSettingDTO);
        return connectSettingDTO;
    }


}
