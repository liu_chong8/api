/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.deploy.util;

import cn.shoptnt.deploy.service.impl.MyMap;

import java.util.HashMap;
import java.util.Map;

/**
 * 组装esde mapping
 * @author fk
 * @version 1.0
 * @since 7.2.2
 * 2021年04月14日18:06:54
 */
public class EsMappingUtil {


    /**
     * 创建商品mapping
     *
     * @return
     */
    public static Map createGoodsMapping() {

        Map goodsMap = new HashMap();

        goodsMap.put("goodsId", new MyMap().put("type", "long").getMap());
        goodsMap.put("goodsName", new MyMap().put("type", "text").put("analyzer", "ik_max_word").getMap());
        goodsMap.put("thumbnail", new MyMap().put("type", "text").getMap());
        goodsMap.put("small", new MyMap().put("type", "text").getMap());
        goodsMap.put("buyCount", new MyMap().put("type", "integer").getMap());
        goodsMap.put("sellerId", new MyMap().put("type", "long").getMap());
        goodsMap.put("sellerName", new MyMap().put("type", "text").getMap());
        goodsMap.put("shopCatId", new MyMap().put("type", "long").getMap());
        goodsMap.put("shopCatPath", new MyMap().put("type", "text").getMap());
        goodsMap.put("commentNum", new MyMap().put("type", "integer").getMap());
        goodsMap.put("grade", new MyMap().put("type", "double").getMap());
        goodsMap.put("price", new MyMap().put("type", "double").getMap());
        goodsMap.put("brand", new MyMap().put("type", "long").getMap());
        goodsMap.put("categoryId", new MyMap().put("type", "long").getMap());
        goodsMap.put("categoryPath", new MyMap().put("type", "text").getMap());
        goodsMap.put("disabled", new MyMap().put("type", "integer").getMap());
        goodsMap.put("marketEnable", new MyMap().put("type", "integer").getMap());
        goodsMap.put("priority", new MyMap().put("type", "integer").getMap());
        goodsMap.put("isAuth", new MyMap().put("type", "integer").getMap());
        goodsMap.put("intro", new MyMap().put("type", "text").getMap());
        goodsMap.put("selfOperated", new MyMap().put("type", "integer").getMap());

        Map paramPro = new MyMap().put("name", new MyMap().put("type", "keyword").getMap()).put("value", new MyMap().put("type", "keyword").getMap()).getMap();
        goodsMap.put("params", new MyMap().put("type", "nested").put("properties", paramPro).getMap());

        return new MyMap().put("properties", goodsMap).getMap();
    }

    /**
     * 创建拼团mapping
     *
     * @return
     */
    public static Map createPingTuanMapping() {

        Map pingTuanMap = new HashMap();
        pingTuanMap.put("buyCount", new MyMap().put("type", "integer").getMap());
        pingTuanMap.put("categoryId", new MyMap().put("type", "long").getMap());
        pingTuanMap.put("categoryPath", new MyMap().put("type", "text").getMap());
        pingTuanMap.put("goodsId", new MyMap().put("type", "long").getMap());
        pingTuanMap.put("goodsName", new MyMap().put("type", "text").put("analyzer", "ik_max_word").getMap());
        pingTuanMap.put("originPrice", new MyMap().put("type", "double").getMap());
        pingTuanMap.put("salesPrice", new MyMap().put("type", "double").getMap());
        pingTuanMap.put("skuId", new MyMap().put("type", "long").getMap());
        pingTuanMap.put("thumbnail", new MyMap().put("type", "text").getMap());
        return new MyMap().put("properties", pingTuanMap).getMap();
    }


}
