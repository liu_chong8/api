/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.system.impl;

import cn.shoptnt.model.base.dto.FileDTO;
import cn.shoptnt.model.base.vo.FileVO;
import cn.shoptnt.service.base.plugin.upload.Uploader;
import cn.shoptnt.client.system.UploadFactoryClient;
import cn.shoptnt.service.base.service.FileManager;
import cn.shoptnt.service.base.service.impl.FileManagerImpl;
import cn.shoptnt.service.system.factory.UploadFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @version v7.0
 * @Description:
 * @Author: zjp
 * @Date: 2018/7/27 16:27
 */
@Service
@ConditionalOnProperty(value = "shoptnt.product", havingValue = "stand")
public class UploadFactoryClientDefaultImpl implements UploadFactoryClient {
    @Autowired
    private UploadFactory uploadFactory;

    @Autowired
    private FileManager fileManager;

    @Override
    public String getUrl(String url, Integer width, Integer height) {
        Uploader uploader = uploadFactory.getUploader();
        return uploader.getThumbnailUrl(url, width, height);
    }

    @Override
    public FileVO upload(MultipartFile file, String scene) {
        return fileManager.uploadFile(file, scene);
    }


}
