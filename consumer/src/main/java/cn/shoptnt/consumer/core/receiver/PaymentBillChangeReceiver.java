/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.receiver;

import cn.shoptnt.consumer.core.event.PaymentBillPayStatusChangeEvent;
import cn.shoptnt.model.base.message.PaymentBillStatusChangeMsg;
import cn.shoptnt.model.base.rabbitmq.AmqpExchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 支付账单状态改变消费者
 * 
 * @author fk
 * @version v2.0
 * @since v7.2.1 2020年3月11日 上午10:31:42
 */
@Component
public class PaymentBillChangeReceiver {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired(required = false)
	private List<PaymentBillPayStatusChangeEvent> events;

	/**
	 * 订单状态改变
	 * @param paymentBillMessage
	 */
	@RabbitListener(bindings = @QueueBinding(
			value = @Queue(value = AmqpExchange.PAYMENT_BILL_CHANGE + "_QUEUE"),
			exchange = @Exchange(value = AmqpExchange.PAYMENT_BILL_CHANGE, type = ExchangeTypes.FANOUT)
	))
	public void billChange(PaymentBillStatusChangeMsg paymentBillMessage) {
		if (events != null) {
			for (PaymentBillPayStatusChangeEvent event : events) {
				try {
					event.billChange(paymentBillMessage);
				} catch (Exception e) {
					logger.error("支付账单状态改变消息出错", e);
				}
			}
		}

	}

}
