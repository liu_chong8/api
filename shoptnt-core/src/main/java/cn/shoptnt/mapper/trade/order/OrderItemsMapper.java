/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.mapper.trade.order;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.shoptnt.framework.cache.MybatisRedisCache;
import cn.shoptnt.model.trade.order.dos.OrderItemsDO;
import org.apache.ibatis.annotations.CacheNamespace;

/**
 * 订单货物mapper
 * @author zs
 * @version v1.0
 * @since v7.2.2
 * 2020-08-07
 */
//@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface OrderItemsMapper extends BaseMapper<OrderItemsDO> {
}
