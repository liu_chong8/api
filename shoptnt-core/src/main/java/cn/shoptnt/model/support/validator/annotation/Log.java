/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.support.validator.annotation;

import cn.shoptnt.model.support.LogClient;

import java.lang.annotation.*;

/**
 * 记录操作日志自定义注解
 * @author fk
 * @version v1.0
 * @since v6.1
 * 2016年12月7日 下午1:10:14
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface Log {

	/**
	 * 商家端，管理端
	 * @return
	 */
	LogClient client();

	/**
	 * 操作说明
	 * @return
	 */
	String detail();

	/**
	 * 日志级别，默认是一般级别
	 * @return
	 */
	LogLevel level() default LogLevel.normal;
	
}
