/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.security;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 安全模块
 * @author 妙贤
 * @version 1.0
 * @data 2021/11/19 16:34
 **/
@TableName("es_security_module")
public class SecurityModule {

    @TableId(type= IdType.ASSIGN_ID)
    private Long moduleId;

    /**
     * 模块名称，如订单、会员
     */
    private String moduleName;

    /**
     * 轮次
     */
    private String rounds;

    /**
     * 状态
     */
    private String state;

    public Long getModuleId() {
        return moduleId;
    }

    public void setModuleId(Long moduleId) {
        this.moduleId = moduleId;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getRounds() {
        return rounds;
    }

    public void setRounds(String rounds) {
        this.rounds = rounds;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
