/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.orderbill;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.goods.enums.Permission;
import cn.shoptnt.model.support.LogClient;
import cn.shoptnt.model.support.validator.annotation.Log;
import cn.shoptnt.service.orderbill.validator.annotation.BillItemType;
import cn.shoptnt.model.orderbill.dos.Bill;
import cn.shoptnt.model.orderbill.vo.BillDetail;
import cn.shoptnt.model.orderbill.vo.BillExcel;
import cn.shoptnt.model.orderbill.vo.BillQueryParam;
import cn.shoptnt.service.orderbill.BillItemManager;
import cn.shoptnt.service.orderbill.BillManager;
import cn.shoptnt.framework.util.DateUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @author fk
 * @version v1.0
 * @Description: 结算单账单控制器
 * @date 2018/4/27 11:49
 * @since v7.0.0
 */
@RestController
@RequestMapping("/admin/order/bills")
@Api(tags= "结算相关API")
@Validated
public class OrderBillManagerController {

    @Autowired
    private BillManager billManager;

    @Autowired
    private BillItemManager billItemManager;

    @ApiOperation(value = "管理员查询所有周期结算单列表统计")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_no", value = "页码", required = false, dataType = "int", dataTypeClass = Integer.class, paramType = "query"),
            @ApiImplicitParam(name = "page_size", value = "每页数量", required = false, dataType = "int", dataTypeClass = Integer.class, paramType = "query"),
            @ApiImplicitParam(name = "sn", value = "账单号", required = false, dataType = "String", dataTypeClass = String.class, paramType = "query")
    })
    @GetMapping("/statistics")
    public WebPage getAllBill(@ApiIgnore Long pageNo, @ApiIgnore Long pageSize, @ApiIgnore String sn){

        WebPage page = this.billManager.getAllBill(pageNo, pageSize, sn);

        return page;
    }


    @ApiOperation(value = "管理员查询某周期结算列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_no", value = "页码", required = false, dataType = "int", dataTypeClass = Integer.class, paramType = "query"),
            @ApiImplicitParam(name = "page_size", value = "每页数量", required = false, dataType = "int", dataTypeClass = Integer.class, paramType = "query")
    })
    @GetMapping
    public WebPage getBillDetail(@ApiIgnore Long pageNo, @ApiIgnore Long pageSize, BillQueryParam param){

        param.setPageNo(pageNo);
        param.setPageSize(pageSize);

        return  this.billManager.queryBills(param);
    }


    @ApiOperation(value = "查看某账单详细")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "bill_id", value = "结算单id", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "path"),
    })
    @GetMapping("/{bill_id}")
    public BillDetail queryBill(@PathVariable("bill_id") Long billId) {

        return  this.billManager.getBillDetail(billId, Permission.ADMIN);
    }

    @ApiOperation(value = "导出某账单详细")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "bill_id", value = "结算单id", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "path"),
    })
    @GetMapping("/{bill_id}/export")
    public BillExcel exportBill(@PathVariable("bill_id") Long billId) {

        return  this.billManager.exportBill(billId);
    }


    @ApiOperation(value = "对账单进行下一步操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "bill_id", value = "账单id", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "path"),
    })
    @Log(client = LogClient.admin,detail = "管理员操作结算单，结算单号：${billId}")
    @PutMapping(value = "/{bill_id}/next")
    public Bill nextBill(@PathVariable("bill_id")Long billId) {

        Bill bill = this.billManager.editStatus(billId, Permission.ADMIN);

        return bill;
    }

    @ApiOperation(value = "查看账单中的订单列表或者退款单列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_no", value = "页码", required = false, dataType = "int", dataTypeClass = Integer.class, paramType = "query"),
            @ApiImplicitParam(name = "page_size", value = "每页数量", required = false, dataType = "int", dataTypeClass = Integer.class, paramType = "query"),
            @ApiImplicitParam(name = "bill_id", value = "账单id", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "path"),
            @ApiImplicitParam(name = "bill_type", value = "账单类型", required = true, dataType = "String", dataTypeClass = String.class, paramType = "path", allowableValues = "REFUND,PAYMENT"),
    })
    @GetMapping("/{bill_id}/{bill_type}")
    public WebPage queryBillItems(@ApiIgnore Long pageNo, @ApiIgnore Long pageSize, @PathVariable("bill_id") Long billId, @BillItemType @PathVariable("bill_type") String billType) {

        return this.billItemManager.list(pageNo, pageSize, billId, billType);
    }

    @GetMapping("/init")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "start_time", value = "开始时间 2020-06-01 00:00:01", required = true, dataType = "String", dataTypeClass = String.class,paramType = "query"),
            @ApiImplicitParam(name = "end_time", value = "结束时间 2020-06-30 23:59:59", required = true, dataType = "String", dataTypeClass = String.class,paramType = "query"),
    })
    public String createBill(@ApiIgnore String startTime,@ApiIgnore String endTime){

        this.billManager.createBills(DateUtil.getDateline(startTime,"yyyy-MM-dd HH:mm:ss"),DateUtil.getDateline(endTime,"yyyy-MM-dd HH:mm:ss"));

        return "";
    }
}
