/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.pagedata;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.pagedata.PageData;
import cn.shoptnt.model.pagedata.PageQueryParam;
import cn.shoptnt.service.pagedata.PageDataManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 楼层控制器
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-05-21 16:39:22
 */
@RestController
@RequestMapping("/admin/pages")
@Api(tags = "楼层相关API")
@Validated
public class PageDataManagerController {

    @Autowired
    private PageDataManager pageManager;


    @GetMapping()
    @ApiOperation(value = "查询页面列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "client_type", value = "PC/MOBILE", required = true, dataType = "String", dataTypeClass = String.class, paramType = "query")
    })
    public WebPage getList(PageQueryParam param) {
        param.setSellerId(0L);
        WebPage pageList = this.pageManager.getPageList(param);
        return pageList;
    }


    @PostMapping()
    @ApiOperation(value = "添加微页面", response = PageData.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_name", value = "微页面标题", required = true, dataType = "String", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "page_data", value = "页面元素", required = true, dataType = "String", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "client_type", value = "", required = true, dataType = "String", dataTypeClass = String.class, paramType = "query")
    })
    public PageData add(@Valid @RequestBody PageData page) {
        page.setSellerId(0L);
        this.pageManager.add(page);
        return page;
    }


    @PutMapping(value = "/{id}")
    @ApiOperation(value = "修改楼层", response = PageData.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "long", dataTypeClass = Long.class, paramType = "path"),
            @ApiImplicitParam(name = "page_name", value = "微页面标题", required = true, dataType = "String", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "page_data", value = "PC/MOBILE", required = true, dataType = "String", dataTypeClass = String.class, paramType = "query")
    })
    public PageData edit(@Valid @RequestBody PageData page, @PathVariable("id") Long id) {
        return this.pageManager.edit(page, id);
    }

    @PutMapping(value = "/{id}/publish")
    @ApiOperation(value = "修改发布状态", response = PageData.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "long", dataTypeClass = Long.class, paramType = "path"),
    })
    public PageData updatePublish(@PathVariable("id") Long id) {
        return pageManager.updatePublish(id);
    }


    @GetMapping(value = "/{id}")
    @ApiOperation(value = "查询一个楼层")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "要查询的楼层主键", required = true, dataType = "long", dataTypeClass = Long.class, paramType = "path")
    })
    public PageData get(@PathVariable("id") Long id) {
        PageData page = this.pageManager.getModel(id);
        return page;
    }


    @DeleteMapping(value = "/{ids}")
    @ApiOperation(value = "删除微页面")
    @ApiImplicitParams({@ApiImplicitParam(name = "ids", value = "id集合", required = true, dataType = "long", dataTypeClass = Long.class, paramType = "path", allowMultiple = true)})
    public void delete(@PathVariable("ids") Long[] ids) {
        this.pageManager.delete(ids);
    }


    @PutMapping(value = "/{id}/index")
    @ApiOperation(value = "设为首页", response = PageData.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "long", dataTypeClass = Long.class, paramType = "path")
    })
    public PageData editIndex(@PathVariable("id") Long id) {
        PageData pageData = this.pageManager.editIndex(0L, id);
        return pageData;
    }


}
