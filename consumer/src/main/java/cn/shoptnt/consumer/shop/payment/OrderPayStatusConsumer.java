/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.shop.payment;

import cn.shoptnt.client.trade.OrderOperateClient;
import cn.shoptnt.consumer.core.event.PaymentBillPayStatusChangeEvent;
import cn.shoptnt.model.base.message.PaymentBillStatusChangeMsg;
import cn.shoptnt.model.payment.dos.PaymentBillDO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 订单支付状态改变消费者
 *
 * @author fk
 * @version v2.0
 * @since v7.2.1
 * 2020年3月11日 上午9:52:13
 */
@Service
public class OrderPayStatusConsumer implements PaymentBillPayStatusChangeEvent {

    @Autowired
    private OrderOperateClient orderOperateClient;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void billChange(PaymentBillStatusChangeMsg paymentBillMessage) {

        // 支付成功
        if (PaymentBillStatusChangeMsg.SUCCESS.equals(paymentBillMessage.getStatus())) {
            //获取账单信息
            PaymentBillDO bill = paymentBillMessage.getBill();
            //获取交易类型
            String tradeType = bill.getServiceType();

            orderOperateClient.paySuccess(tradeType,bill.getSubSn(), bill.getReturnTradeNo(), paymentBillMessage.getPayPrice());

            logger.debug("支付更改成功");
        }


    }

}
