/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.debugger;

import cn.shoptnt.framework.util.DateUtil;
import cn.shoptnt.service.orderbill.BillManager;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @author lzg
 * @version v1.0
 * @Description: 结算单账单控制器
 * @date 2021/02/24 16:49
 * @since v7.0.0
 */
@RestController
@RequestMapping("/debugger")
@ConditionalOnProperty(value = "shoptnt.debugger", havingValue = "true")
public class BillCreateController {

    @Autowired
    private BillManager billManager;


    @GetMapping("/bills")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "start_time", value = "开始时间 2021-01-01 00:00:01", required = true, dataType = "String", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "end_time", value = "结束时间 2021-01-30 23:59:59", required = true, dataType = "String", dataTypeClass = String.class, paramType = "query")
    })
    public String createBill(@ApiIgnore String startTime, @ApiIgnore String endTime) {

        this.billManager.createBills(DateUtil.getDateline(startTime, "yyyy-MM-dd HH:mm:ss"), DateUtil.getDateline(endTime, "yyyy-MM-dd HH:mm:ss"));
        return "结算单生成成功";
    }

}
