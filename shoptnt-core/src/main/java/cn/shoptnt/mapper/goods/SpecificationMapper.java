/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.mapper.goods;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.shoptnt.framework.cache.MybatisRedisCache;
import cn.shoptnt.model.goods.dos.SpecificationDO;
import cn.shoptnt.model.goods.vo.SelectVO;
import cn.shoptnt.model.goods.vo.SpecificationVO;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Param;
import org.springframework.security.core.parameters.P;

import java.util.List;
import java.util.Map;

/**
 * SpecificationDO的Mapper
 * @author fk
 * @version 1.0
 * @since 7.1.0
 * 2020/7/21
 */
//@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface SpecificationMapper extends BaseMapper<SpecificationDO> {

    /**
     * 查询分类关联的规格
     * @param categoryId 分类id
     * @return
     */
    List<SelectVO> getCatSpecification(@Param("category_id") Long categoryId);

    /**
     * 验证商家规格名称是否存在
     * @param map 条件
     * @return
     */
    Integer checkSellerSpecName(@Param("ew") Map map);


    /**
     * 查询分类绑定的商家规格
     * @param categoryId 分类id
     * @param sellerId 商家id
     * @return
     */
    List<SpecificationVO> queryCatSpec(@Param("category_id") Long categoryId,@Param("seller_id")Long sellerId);

    /**
     * 查询分类绑定的平台规格
     * @param categoryId 分类id
     * @return
     */
    List<SpecificationVO> queryAdminSpec(@Param("category_id") Long categoryId);

}
