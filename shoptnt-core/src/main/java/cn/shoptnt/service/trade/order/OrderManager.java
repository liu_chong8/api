/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.trade.order;

import cn.shoptnt.model.trade.order.dos.OrderDO;
import cn.shoptnt.model.trade.order.vo.OrderDetailVO;

/**
 * 订单操作接口
 * @author Snow create in 2018/5/21
 * @version v2.0
 * @since v7.0.0
 */
public interface OrderManager {

    /**
     * 修改订单信息
     * @param orderDO 订单实体
     * @return 订单明细实体
     */
    OrderDetailVO update(OrderDO orderDO);



}
