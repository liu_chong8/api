/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.debugger;

import cn.shoptnt.model.base.rabbitmq.AmqpExchange;
import cn.shoptnt.model.system.vo.TaskProgressConstant;
import cn.shoptnt.model.util.progress.ProgressManager;
import cn.shoptnt.framework.exception.ResourceNotFoundException;
import cn.shoptnt.framework.rabbitmq.MessageSender;
import cn.shoptnt.framework.rabbitmq.MqMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2019-04-24
 */
@RestController
@RequestMapping("/debugger/index-create")
@ConditionalOnProperty(value = "shoptnt.debugger", havingValue = "true")
public class IndexCreateCheckController {
    @Autowired
    private MessageSender messageSender;


    @Autowired
    private ProgressManager progressManager;

    @GetMapping(value = "/test")
    public String test( String pageType ) {

        if (progressManager.getProgress(TaskProgressConstant.GOODS_INDEX) != null) {
            throw new ResourceNotFoundException("有索引任务正在进行中，需等待本次任务完成后才能再次生成。");
        }
        /** 发送索引生成消息 */
        this.messageSender.send(new MqMessage(AmqpExchange.INDEX_CREATE, AmqpExchange.INDEX_CREATE+"_ROUTING","1"));


        return "ok";
    }

}
