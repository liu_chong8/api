/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.goodssearch;

import cn.shoptnt.model.goods.vo.GoodsSearchResultVO;
import cn.shoptnt.model.goodssearch.GoodsSearchDTO;
import cn.shoptnt.model.goodssearch.GoodsWords;
import cn.shoptnt.framework.database.WebPage;

import java.util.List;
import java.util.Map;

/**
 * 商品搜索
 *
 * @author fk
 * @version v6.4
 * @since v6.4
 * 2017年9月14日 上午10:52:20
 */
public interface GoodsSearchManager {

    /**
     * 通过关键字获取商品分词索引
     *
     * @param keyword 关键字
     * @return
     */
    List<GoodsWords> getGoodsWords(String keyword);

    /**
     * 获取'为你推荐'商品列表
     * @param goodsSearch 查询参数
     * @return 分页数据
     */
    WebPage recommendGoodsList(GoodsSearchDTO goodsSearch);

    /**
     * 查询商品列表和商品选择器
     * @param goodsSearch 查询参数
     * @return
     */
    GoodsSearchResultVO searchGoodsAndSelector(GoodsSearchDTO goodsSearch);
}
