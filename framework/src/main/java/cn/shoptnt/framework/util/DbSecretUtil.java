/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.framework.util;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import cn.shoptnt.framework.security.model.TokenConstant;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * @author zjp
 * @version v7.0
 * @Description AES加密工具
 * @ClassName AESUtil
 * @since v7.0 下午10:53 2018/6/26
 */
public class DbSecretUtil {

    private static final Logger logger = LoggerFactory.getLogger(DbSecretUtil.class);

    public static Boolean isEncryptSetting = true;

    /**
     * 加密方法
     * @param data  要加密的数据
     * @param key 加密key
     * @return 加密的结果
     * @throws Exception
     */
    public static String encrypt(String data, String key){
        if(isEncryptSetting){
            return AESUtil.encrypt(data, key);
        }
        return data;
    }

    /**
     * 解密方法
     * @param data 要解密的数据
     * @param key  解密key
     * @return 解密的结果
     * @throws Exception
     */
    public static String decrypt(String data, String key) {
        if(isEncryptSetting){
            return AESUtil.decrypt(data, key);
        }
        return data;
    }

}
