/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.framework.context.request;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 数据脱敏的拦截器，主要是这是哪些api需要脱敏，需要脱敏的，则ThreadLoacal设置成true
 *
 * @author fk
 * @version v1.0
 * @since v7.2.3
 * 2021年11月17日10:18:28
 */
public class DesensitizationThreadContextHolder {

    /**
     * 是否需要脱敏
     */
    private static ThreadLocal<Boolean> desensitizationThreadLocalHolder = new ThreadLocal<>();

    /**
     * 设置脱敏标志，是否需要脱敏
     * @param flag
     */
    public static void setDesensitizationFlag(Boolean flag){
        desensitizationThreadLocalHolder.set(flag);
    }


    /**
     * 移除脱敏标志
     */
    public static void remove(){
        desensitizationThreadLocalHolder.remove();
        desensitizationThreadLocalHolder.remove();
    }

    /**
     * 获取脱敏标志，是否脱敏
     * @return
     */
    public static Boolean getDesensitizationFlag(){
        return  desensitizationThreadLocalHolder.get();
    }
}
