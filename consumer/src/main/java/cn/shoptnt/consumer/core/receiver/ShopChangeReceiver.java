/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.receiver;

import cn.shoptnt.consumer.core.event.ShopChangeEvent;
import cn.shoptnt.model.base.rabbitmq.AmqpExchange;
import cn.shoptnt.model.shop.vo.ShopChangeMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * 店铺变更消费者
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018年3月23日 上午10:32:08
 */
@Component
public class ShopChangeReceiver {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<ShopChangeEvent> events;

    /**
     * 店铺变更
     *
     * @param shopNameChangeMsg
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.SHOP_CHANGE + "_ROUTING"),
            exchange = @Exchange(value = AmqpExchange.SHOP_CHANGE, type = ExchangeTypes.FANOUT)
    ))
    public void shopChange(ShopChangeMsg shopNameChangeMsg) {

        if (events != null) {
            for (ShopChangeEvent event : events) {
                try {
                    event.shopChange(shopNameChangeMsg);
                } catch (Exception e) {
                    logger.error("店铺信息变更消息出错", e);
                }
            }
        }

    }

}
