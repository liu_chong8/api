/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.deploy.service.impl;

import cn.shoptnt.deploy.model.Elasticsearch;
import cn.shoptnt.deploy.service.DeployExecutor;
import cn.shoptnt.deploy.service.ElasticsearchManager;
import cn.shoptnt.framework.elasticsearch.ElasticBuilderUtil;
import cn.shoptnt.framework.elasticsearch.ElasticOperationUtil;
import cn.shoptnt.framework.elasticsearch.EsSettings;
import cn.shoptnt.framework.util.StringUtil;
import io.searchbox.client.JestClient;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.common.settings.Settings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by 妙贤 on 2019-02-13.
 * elasticsearch部署执行器
 *
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2019-02-13
 */
@Service
public class EsDeployExecutor implements DeployExecutor {

    @Autowired
    private ElasticsearchManager elasticsearchManager;


    @Override
    public void deploy(Long deployId) {
        for (int i = 0; i <= 4; i++) {
            try {
                //获取ES配置
                Elasticsearch elasticsearch = elasticsearchManager.getByDeployId(deployId);
                JestClient jestClient;
                //截取host和port
                String[] clusterName = elasticsearch.getClusterNodes().split(",");
                if (!StringUtil.isEmpty(elasticsearch.getXpackSecurityUser())) {
                    //截取用户名密码
                    String[] securityUser = elasticsearch.getXpackSecurityUser().split(":");
                    jestClient = ElasticBuilderUtil.buildJestClient(clusterName,StringUtil.toInt(elasticsearch.getClusterPort(),9200),"http://",securityUser[0],securityUser[1]);

                } else {
                    jestClient = ElasticBuilderUtil.buildJestClient(clusterName,StringUtil.toInt(elasticsearch.getClusterPort(),9200),"http://");
                }
                //拼装索引名称
                String goodsIndices = elasticsearch.getIndexName() + "_" + EsSettings.GOODS_INDEX_NAME;
                String ptIndices = elasticsearch.getIndexName() + "_" + EsSettings.PINTUAN_INDEX_NAME;
                //创建Goods 和 拼团映射
                CreateIndexRequest goodsRequest = new CreateIndexRequest(goodsIndices);
                CreateIndexRequest ptRequest = new CreateIndexRequest(ptIndices);
                //设置分片数
                Settings settings = Settings.builder().put("number_of_shards",5).put("number_of_replicas",3).build();
                goodsRequest.settings(settings);
                ptRequest.settings(settings);
                //创建之前先删除映射
                //检测商品索引是否存在
                if (ElasticOperationUtil.indicesExists(jestClient, goodsIndices)) {
                    // 存在则删除
                    ElasticOperationUtil.deleteIndex(jestClient,goodsIndices);
                }

                //检测拼团索引是否存在
                if (ElasticOperationUtil.indicesExists(jestClient, ptIndices)) {
                    // 存在则删除
                    ElasticOperationUtil.deleteIndex(jestClient,ptIndices);
                }
                // 获取ES版本号
                ElasticBuilderUtil.version = ElasticOperationUtil.version(jestClient);
                //创建映射
                ElasticOperationUtil.createIndex(jestClient,goodsIndices,ElasticBuilderUtil.createGoodsMapping());
                ElasticOperationUtil.createIndex(jestClient,ptIndices,ElasticBuilderUtil.createPingTuanMapping());
                //如果成功，则直接跳出循环
                break;
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }

        }

    }

    @Override
    public String getType() {
        return "elasticsearch";
    }

}
