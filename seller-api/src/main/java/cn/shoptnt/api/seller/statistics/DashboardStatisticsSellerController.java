/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.statistics;

import cn.shoptnt.model.statistics.vo.ShopDashboardVO;
import cn.shoptnt.service.statistics.DashboardStatisticManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 商家中心，首页数据
 *
 * @author mengyuanming
 * @version 2.0
 * @since 7.0
 * 2018/6/25 15:13
 */
@RestController
@RequestMapping("/seller/statistics/dashboard")
@Api(tags= "商家中心，首页数据")
public class DashboardStatisticsSellerController {

    @Autowired
    private DashboardStatisticManager dashboardStatisticManager;

    @ApiOperation(value = "首页数据",response = ShopDashboardVO.class)
    @GetMapping("/shop")
    public ShopDashboardVO shop() {
        return this.dashboardStatisticManager.getShopData();
    }

}
