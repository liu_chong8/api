/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.deploy.enums;

/**
 * @Program: code_b2b2c_7.2.2_rc
 * @Author: 李正国
 * @Date: 2020-12-22 15:25
 * @Description: 遍历
 */
public enum MysqlData {

    /**
     * DEFAULT_DATABASE
     */
    DEFAULT_DATABASE,

    /**
     * SHARD
     */
    SHARD,

    /**
     * SHOP01
     */
    SHOP01,

}
