/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.shop.member;

import cn.shoptnt.consumer.core.event.MemberInfoChangeEvent;
import cn.shoptnt.client.member.MemberClient;
import cn.shoptnt.client.member.MemberCommentClient;
import cn.shoptnt.model.member.dos.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 会员信息消费者
 *
 * @author zh
 * @version v7.0
 * @date 18/12/26 下午4:39
 * @since v7.0
 */
@Component
public class MemberInfoConsumer implements MemberInfoChangeEvent {

    @Autowired
    private MemberCommentClient memberCommentClient;
    @Autowired
    private MemberClient memberClient;

    @Override
    public void memberInfoChange(Long memberId) {
        //获取用户信息
        Member member = memberClient.getModel(memberId);
        //修改用户评论信息
        memberCommentClient.editComment(member.getMemberId(), member.getFace());

    }
}
