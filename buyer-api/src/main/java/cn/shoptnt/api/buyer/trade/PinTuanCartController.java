/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.trade;

import cn.shoptnt.framework.exception.ServiceException;
import cn.shoptnt.model.errorcode.PromotionErrorCode;
import cn.shoptnt.model.promotion.pintuan.PinTuanGoodsVO;
import cn.shoptnt.model.promotion.pintuan.Pintuan;
import cn.shoptnt.model.trade.cart.vo.CartSkuOriginVo;
import cn.shoptnt.model.trade.cart.vo.CartView;
import cn.shoptnt.model.trade.order.vo.TradeVO;
import cn.shoptnt.service.trade.pintuan.PintuanCartManager;
import cn.shoptnt.service.trade.pintuan.PintuanGoodsManager;
import cn.shoptnt.service.trade.pintuan.PintuanManager;
import cn.shoptnt.service.trade.pintuan.impl.PintuanTradeManagerImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.constraints.NotNull;

/**
 * Created by 妙贤 on 2019-01-23.
 *
 * @author 妙贤
 * @version 1.0
 * @since 7.0.0
 * 2019-01-23
 */

@Api(tags= "拼团购物API")
@RestController
@RequestMapping("/pintuan")
public class PinTuanCartController {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private PintuanCartManager pintuanCartManager;

    @Autowired
    private PintuanTradeManagerImpl pintuanTradeManagerImpl;

    @Autowired
    private PintuanGoodsManager pintuanGoodsManager;
    @Autowired
    private PintuanManager pintuanManager;

    @ApiOperation(value = "获取购物车页面购物车详情")
    @GetMapping("/cart")
    public CartView cart() {

        CartView cartView = pintuanCartManager.getCart();
        logger.debug("cartView:"+cartView);

        return cartView;
    }


    @ApiOperation(value = "向拼团购物车中加一个sku")
    @PostMapping("/cart/sku")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sku_id", value = "sku ID", dataType = "int", dataTypeClass = Integer.class, paramType = "query", required = true),
            @ApiImplicitParam(name = "num", value = "购买数量", dataType = "int", dataTypeClass = Integer.class, paramType = "query")
    })
    public CartSkuOriginVo addSku(@ApiIgnore @NotNull(message = "sku id不能为空") Long skuId, Integer num) {
        PinTuanGoodsVO pinTuanGoodsVO = pintuanGoodsManager.getDetail(skuId, null);

        Pintuan pintuan = pintuanManager.getModel(pinTuanGoodsVO.getPintuanId());

        if(pintuan!=null && null!=pintuan.getLimitNum() && pintuan.getLimitNum()!=0){
            if (num > pintuan.getLimitNum()) {
                throw new ServiceException(PromotionErrorCode.E402.code(), "购买数量超过限购数量" );
            }
        }

        CartSkuOriginVo cartSkuOriginVo = pintuanCartManager.addSku(skuId, num);
        return cartSkuOriginVo;
    }


    @ApiOperation(value = "创建交易")
    @PostMapping(value = "/trade")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "client", value = "客户端类型", required = false, dataType = "String", dataTypeClass = String.class, paramType = "query", allowableValues = "PC,WAP,NATIVE,REACT"),
            @ApiImplicitParam(name = "pintuan_order_id", value = "拼团订单id，如果为空创建拼团，如果不为空参团", required = false, dataType = "int", dataTypeClass = Integer.class, paramType = "query")
    })
    public TradeVO create(@ApiIgnore String client, @ApiIgnore Long pintuanOrderId) {
        TradeVO tradeVO = this.pintuanTradeManagerImpl.createTrade(client, pintuanOrderId);
        return tradeVO;
    }

}
