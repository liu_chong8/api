/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.mapper.member;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.shoptnt.framework.cache.MybatisRedisCache;
import cn.shoptnt.model.member.dos.ConnectSettingDO;
import cn.shoptnt.model.member.vo.ConnectSettingVO;
import org.apache.ibatis.annotations.CacheNamespace;

import java.util.List;

/**
 * 信任登录后台参数Mapper接口
 * @author duanmingyu
 * @version v1.0
 * @since v7.2.2
 * 2020-07-23
 */
//@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface ConnectSettingMapper extends BaseMapper<ConnectSettingDO> {

    /**
     * 获取后台信任登录参数集合
     * @return
     */
    List<ConnectSettingVO> selectConnectSettingVo();

}
