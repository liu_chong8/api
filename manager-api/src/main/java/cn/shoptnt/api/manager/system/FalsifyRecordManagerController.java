/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.system;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.security.FalsifyRecordParams;
import cn.shoptnt.service.security.FalsifyRecordManager;
import cn.shoptnt.service.security.SignScanManager;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;


/**
 * 篡改记录控制器
 *
 * @author shenyanwu
 * @version v7.0
 * @since v7.0.0
 * 2021-11-21 20:38:26
 */
@RestController
@RequestMapping("/admin/systems/falsify-record")
@Api(tags= "篡改记录API")
@Validated
public class FalsifyRecordManagerController {

    @Autowired
    private FalsifyRecordManager falsifyRecordManager;
    @Autowired
    private SignScanManager signScanManager;

    @ApiOperation(value = "查询篡改记录列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_no", value = "页码", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "query"),
            @ApiImplicitParam(name = "page_size", value = "每页显示数量", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "query")
    })
    @GetMapping
    public WebPage list(@ApiIgnore Long pageNo, @ApiIgnore Long pageSize, FalsifyRecordParams params) {
        params.setPageNo(pageNo);
        params.setPageSize(pageSize);
        return this.falsifyRecordManager.list(params);
    }


    @ApiOperation(value = "修复数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "record_id", value = "篡改记录id", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "query")
    })
    @PutMapping("/{record_id}")
    public void repair(@ApiIgnore @PathVariable("record_id") Long recordId) {
        this.falsifyRecordManager.repair(recordId);
    }

    @ApiOperation(value = "手动触发篡改数据扫描")
    @GetMapping("/scan")
    public void scan() {
        signScanManager.scan();
    }

}
