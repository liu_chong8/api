/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.receiver;

import cn.shoptnt.consumer.core.event.SiteNavigationChangeEvent;
import cn.shoptnt.model.base.rabbitmq.AmqpExchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 站点导航栏变化
 * 
 * @author fk
 * @version v2.0
 * @since v7.0.0 2018年3月23日 上午10:32:15
 */
@Component
public class SiteNavigationChangeReceiver {

	@Autowired(required=false)
	private List<SiteNavigationChangeEvent> events;

	private final Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 导航栏变化
	 * @param clientType PC端 或者 MOBILE端
	 */
	@RabbitListener(bindings = @QueueBinding(
			value = @Queue(value = AmqpExchange.SITE_NAVIGATION_CHANGE + "_QUEUE"),
			exchange = @Exchange(value = AmqpExchange.SITE_NAVIGATION_CHANGE, type = ExchangeTypes.FANOUT)
	))
	public void change(String clientType){
		if(events!=null){
			for(SiteNavigationChangeEvent event : events){
				try {
					event.navigationChange(clientType);
				} catch (Exception e) {
					logger.error("导航栏变化出错", e);
				}
			}
		}
	}
}
