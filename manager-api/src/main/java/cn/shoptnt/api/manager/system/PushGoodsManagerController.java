/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.system;

import cn.shoptnt.client.goods.GoodsClient;
import cn.shoptnt.model.goods.vo.CacheGoods;
import cn.shoptnt.model.system.vo.AppPushSetting;
import cn.shoptnt.service.system.PushManager;
import cn.shoptnt.framework.exception.ResourceNotFoundException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.constraints.NotEmpty;

/**
 * 商品推送api
 *
 * @author zh
 * @version v7.0
 * @date 18/6/6 下午7:57
 * @since v7.0
 */
@RestController
@RequestMapping("/admin/systems/push")
@Api(tags= "商品推送api")
@Validated
public class PushGoodsManagerController {

    @Autowired
    private PushManager pushManager;
    @Autowired
    private GoodsClient goodsClient;

    @GetMapping(value = "/{goods_id}")
    @ApiOperation(value = "商品推送", response = AppPushSetting.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "title", value = "标题", required = true, dataType = "String", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "goods_id", value = "商品id", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "path")
    })
    public String getPushSetting(@NotEmpty(message = "标题不能为空") String title, @PathVariable("goods_id") @ApiIgnore Long goodsId) {
        CacheGoods goodsDO = goodsClient.getFromCache(goodsId);
        if (goodsDO == null) {
            throw new ResourceNotFoundException("此商品不存在");
        }
        //推送Android端
        pushManager.pushGoodsForAndroid(title, goodsId);
        //推送ios端
        pushManager.pushGoodsForIOS(title, goodsId);
        return null;

    }

}
