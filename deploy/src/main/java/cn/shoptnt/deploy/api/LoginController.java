/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.deploy.api;

import cn.shoptnt.deploy.model.Certificate;
import cn.shoptnt.framework.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by 妙贤 on 23/02/2018.
 * 登录控制器
 * @author 妙贤
 * @version 1.0
 * @since 6.4.0
 * 23/02/2018
 */
@RestController
@RequestMapping("/user")
public class LoginController {


    @Autowired
    private Certificate certificate;

    @PostMapping("/login")
    public  void login(String username,String password){

        if (certificate.getPassword().equals(password) && certificate.getUsername().equals(username)) {

        }else {
            throw  new ServiceException("403","用户名密码错误");
        }

    }


}
