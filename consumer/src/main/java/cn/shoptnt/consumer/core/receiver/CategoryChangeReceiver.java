/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.receiver;

import java.util.List;

import cn.shoptnt.model.base.rabbitmq.AmqpExchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cn.shoptnt.consumer.core.event.CategoryChangeEvent;
import cn.shoptnt.model.base.message.CategoryChangeMsg;

/**
 * 分类 变更
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018年3月23日 上午10:29:42
 */
@Component
public class CategoryChangeReceiver {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired(required=false)
	private List<CategoryChangeEvent> events;



	@RabbitListener(bindings = @QueueBinding(
			value = @Queue(value = AmqpExchange.GOODS_CATEGORY_CHANGE + "_QUEUE"),
			exchange = @Exchange(value = AmqpExchange.GOODS_CATEGORY_CHANGE, type = ExchangeTypes.FANOUT)
	))
	public void categoryChange(CategoryChangeMsg categoryChangeMsg){
		
		if(events!=null){
			for(CategoryChangeEvent event : events){
				try {
					event.categoryChange(categoryChangeMsg);
				} catch (Exception e) {
					logger.error("处理商品分类变化消息出错",e);
				}
			}
		}
		
	}
}
