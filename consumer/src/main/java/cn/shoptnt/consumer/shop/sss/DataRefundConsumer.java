/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.shop.sss;

import cn.shoptnt.client.trade.AfterSaleClient;
import cn.shoptnt.consumer.core.event.AfterSaleChangeEvent;
import cn.shoptnt.model.aftersale.dos.RefundDO;
import cn.shoptnt.model.aftersale.enums.ServiceStatusEnum;
import cn.shoptnt.model.aftersale.enums.ServiceTypeEnum;
import cn.shoptnt.model.base.message.AfterSaleChangeMessage;
import cn.shoptnt.client.statistics.RefundDataClient;
import cn.shoptnt.model.statistics.dto.RefundData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * 订单申请通过
 *
 * @author chopper
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2018/6/20 下午2:21
 */
@Component
public class DataRefundConsumer implements AfterSaleChangeEvent {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private RefundDataClient refundDataClient;

    @Autowired
    private AfterSaleClient afterSaleClient;

    /**
     * 售后服务消息处理
     * @param afterSaleChangeMessage
     */
    @Override
    public void afterSaleChange(AfterSaleChangeMessage afterSaleChangeMessage) {
        try {
            //如果售后服务单状态为审核通过 并且 售后服务单类型为退货或者取消订单
            boolean flag = ServiceStatusEnum.PASS.equals(afterSaleChangeMessage.getServiceStatus()) &&
                    (ServiceTypeEnum.RETURN_GOODS.equals(afterSaleChangeMessage.getServiceType()) || ServiceTypeEnum.ORDER_CANCEL.equals(afterSaleChangeMessage.getServiceType()));

            if (flag) {
                //获取退款单
                RefundDO refundDO = this.afterSaleClient.getAfterSaleRefundModel(afterSaleChangeMessage.getServiceSn());

                if (refundDO != null) {
                    //退款消息写入
                    this.refundDataClient.put(new RefundData(refundDO));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("订单售后服务异常：", e);
        }

    }
}
