/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.security;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.security.FalsifyRecord;
import cn.shoptnt.model.security.FalsifyRecordParams;


/**
 * 篡改记录业务层
 *
 * @author shenyanwu
 * @version v2.0
 * @since v7.0.0
 * 2021-11-21 23:19:39
 */
public interface FalsifyRecordManager {

    /**
     * 查询篡改记录分页数据列表
     *
     * @param params 查询参数
     * @return WebPage
     */
    WebPage list(FalsifyRecordParams params);


    /**
     * 修复数据
     *
     * @param recordId 篡改记录id
     * @return WebPage
     */
    void repair(Long recordId);

    /**
     * 添加篡改记录
     * @param falsifyRecord
     */
    void add(FalsifyRecord falsifyRecord);
}
