/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.debugger;

import cn.shoptnt.model.base.vo.EmailVO;
import cn.shoptnt.model.base.rabbitmq.AmqpExchange;
import cn.shoptnt.framework.rabbitmq.MessageSender;
import cn.shoptnt.framework.rabbitmq.MqMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2019-04-24
 */
@RestController
@RequestMapping("/debugger/email")
@ConditionalOnProperty(value = "shoptnt.debugger", havingValue = "true")
public class EmailCheckController {

    @Autowired
    private MessageSender messageSender;

    @GetMapping(value = "/test")
    public String test( String email ) {

        EmailVO emailVO = new EmailVO();
        emailVO.setContent("测试邮件");
        emailVO.setTitle("测试邮件");
        emailVO.setEmail(email);

        this.messageSender.send(new MqMessage(AmqpExchange.EMAIL_SEND_MESSAGE, "emailSendMessageMsg", emailVO));


        return "ok";
    }

}
