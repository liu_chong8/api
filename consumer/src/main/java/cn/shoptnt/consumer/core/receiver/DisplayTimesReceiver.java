/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.receiver;

import cn.shoptnt.model.base.rabbitmq.AmqpExchange;
import cn.shoptnt.client.statistics.DisplayTimesClient;
import cn.shoptnt.model.statistics.dos.GoodsPageView;
import cn.shoptnt.model.statistics.dos.ShopPageView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 流量统计amqp 消费
 *
 * @author Chopper
 * @version v1.0
 * @since v7.0
 * 2018-08-07 下午4:34
 */
@Component
public class DisplayTimesReceiver {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private DisplayTimesClient displayTimesClient;

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.SHOP_VIEW_COUNT + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.SHOP_VIEW_COUNT, type = ExchangeTypes.FANOUT)
    ))
    public void viewShop(List<ShopPageView> shopPageViews) {
        try {

            displayTimesClient.countShop(shopPageViews);
        } catch (Exception e) {
            logger.error("流量整理：店铺 异常", e);
        }
    }


    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.GOODS_VIEW_COUNT + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.GOODS_VIEW_COUNT, type = ExchangeTypes.FANOUT)
    ))
    public void viewGoods(List<GoodsPageView> goodsPageViews) {
        try {
            displayTimesClient.countGoods(goodsPageViews);
        } catch (Exception e) {
            logger.error("流量整理：商品 异常", e);
        }
    }


}
