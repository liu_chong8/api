/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.deploy.enums;

/**
 * ShopTnt业务类型
 *
 * @author 妙贤
 * @version 1.0
 * @since 7.0.0
 * 2018/4/24
 */
public enum ServiceType {

    /**
     * 默认数据库
     */
    DEFAULT_DATABASE("default_database"),

    /**
     * 初始化库1
     */
    SHOP01("分库的1库"),

    /**
     * 初始化库2
     */
    SHOP02("分库的2库"),

    /**
     * xxl-job
     */
    XXL_JOB("xxl-job");

    /**
     * 类型名称
     */
    private String typeName;


    ServiceType(String typeName) {
        this.typeName = typeName;
    }


    /**
     * 获取类型名称
     *
     * @return
     */
    public String getTypeName() {
        return this.typeName;
    }

}
