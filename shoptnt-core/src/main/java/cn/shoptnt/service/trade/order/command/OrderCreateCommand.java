package cn.shoptnt.service.trade.order.command;

import cn.shoptnt.model.trade.order.dto.OrderDTO;
import cn.shoptnt.model.trade.order.vo.CommandResult;

/**
 * 订单创建命令执行接口
 * @author dmy
 * @version 1.0
 * 2022-01-15
 */
public interface OrderCreateCommand {

    /**
     * 订单创建命令执行方法
     * @param orderDTO 订单信息
     * @return
     */
    CommandResult execute(OrderDTO orderDTO);

    /**
     * 订单创建命令执行失败回滚方法
     */
    void rollback(OrderDTO order);

}
