/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.shop.trigger;

import cn.shoptnt.client.promotion.PromotionGoodsClient;
import cn.shoptnt.client.promotion.PromotionScriptClient;
import cn.shoptnt.model.base.message.PromotionScriptMsg;
import cn.shoptnt.model.promotion.seckill.dos.SeckillApplyDO;
import cn.shoptnt.model.promotion.seckill.enums.SeckillGoodsApplyStatusEnum;
import cn.shoptnt.model.promotion.tool.enums.ScriptOperationTypeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import cn.shoptnt.framework.trigger.Interface.TimeTriggerExecuter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 限时抢购促销活动脚本删除延时任务执行器
 * @author duanmingyu
 * @version v1.0
 * @since v7.2.0
 * 2020-02-19
 */
@Component("seckillScriptTimeTriggerExecuter")
public class SeckillScriptTimeTriggerExecuter implements TimeTriggerExecuter {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private PromotionScriptClient promotionScriptClient;

    @Autowired
    private PromotionGoodsClient promotionGoodsClient;

    @Override
    public void execute(Object object) {

        PromotionScriptMsg promotionScriptMsg = (PromotionScriptMsg) object;

        //如果是限时抢购促销活动结束
        if (ScriptOperationTypeEnum.DELETE.equals(promotionScriptMsg.getOperationType())) {

            //获取促销活动ID
            Long promotionId = promotionScriptMsg.getPromotionId();

            //获取所有参与活动审核通过的商品集合
            List<SeckillApplyDO> goodsList = this.promotionGoodsClient.getSeckillGoodsList(promotionId, SeckillGoodsApplyStatusEnum.PASS.value());

            //清除促销活动脚本信息
            this.promotionScriptClient.deleteCacheScript(promotionId, goodsList);

            this.logger.debug("促销活动[" + promotionScriptMsg.getPromotionName() + "]结束，id=[" + promotionId + "]");
        }

    }
}
