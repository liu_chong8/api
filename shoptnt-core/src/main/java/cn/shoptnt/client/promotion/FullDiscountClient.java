package cn.shoptnt.client.promotion;

import cn.shoptnt.model.promotion.fulldiscount.dos.FullDiscountDO;

/**
 * @author zs
 * @version v2.0
 * @Description: 满减满赠活动client接口
 * @date 2021-12-16
 * @since v7.2.2
 */
public interface FullDiscountClient {

    /**
     * 查询一个满减满赠活动
     *
     * @param id 主键
     * @return
     */
    FullDiscountDO getModel(Long id);

}
