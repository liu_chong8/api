/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.mapper.promotion;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.shoptnt.framework.cache.MybatisRedisCache;
import cn.shoptnt.model.promotion.coupon.dos.CouponDO;
import cn.shoptnt.model.promotion.coupon.vo.CouponVO;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Param;

/**
 * CouponMapper的Mapper
 *
 * @author fk
 * @version 1.0
 * @since 7.1.0
 * 2020/7/21
 */
@CacheNamespace(implementation = MybatisRedisCache.class, eviction = MybatisRedisCache.class)
public interface CouponMapper extends BaseMapper<CouponDO> {

    /**
     * 查询优惠券列表
     *
     * @param page    分页
     * @param wrapper 条件
     * @return
     */
    IPage<CouponVO> selectCouponPage(Page page, @Param("ew") QueryWrapper<CouponDO> wrapper);
}
