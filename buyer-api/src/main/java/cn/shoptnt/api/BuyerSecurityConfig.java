/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api;

import cn.shoptnt.model.base.DomainHelper;
import cn.shoptnt.framework.context.ApplicationContextHolder;
import cn.shoptnt.framework.security.AuthenticationService;
import cn.shoptnt.framework.security.TokenAuthenticationFilter;
import cn.shoptnt.framework.security.message.UserDisableReceiver;
import cn.shoptnt.framework.security.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.header.writers.frameoptions.AllowFromStrategy;
import org.springframework.security.web.header.writers.frameoptions.StaticAllowFromStrategy;
import org.springframework.security.web.header.writers.frameoptions.XFrameOptionsHeaderWriter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * Created by 妙贤 on 2018/3/12.
 * 买家安全配置
 *
 * @author 妙贤
 * @version 1.0
 * @since 7.0.0
 * 2018/3/12
 */
@Configuration
@EnableWebSecurity
public class BuyerSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DomainHelper domainHelper;

    @Autowired
    private BuyerAuthenticationService buyerAuthenticationService;

    @Autowired
    private AccessDeniedHandler accessDeniedHandler;

    @Autowired
    private AuthenticationEntryPoint authenticationEntryPoint;


    /**
     * 定义 buyer 工程的权限
     *
     * @param http
     * @throws Exception
     */
    @Override
    public void configure(HttpSecurity http) throws Exception {

        http.cors().configurationSource((CorsConfigurationSource) ApplicationContextHolder.getBean("corsConfigurationSource")).and().csrf().disable()
                //禁用session
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()

                //定义验权失败返回格式
                .exceptionHandling().accessDeniedHandler(accessDeniedHandler).authenticationEntryPoint(authenticationEntryPoint).and()
                .authorizeRequests()
                .and()
                .addFilterBefore(new TokenAuthenticationFilter(buyerAuthenticationService),
                        UsernamePasswordAuthenticationFilter.class);
        //过滤掉swagger的路径
        http.authorizeRequests().antMatchers("/v3/api-docs**", "/configuration/ui", "/swagger-resources/**","/doc.html/**","/v2/api-docs-ext/**","/v2/api-docs/**",
                "/configuration/security", "/swagger-ui/**", "/webjars/**","/actuator/**").permitAll()
        //过滤掉不需要买家权限的 shop 的 api
        .antMatchers("/shops/list", "/shops/{spring:[0-9]+}",
                "/shops/cats/{spring:[0-9]+}",
                "/shops/navigations/{spring:[0-9]+}", "/shops/sildes/{spring:[0-9]+}").permitAll()
        //过滤掉不需要买家权限的 members 的 api
        .antMatchers("/members/asks/goods/{spring:[0-9]+}",
                "/members/asks/detail/{spring:[0-9]+}", "/members/history", "/members/logout*",
                "/members/asks/relation/{spring:[0-9]+}/{spring:[0-9]+}", "/members/asks/reply/list/{spring:[0-9]+}",
                "/members/comments/goods/{spring:[0-9]+}", "/members/comments/goods/{spring:[0-9]+}/count").permitAll()
        //过滤掉不需要买家权限的 payment 的 api
        .antMatchers("/payment/weixin/**", "/payment/callback/**",
                "/payment/order/pay/query/**", "/payment/return/**").permitAll()

        //过滤掉不需要买家权限的 pintuan 的 api
        .antMatchers("/pintuan/orders/**", "/pintuan/goods",
                "/pintuan/goods/**").permitAll()

        //过滤掉不需要买家权限的 payment 的 api
        .antMatchers("/payment/weixin/**", "/payment/callback/**",
                "/payment/order/pay/query/**", "/payment/return/**").permitAll()

        //过滤掉不需要买家权限的 登录和支付相关 的 api
        .antMatchers("/passport/connect/pc/WECHAT/**",
                "/passport/login-binder/pc/**", "/passport/**", "/account-binder/**", "/wechat/**", "/qq/**",
                "/alipay/**", "/weibo/**", "/apple/**").permitAll()

        //过滤掉不需要买家权限的api
        .antMatchers("/goods/**", "/pages/**", "/focus-pictures/**",
                "/actuator/**", "/debugger/**", "/jquery.min.js",
                "/promotions/**", "/view", "/trade/goods/**", "/distribution/su/**").permitAll();

        //定义有买家权限才可以访问
        http.authorizeRequests().anyRequest().hasRole(Role.BUYER.name());
        http.headers().addHeaderWriter(xFrameOptionsHeaderWriter());
        //禁用缓存
        http.headers().cacheControl().and()
                .contentSecurityPolicy("script-src  'self' 'unsafe-inline' ;" +
                        " frame-ancestors " + domainHelper.getBuyerDomain());

    }

    public XFrameOptionsHeaderWriter xFrameOptionsHeaderWriter() throws URISyntaxException {

        String buyerDomain = domainHelper.getBuyerDomain();

        URI uri = new URI(buyerDomain);

        AllowFromStrategy allowFromStrategy = new StaticAllowFromStrategy(uri);

        XFrameOptionsHeaderWriter xFrameOptionsHeaderWriter = new XFrameOptionsHeaderWriter(allowFromStrategy);

        return xFrameOptionsHeaderWriter;
    }

    /**
     * 定义跨域配置
     *
     * @return
     */
    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOriginPattern("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        source.registerCorsConfiguration("/**", config);
        return source;
    }

    @Autowired
    private List<AuthenticationService> authenticationServices;


    @Bean
    public UserDisableReceiver userDisableReceiver() {
        return new UserDisableReceiver(authenticationServices);
    }

}
