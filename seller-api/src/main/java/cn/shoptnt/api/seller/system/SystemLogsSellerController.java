/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.system;

import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.system.dos.SystemLogs;
import cn.shoptnt.model.system.dto.SystemLogsParam;
import cn.shoptnt.service.system.SystemLogsManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;


/**
 * 系统日志控制器
 * @author fk
 * @version v2.0
 * @since v2.0
 * 2021-03-22 16:05:59
 */
@RestController
@RequestMapping("/seller/system-logs")
@Api(tags= "系统日志相关API")
public class SystemLogsSellerController {
	
	@Autowired
	private SystemLogsManager systemLogsManager;
				

	@ApiOperation(value	= "查询系统日志列表", response = SystemLogs.class)
	@ApiImplicitParams({
		 @ApiImplicitParam(name	= "page_no",	value =	"页码",	required = true, dataType = "long", dataTypeClass = Long.class,	paramType =	"query"),
		 @ApiImplicitParam(name	= "page_size",	value =	"每页显示数量",	required = true, dataType = "long", dataTypeClass = Long.class,	paramType =	"query")
	})
	@GetMapping
	public WebPage list(SystemLogsParam param, @ApiIgnore Long pageNo, @ApiIgnore Long pageSize)	{

		param.setPageNo(pageNo);
		param.setPageSize(pageSize);
		param.setSellerId(UserContext.getSeller().getSellerId());

		return this.systemLogsManager.list(param, "seller");
	}
				
	
	@GetMapping(value =	"/{id}")
	@ApiOperation(value	= "查询一个系统日志")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "id",	value = "要查询的系统日志主键",required = true, dataType = "long", dataTypeClass = Long.class,	paramType = "path")
	})
	public	SystemLogs get(@PathVariable Long id)	{
		
		SystemLogs systemLogs = this.systemLogsManager.getModel(id);
		
		return	systemLogs;
	}
				
}