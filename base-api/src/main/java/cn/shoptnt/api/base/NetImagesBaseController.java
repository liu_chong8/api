/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.base;

import cn.shoptnt.framework.util.Base64;
import cn.shoptnt.framework.util.NetImageUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author liuyulei
 * @version 1.0
 * @since 7.2.2
 * 2020/11/17 0017  10:32
 */
@RestController
@RequestMapping("/net-image")
@Api(tags= "网络图片处理API")
public class NetImagesBaseController {

    @GetMapping(value = "/base64")
    @ApiOperation(value = "获取网络图片的base64编码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "url", value = "网络图片地址", required = true, dataType = "String", dataTypeClass = String.class, paramType = "query")
    })
    public String base64Images(String url) {
        byte[] urlByte = NetImageUtil.getImageFromNetByUrl(url);
        String base64Image = Base64.encode(urlByte);
        return base64Image;
    }
}
