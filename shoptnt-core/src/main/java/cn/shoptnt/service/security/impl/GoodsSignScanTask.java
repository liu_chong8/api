/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.security.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.shoptnt.client.goods.GoodsClient;
import cn.shoptnt.model.goods.dos.GoodsDO;
import cn.shoptnt.model.member.dos.Member;
import cn.shoptnt.model.security.ScanModuleDTO;
import cn.shoptnt.model.security.ScanResult;
import cn.shoptnt.service.security.BaseSignScanTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 商品篡改数据扫描任务
 * @author 妙贤
 * @version 1.0
 * @data 2021/11/19 16:23
 **/
@Service("goodsSignScanTask")
public class GoodsSignScanTask extends BaseSignScanTask<GoodsDO> {

    @Autowired
    private GoodsClient goodsClient;


    @Override
    protected ScanResult scanModule(String rounds) throws IllegalAccessException {
        QueryWrapper<GoodsDO> queryWrapper = createWrapper(rounds);
        ScanModuleDTO<GoodsDO> scanModuleDTO = new ScanModuleDTO<>(rounds, queryWrapper, pageSize);
        return goodsClient.scanModule(scanModuleDTO);
    }


    @Override
    public void reSign() {
        goodsClient.reSign();
    }
}
