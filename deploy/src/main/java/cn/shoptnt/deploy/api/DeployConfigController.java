/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.deploy.api;

import cn.shoptnt.deploy.model.Elasticsearch;
import cn.shoptnt.deploy.model.Rabbitmq;
import cn.shoptnt.deploy.model.Redis;
import cn.shoptnt.deploy.service.ElasticsearchManager;
import cn.shoptnt.deploy.service.RabbitmqManager;
import cn.shoptnt.deploy.service.RedisManager;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 部署  配置信息控制器
 * @author 妙贤
 * @version 1.0
 * @since 7.0.0
 * 2018/5/9
 */
@RestController
@RequestMapping("/data/deploys/{deployId}")
public class DeployConfigController {

    @Autowired
    private RedisManager redisManager;

    @Autowired
    private RabbitmqManager rabbitmqManager;

    @Autowired
    private ElasticsearchManager elasticsearchManager;

    @GetMapping(value =	"/redis")
    @ApiOperation(value	= "查询某个部署的redis配置")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "deploy_id",	value = "部署的i",	required = true, dataType = "int", dataTypeClass = Integer.class,	paramType = "path")
    })
    public Redis getRedis (@PathVariable Long deployId){
        Redis redis = redisManager.getByDeployId(deployId);
        return redis;
    }


    @GetMapping(value =	"/rabbitmq")
    @ApiOperation(value	= "查询某个部署的rabbitmq配置")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "deploy_id",	value = "部署的i",	required = true, dataType = "int", dataTypeClass = Integer.class,	paramType = "path")
    })
    public Rabbitmq getRabbitmq (@PathVariable Integer deployId){
        Rabbitmq rabbitmq = rabbitmqManager.getByDeployId(deployId);
        return rabbitmq;
    }


    @GetMapping(value =	"/elasticsearch")
    @ApiOperation(value	= "查询某个部署的elasticsearch配置")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "deploy_id",	value = "部署的id",	required = true, dataType = "int", dataTypeClass = Integer.class,	paramType = "path")
    })
    public Elasticsearch getElasticsearch (@PathVariable Long deployId){
        return  elasticsearchManager.getByDeployId(deployId);
    }


}
