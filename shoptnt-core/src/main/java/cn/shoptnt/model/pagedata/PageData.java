/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.pagedata;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.Objects;


/**
 * 楼层实体
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-05-21 16:39:22
 */
@TableName("es_page")
@ApiModel
public class PageData implements Serializable {

    private static final long serialVersionUID = 3806389481183972L;

    /**
     * 主键id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;
    /**
     * 楼层名称
     */
    @ApiModelProperty(name = "page_name", value = "微页面标题", required = true)
    @NotEmpty(message = "名称不能为空")
    private String pageName;
    /**
     * 楼层数据
     */
    @ApiModelProperty(name = "page_data", value = "页面元素", required = true)
    @NotEmpty(message = "页面数据不能为空")
    private String pageData;
    /**
     * 所属商家
     */
    @ApiModelProperty(name = "seller_id", value = "所属商家，商城的存值为0", required = true)
    private Long sellerId;
    /**
     * 发布状态
     */
    @ApiModelProperty(name = "publish_status", value = "发布状态YES/NO", required = true)
    private String publishStatus;
    /**
     * 是否是首页
     */
    @ApiModelProperty(name = "is_index", value = "是否是店铺/商城首页，YES/NO", required = true)
    private String isIndex;
    /**
     * 备注
     */
    @ApiModelProperty(name = "remark", value = "备注", required = true)
    private String remark;
    /**
     * 添加时间
     */
    @ApiModelProperty(name = "create_time", value = "添加时间", hidden = true)
    private Long createTime;
    /**
     * 客户端类型
     */
    @ApiModelProperty(name = "client_type", value = "客户端类型：PC/MOBILE", hidden = true)
    private String clientType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @PrimaryKeyField


    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public String getPageData() {
        return pageData;
    }

    public void setPageData(String pageData) {
        this.pageData = pageData;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getPublishStatus() {
        return publishStatus;
    }

    public void setPublishStatus(String publishStatus) {
        this.publishStatus = publishStatus;
    }

    public String getIsIndex() {
        return isIndex;
    }

    public void setIsIndex(String isIndex) {
        this.isIndex = isIndex;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "PageData{" +
                "id=" + id +
                ", pageName='" + pageName + '\'' +
                ", pageData='" + pageData + '\'' +
                ", sellerId=" + sellerId +
                ", publishStatus='" + publishStatus + '\'' +
                ", isIndex='" + isIndex + '\'' +
                ", remark='" + remark + '\'' +
                ", createTime=" + createTime +
                ", clientType='" + clientType + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PageData)) {
            return false;
        }
        PageData pageData1 = (PageData) o;
        return Objects.equals(id, pageData1.id) && Objects.equals(pageName, pageData1.pageName) && Objects.equals(pageData, pageData1.pageData) && Objects.equals(sellerId, pageData1.sellerId) && Objects.equals(publishStatus, pageData1.publishStatus) && Objects.equals(isIndex, pageData1.isIndex) && Objects.equals(remark, pageData1.remark) && Objects.equals(createTime, pageData1.createTime) && Objects.equals(clientType, pageData1.clientType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, pageName, pageData, sellerId, publishStatus, isIndex, remark, createTime, clientType);
    }
}
