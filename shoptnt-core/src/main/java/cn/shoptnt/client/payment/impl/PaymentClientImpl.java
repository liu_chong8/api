/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.payment.impl;

import cn.shoptnt.client.payment.PaymentClient;
import cn.shoptnt.model.payment.dos.PaymentMethodDO;
import cn.shoptnt.model.payment.dto.PayParam;
import cn.shoptnt.service.payment.PaymentBillManager;
import cn.shoptnt.service.payment.PaymentManager;
import cn.shoptnt.service.payment.PaymentMethodManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 支付中心client实现
 *
 * @author fk
 * @version v7.0
 * @date 20/3/9 下午3:51
 * @since v7.2.1
 */
@Service
@ConditionalOnProperty(value="shoptnt.product", havingValue="stand")
public class PaymentClientImpl implements PaymentClient {

    @Autowired
    private PaymentManager paymentManager;

    @Autowired
    private PaymentBillManager paymentBillManager;

    @Autowired
    private PaymentMethodManager paymentMethodManager;

    @Override
    public Map pay(PayParam param) {

        return paymentManager.pay(param);
    }

    @Override
    public String queryBill(String subSn,String serviceType) {

        return paymentManager.queryResult(subSn, serviceType);
    }

    @Override
    public PaymentMethodDO getByPluginId(String pluginId) {
        return paymentMethodManager.getByPluginId(pluginId);
    }
}
