-- 订单表增加签名相关字段
ALTER TABLE `es_order`
ADD COLUMN `sign` varchar(255) NULL AFTER `balance`,
ADD COLUMN `sign_result` int(1) NULL DEFAULT 0 AFTER `sign`,
ADD COLUMN `scan_rounds` varchar(50) NULL AFTER `sign_result`;

-- 商品表增加签名相关字段
ALTER TABLE `es_goods`
    ADD COLUMN `sign` varchar(255) NULL AFTER `goods_video`,
    ADD COLUMN `sign_result` int(1) NULL DEFAULT 0 AFTER `sign`,
    ADD COLUMN `scan_rounds` varchar(50) NULL AFTER `sign_result`;

-- sku表增加签名相关字段
ALTER TABLE `es_goods_sku`
    ADD COLUMN `sign` varchar(255) NULL AFTER `hash_code`,
    ADD COLUMN `sign_result` int(1) NULL DEFAULT 0 AFTER `sign`,
    ADD COLUMN `scan_rounds` varchar(50) NULL AFTER `sign_result`;


-- 交易表增加签名相关字段
ALTER TABLE `es_trade`
    ADD COLUMN `sign` varchar(255) NULL AFTER `balance`,
    ADD COLUMN `sign_result` int(1) NULL DEFAULT 0 AFTER `sign`,
    ADD COLUMN `scan_rounds` varchar(50) NULL AFTER `sign_result`;

-- 会员表增加签名相关字段
ALTER TABLE `es_member`
    ADD COLUMN `sign` varchar(255) NULL AFTER `nickname`,
    ADD COLUMN `sign_result` int(1) NULL DEFAULT 0 AFTER `sign`,
    ADD COLUMN `scan_rounds` varchar(50) NULL AFTER `sign_result`;

-- 会员钱包增加签名相关字段
ALTER TABLE `es_member_wallet`
    ADD COLUMN `sign` varchar(255) NULL AFTER `deposite_password`,
    ADD COLUMN `sign_result` int(1) NULL DEFAULT 0 AFTER `sign`,
    ADD COLUMN `scan_rounds` varchar(50) NULL AFTER `sign_result`;



DROP TABLE IF EXISTS `es_security_module`;
CREATE TABLE `es_security_module` (
 `module_id` bigint(20) NOT NULL,
 `module_name` varchar(50) COLLATE utf8_bin DEFAULT NULL,
 `rounds` varchar(50) COLLATE utf8_bin DEFAULT NULL,
 `state` varchar(50) COLLATE utf8_bin DEFAULT NULL,
 PRIMARY KEY (`module_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


INSERT INTO `es_security_module` VALUES (0, 'member', '1', 'finished');
INSERT INTO `es_security_module` VALUES (1, 'order', '1', 'finished');
INSERT INTO `es_security_module` VALUES (2, 'goods', '1', 'finished');
INSERT INTO `es_security_module` VALUES (3, 'trade', '1', 'finished');
INSERT INTO `es_security_module` VALUES (4, 'deposit', '1', 'finished');
INSERT INTO `es_security_module` VALUES (5, 'sku', '1', 'finished');


CREATE TABLE `es_falsify_record` (
 `record_id` bigint(20) NOT NULL,
 `module` varchar(50) COLLATE utf8_bin DEFAULT NULL,
 `module_name` varchar(50) COLLATE utf8_bin DEFAULT NULL,
 `data_id` bigint(20) DEFAULT NULL,
 `state` varchar(50) COLLATE utf8_bin DEFAULT NULL,
 PRIMARY KEY (`record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `es_falsify_record`
ADD INDEX `ind_falsify_record`(`module`, `state`);


-- 日志表
CREATE TABLE `es_system_logs` (
 `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
 `method` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '请求方法',
 `params` longtext COLLATE utf8mb4_unicode_ci COMMENT '请求参数',
 `operate_time` bigint(20) DEFAULT NULL COMMENT '操作时间',
 `operate_detail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作描述',
 `operate_ip` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ip地址',
 `operator_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '操作员',
 `operator_id` bigint(20) DEFAULT NULL COMMENT '操作员id，管理员id/会员id',
 `seller_id` bigint(20) DEFAULT NULL COMMENT '商家id, 店员要存储所属商家，管理端存0',
 `level` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
 `client` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
 PRIMARY KEY (`id`) USING BTREE,
 KEY `ind_sys_log` (`client`,`level`)
)  ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `es_shop_detail`
CHANGE COLUMN `company_phone` `company_phone` varchar(255) DEFAULT NULL COMMENT '公司电话';

-- 会员表增加会员状态字段 add by cs 2021-11-20
ALTER TABLE `es_member` ADD COLUMN `status` INT(1) DEFAULT 1 NULL COMMENT '用户状态:1:正常;0:休眠;';



ALTER TABLE `es_trade`
CHANGE COLUMN `consignee_mobile` `consignee_mobile` varchar(255) DEFAULT NULL COMMENT '收货人手机号';

ALTER TABLE `es_order`
CHANGE COLUMN `ship_mobile` `ship_mobile` varchar(255) DEFAULT NULL COMMENT '收货人手机';

-- 增加安全设置数据 add by cs 2021-11-24
insert into `es_settings` (`id`, `cfg_value`, `cfg_group`) values('489','{\"retry_cycle\":11,\"retry_times\":2,\"lock_duration\":44,\"sleep_threshold\":1,\"password_auth\":1,\"message_auth\":0,\"scanning_time\":2,\"mobile\":\"13122255544\"}','ACCOUNT');

DROP TABLE IF EXISTS `es_page`;
CREATE TABLE `es_page` (
    `id` bigint(20) NOT NULL,
    `page_name` varchar(50) DEFAULT NULL COMMENT '微页面标题',
    `page_data` longtext COMMENT '页面元素',
    `seller_id` bigint(20) DEFAULT NULL COMMENT '所属商家，商城的存值为0',
    `client_type` varchar(20) DEFAULT NULL COMMENT '客户端PC/MOBILE',
    `publish_status` varchar(10) DEFAULT NULL COMMENT '发布状态YES/NO',
    `is_index` varchar(10) DEFAULT NULL COMMENT '是否是店铺/商城首页',
    `remark` varchar(255) DEFAULT NULL COMMENT '备注',
    `create_time` bigint(20) DEFAULT NULL COMMENT '添加时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='楼层装修';


ALTER TABLE `es_shop_detail`
CHANGE COLUMN `bank_number` `bank_number` varchar(255) DEFAULT NULL COMMENT '银行开户账号',
CHANGE COLUMN `legal_id` `legal_id` varchar(255) DEFAULT NULL COMMENT '法人身份证';

-- ----------------------------
-- Table structure for `es_authentication`
-- ----------------------------
DROP TABLE IF EXISTS `es_authentication`;
CREATE TABLE `es_authentication` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `admin_id` bigint(20) DEFAULT NULL COMMENT '管理员id',
  `password_auth` int(11) DEFAULT NULL COMMENT '是否开启密码验证',
  `password` varchar(100) DEFAULT NULL COMMENT '密码',
  `message_auth` int(11) DEFAULT NULL COMMENT '是否开启短信验证',
  `mobile` varchar(20) DEFAULT NULL COMMENT '手机号码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT=' ';

-- ----------------------------
-- Records of es_authentication
-- ----------------------------
INSERT INTO `es_authentication` VALUES ('1462358155667787778', '1', '1', '111111', '0', '');
INSERT INTO `es_authentication` VALUES ('1463436503638638593', '8', '1', '15cefee6071a38058110c203e3402a3b', '0', '15321975080');

ALTER TABLE `es_falsify_record` ADD COLUMN `san_time` bigint(20) NOT NULL COMMENT '发现时间（扫描发现的时刻）' AFTER `state`;
ALTER TABLE `es_member` CHANGE COLUMN `midentity` `midentity` varchar(255) DEFAULT NULL COMMENT '身份证号';

-- 修改订单状态改变的时间配置
delete from es_settings where cfg_group = 'TRADE';
INSERT INTO `es_settings` VALUES (486, '{\"cancel_order_day\":1,\"cancel_order_hour\":0,\"cancel_order_minutes\":0,\"rog_order_day\":7,\"comment_order_day\":7,\"service_expired_day\":7,\"complete_order_day\":3,\"complete_order_pay\":1,\"complain_expired_day\":10}', 'TRADE');

-- 增加微页面关联的商品对照表
DROP TABLE IF EXISTS `es_page_goods`;
CREATE TABLE `es_page_goods` (
  `id` bigint(20) NOT NULL,
  `goods_id` bigint(20) NOT NULL,
  `page_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 更新管理端菜单初始数据 add by syw 2021-12-01
INSERT INTO `es_menu` VALUES ('1', '0', '商品', '', 'goods', '/admin/goods.*', '0', ',1,', '1');
INSERT INTO `es_menu` VALUES ('2', '0', '订单', '', 'order', '/admin/trade.*', '0', ',2,', '1');
INSERT INTO `es_menu` VALUES ('3', '0', '会员', '', 'member', '/admin/members.*', '0', ',3,', '1');
INSERT INTO `es_menu` VALUES ('4', '0', '门店', '', 'shop', '/admin/shops.*', '0', ',4,', '1');
INSERT INTO `es_menu` VALUES ('5', '0', '促销', '', 'promotions', '/admin/promotion.*', '0', ',5,', '1');
INSERT INTO `es_menu` VALUES ('6', '0', '页面设置', null, 'page', '/admin/pages.*', '-1', ',6,', '1');
INSERT INTO `es_menu` VALUES ('7', '0', '统计', '', 'statistics', '/admin/statistics.*', '0', ',7,', '1');
INSERT INTO `es_menu` VALUES ('8', '0', '设置', '', 'setting', '/admin/systems.*', '0', ',8,', '1');
INSERT INTO `es_menu` VALUES ('9', '0', '运营', '', 'operations', '/admin/system.*', '0', ',9,', '1');
INSERT INTO `es_menu` VALUES ('10', '1', '商品列表', null, 'goodsList', '(/admin/goods.*)|(/admin/shops/list)', '0', ',1,10,', '2');
INSERT INTO `es_menu` VALUES ('11', '1', '商品审核', null, 'goodsAudit', '(/admin/goods.*)|(/admin/shops/list)', '0', ',1,11,', '2');
INSERT INTO `es_menu` VALUES ('12', '1', '商品设置', null, 'goodsSetting', '/admin/goods.*', '0', ',1,11,', '2');
INSERT INTO `es_menu` VALUES ('13', '12', '分类列表', null, 'categoryList', '(/admin/goods/categories.*)|(/admin/goods/parameters.*)|(/admin/goods/parameter-groups.*)', '0', ',1,12,12,', '3');
INSERT INTO `es_menu` VALUES ('14', '12', '品牌列表', null, 'brandList', '/admin/goods/brands.*', '0', ',1,12,13,', '3');
INSERT INTO `es_menu` VALUES ('15', '12', '规格列表', null, 'specList', '/admin/goods/specs.*', '0', ',1,12,14,', '3');
INSERT INTO `es_menu` VALUES ('16', '2', '订单列表', null, 'orderList', '(/admin/trade/orders.*)|(/admin/shops.*)', '0', ',2,16,', '2');
INSERT INTO `es_menu` VALUES ('17', '142', '售后服务', null, 'serviceList', '/admin/after-sales.*', '0', ',142,17,', '2');
INSERT INTO `es_menu` VALUES ('18', '142', '退款单', null, 'refundList', '/admin/after-sales.*', '0', ',142,18,', '2');
INSERT INTO `es_menu` VALUES ('19', '2', '收款单', null, 'collectionList', '/admin/trade/orders/pay-log.*', '0', ',2,19,', '2');
INSERT INTO `es_menu` VALUES ('20', '2', '开票历史', null, 'receiptHistory', '/admin/members/receipts.*', '0', ',2,20,', '2');
INSERT INTO `es_menu` VALUES ('21', '3', '专票资质', null, 'receiptExamine', '/admin/members/zpzz.*', '0', ',3,21,', '2');
INSERT INTO `es_menu` VALUES ('22', '2', '投诉管理', null, 'complaint', '(/admin/trade/order-complains.*)|(/admin/systems/complain-topics.*)', '-1', ',2,22,', '2');
INSERT INTO `es_menu` VALUES ('23', '22', '投诉列表', null, 'complaintList', '/admin/trade/order-complains.*', '-1', ',2,22,23,', '3');
INSERT INTO `es_menu` VALUES ('24', '22', '投诉主题', null, 'subjectList', '/admin/systems/complain-topics.*', '-1', ',2,22,24,', '3');
INSERT INTO `es_menu` VALUES ('25', '3', '会员列表', '', 'memberList', '(/admin/members.*)|(/admin/trade/orders.*)', '0', ',3,25,', '2');
INSERT INTO `es_menu` VALUES ('26', '25', '会员列表1', '', 'memberList1', '(/admin/members.*)|(/admin/trade/orders.*)', '-1', ',3,25,26,', '3');
INSERT INTO `es_menu` VALUES ('27', '3', '会员回收站', null, 'memberRecycle', '/admin/members.*', '0', ',3,27,', '2');
INSERT INTO `es_menu` VALUES ('28', '3', '评论与咨询', null, 'goodsComment', '/admin/members.*', '-1', ',3,28,', '2');
INSERT INTO `es_menu` VALUES ('29', '28', '评论', '', 'goodsCommentList', '/admin/members/comments.*', '-1', ',3,28,29,', '3');
INSERT INTO `es_menu` VALUES ('30', '28', '咨询列表', null, 'goodsAskList', '/admin/members/asks.*', '-1', ',3,28,30,', '3');
INSERT INTO `es_menu` VALUES ('31', '3', '预存款', null, 'preDeposit', '/admin/members/deposit.*', '0', ',3,31,', '2');
INSERT INTO `es_menu` VALUES ('32', '31', '充值列表', null, 'rechargeList', '/admin/members/deposit.*', '0', ',3,31,32,', '3');
INSERT INTO `es_menu` VALUES ('33', '31', '预存款列表', null, 'preDepositDetail', '/admin/members/deposit.*', '0', ',3,31,33,', '3');
INSERT INTO `es_menu` VALUES ('34', '3', '站内消息', '', 'notificationHistory', '/admin/systems/messages.*', '-1', ',3,34,', '2');
INSERT INTO `es_menu` VALUES ('35', '4', '管理店铺', null, 'shopManage', '/admin/shops,*', '0', ',4,35,', '2');
INSERT INTO `es_menu` VALUES ('36', '35', '店铺列表', null, 'shopList', '(/admin/shops.*)|(/admin/goods/categories.*)', '0', ',4,35,36,', '3');
INSERT INTO `es_menu` VALUES ('37', '35', '店铺审核', null, 'shopAudit', '/admin/shops.*', '0', ',4,35,37,', '3');
INSERT INTO `es_menu` VALUES ('41', '4', '店铺结算单', null, 'settlement', '/admin/order/bills.*', '0', ',4,41,', '2');
INSERT INTO `es_menu` VALUES ('42', '5', '团购', null, 'groupBuyManage', '/admin/promotion.*', '0', ',5,42,', '2');
INSERT INTO `es_menu` VALUES ('43', '42', '团购列表', null, 'groupBuyList', '/admin/promotion.*', '0', ',5,42,43,', '3');
INSERT INTO `es_menu` VALUES ('44', '42', '团购分类', null, 'groupBuyCategory', '/admin/promotion/group-buy-cats.*', '0', ',5,42,44,', '3');
INSERT INTO `es_menu` VALUES ('45', '5', '积分商城', null, 'pointsMallManage', '/admin/promotion.*', '0', ',5,45,', '2');
INSERT INTO `es_menu` VALUES ('46', '45', '积分商品', null, 'pointsGoods', '/admin/goods.*', '0', ',5,45,46,', '3');
INSERT INTO `es_menu` VALUES ('47', '45', '积分分类', null, 'pointsClassify', '/admin/promotion/exchange-cats.*', '0', ',5,45,47,', '3');
INSERT INTO `es_menu` VALUES ('48', '5', '限时抢购', null, 'seckillList', '/admin/promotion.*', '0', ',5,48,', '2');
INSERT INTO `es_menu` VALUES ('49', '5', '优惠券', null, 'couponList', '/admin/promotion/coupons.*', '0', ',5,49,', '2');
INSERT INTO `es_menu` VALUES ('50', '5', '拼团活动', null, 'assembleList', '(/admin/promotion/pintuan.*)|(/admin/shops/list)', '0', ',5,50,', '2');
INSERT INTO `es_menu` VALUES ('51', '6', 'PC端装修', null, 'pcDecoration', '/admin/page.*', '-1', ',6,51,', '2');
INSERT INTO `es_menu` VALUES ('52', '51', '楼层装修', null, 'pcFloorManage', '/admin/pages.*', '0', ',6,51,52,', '3');
INSERT INTO `es_menu` VALUES ('53', '51', '焦点图管理', null, 'pcFocusManage', '/admin/focus-pictures.*', '0', ',6,51,53,', '3');
INSERT INTO `es_menu` VALUES ('54', '6', '移动端装修', null, 'mobileDecoration', '/admin/page.*', '-1', ',6,54,', '2');
INSERT INTO `es_menu` VALUES ('55', '54', '楼层装修', null, 'mobileFloorManage', '/admin/pages.*', '0', ',6,54,55,', '3');
INSERT INTO `es_menu` VALUES ('56', '54', '焦点图管理', null, 'mobileFocusManage', '/admin/focus-pictures.*', '0', ',6,54,56,', '3');
INSERT INTO `es_menu` VALUES ('57', '6', '页面设置', null, 'pageSetting', '/admin/page.*', '-1', ',6,57,', '2');
INSERT INTO `es_menu` VALUES ('59', '57', '移动端导航菜单', null, 'mobileSiteMenu', '/admin/pages/site-navigations.*', '0', ',6,57,59,', '3');
INSERT INTO `es_menu` VALUES ('60', '57', '热门关键字', null, 'hotKeyword', '/admin/pages/hot-keywords.*', '0', ',6,57,60,', '3');
INSERT INTO `es_menu` VALUES ('61', '7', '会员分析', null, 'memberAnalysis', '/admin/statistics.*', '0', ',7,61,', '2');
INSERT INTO `es_menu` VALUES ('62', '61', '会员下单量', null, 'orderAmount', '(/admin/statistics.*)|(/admin/shops/list)', '0', ',7,61,62,', '3');
INSERT INTO `es_menu` VALUES ('63', '61', '新增会员', '', 'addedMember', '/admin/statistics.*', '0', ',7,61,63,', '3');
INSERT INTO `es_menu` VALUES ('64', '7', '商品统计', null, 'goodsStatistics', '/admin/statistic.*', '0', ',7,64,', '2');
INSERT INTO `es_menu` VALUES ('65', '64', '价格销量', null, 'priceSales', '(/admin/shops.*)|(/admin/goods.*)|(/admin/statistics.*)', '0', ',7,64,65,', '3');
INSERT INTO `es_menu` VALUES ('66', '64', '热卖商品', '', 'hotGoods', '(/admin/shops.*)|(/admin/goods.*)|(/admin/statistics.*)', '0', ',7,64,66,', '3');
INSERT INTO `es_menu` VALUES ('67', '64', '销售明细', '', 'goodsSalesDetails', '(/admin/shops.*)|(/admin/goods.*)|(/admin/statistics.*)', '0', ',7,64,67,', '3');
INSERT INTO `es_menu` VALUES ('68', '64', '商品收藏', '', 'goodsCollect', '(/admin/shops.*)|(/admin/goods.*)|(/admin/statistics.*)', '0', ',7,64,68,', '3');
INSERT INTO `es_menu` VALUES ('69', '7', '行业分析', null, 'industryAnalysis', '/admin/statistic.*', '0', ',7,69,', '2');
INSERT INTO `es_menu` VALUES ('70', '69', '行业规模', null, 'industryScale', '(/admin/shops.*)|(/admin/statistics.*)', '0', ',7,69,70,', '3');
INSERT INTO `es_menu` VALUES ('71', '69', '概括总览', null, 'generalityOverview', '(/admin/shops.*)|(/admin/statistics.*)|(/admin/goods/categories.*)', '0', ',7,69,71,', '3');
INSERT INTO `es_menu` VALUES ('72', '7', '流量分析', null, 'trafficAnalysis', '/admin/statistics.*', '0', ',7,72,', '2');
INSERT INTO `es_menu` VALUES ('73', '72', '店铺流量', null, 'trafficAnalysisShop', '(/admin/shops.*)|(/admin/statistics.*)', '0', ',7,72,73,', '3');
INSERT INTO `es_menu` VALUES ('74', '72', '商品流量', null, 'trafficAnalysisGoods', '(/admin/shops.*)|(/admin/statistics.*)', '0', ',7,72,74,', '3');
INSERT INTO `es_menu` VALUES ('75', '7', '其它统计', null, 'otherStatistics', '/admin/statistics.*', '0', ',7,75,', '2');
INSERT INTO `es_menu` VALUES ('76', '75', '订单', null, 'orderStatistics', '(/admin/shops.*)|(/admin/statistics.*)', '0', ',7,75,76,', '3');
INSERT INTO `es_menu` VALUES ('77', '75', '销售收入', '', 'salesRevenueStatistics', '(/admin/shops.*)|(/admin/statistics.*)', '0', ',7,75,77,', '3');
INSERT INTO `es_menu` VALUES ('78', '75', '区域分析', null, 'regionalAnalysis', '(/admin/shops.*)|(/admin/statistics.*)', '0', ',7,75,78,', '3');
INSERT INTO `es_menu` VALUES ('79', '75', '客单价分布图', null, 'customerPriceDistribution', '(/admin/shops.*)|(/admin/goods/categories.*)|(/admin/statistics.*)', '0', ',7,75,79,', '3');
INSERT INTO `es_menu` VALUES ('80', '75', '退款', '', 'refundStatistics', '(/admin/shops.*)|(/admin/statistics/order/return.*)', '0', ',7,75,80,', '3');
INSERT INTO `es_menu` VALUES ('81', '8', '系统参数', '', 'shopSettings', '/admin/system.*', '0', ',8,81,', '2');
INSERT INTO `es_menu` VALUES ('82', '81', '系统设置', null, 'systemSettings', '(/admin/settings.*)|(/admin/goods.*)|(/admin/trade.*)', '0', ',8,81,82,', '3');
INSERT INTO `es_menu` VALUES ('84', '81', 'SMTP设置', null, 'smtpSettings', '/admin/systems/smtps.*', '0', ',8,81,84,', '3');
INSERT INTO `es_menu` VALUES ('85', '81', '短信网关', '', 'smsGatewaySettings', '/admin/systems/platforms.*', '0', ',8,81,85,', '3');
INSERT INTO `es_menu` VALUES ('86', '81', '快递平台', '', 'expressPlatformSettings', '/admin/systems/express-platforms.*', '0', ',8,81,86,', '3');
INSERT INTO `es_menu` VALUES ('87', '81', '验证平台', '', 'validatorPlatformSettings', '/admin/systems/validator.*', '0', ',8,81,87,', '3');
INSERT INTO `es_menu` VALUES ('88', '81', '电子面单', null, 'electronicrEceipt', '/admin/systems/waybills.*', '0', ',8,81,88,', '3');
INSERT INTO `es_menu` VALUES ('89', '81', '储存方案', null, 'storageSolution', '/admin/systems/uploaders.*', '0', ',8,81,89,', '3');
INSERT INTO `es_menu` VALUES ('91', '130', '商品索引', null, 'goodsIndex', '(/admin/task.*)|(/admin/goods/search.*)', '0', ',9,130,91,', '3');
INSERT INTO `es_menu` VALUES ('93', '8', '消息设置', null, 'messageSettings', '/admin/system.*', '0', ',8,93,', '2');
INSERT INTO `es_menu` VALUES ('94', '93', '店铺消息', null, 'shopMessage', '/admin/systems/message-templates.*', '0', ',8,93,94,', '3');
INSERT INTO `es_menu` VALUES ('95', '93', '会员消息', null, 'memberMessage', '/admin/systems/message-templates.*', '0', ',8,93,95,', '3');
INSERT INTO `es_menu` VALUES ('96', '93', '微信消息', null, 'wechatMessage', '/admin/systems/wechat-msg-tmp.*', '0', ',8,93,96,', '3');
INSERT INTO `es_menu` VALUES ('100', '8', '支付与物流', '', 'paymentAndDelivery', '(/admin/system.*)|(/regions.*)', '0', ',8,100,', '2');
INSERT INTO `es_menu` VALUES ('101', '100', '支付方式', null, 'paymentSettings', '(/base-api/uploaders.*)|(/admin/payment/payment-methods.*)', '0', ',8,100,101,', '3');
INSERT INTO `es_menu` VALUES ('102', '100', '物流公司', null, 'expressSettings', '/admin/systems/logi-companies.*', '0', ',8,100,102,', '3');
INSERT INTO `es_menu` VALUES ('103', '100', '信任登录', '', 'trustLogin', '/admin/members/connect.*', '0', ',8,100,103,', '3');
INSERT INTO `es_menu` VALUES ('104', '8', '权限管理', null, 'authSettings', '/admin/system.*', '-1', ',8,104,', '2');
INSERT INTO `es_menu` VALUES ('105', '104', '管理员管理', null, 'administratorManage', '(/uploaders.*)|(/admin/systems/manager/admin-users.*)|(/admin/systems/roles.*)', '-1', ',8,104,105,', '3');
INSERT INTO `es_menu` VALUES ('106', '104', '角色管理', null, 'roleManage', '(/admin/systems/roles.*)|(/admin/systems/menus.*)', '-1', ',8,104,106,', '3');
INSERT INTO `es_menu` VALUES ('107', '8', '搜索设置', null, 'searchSettings', '/admin/goodssearch.*', '-1', ',8,107,', '2');
INSERT INTO `es_menu` VALUES ('108', '107', '搜索分词', null, 'searchKeyword', '/admin/goodssearch.*', '-1', ',8,107,108,', '3');
INSERT INTO `es_menu` VALUES ('109', '107', '搜索历史', null, 'searchHistory', '/admin/goodssearch.*', '-1', ',8,107,109,', '3');
INSERT INTO `es_menu` VALUES ('110', '107', '搜索提示词', null, 'searchTips', '/admin/goodssearch.*', '-1', ',8,107,110,', '3');
INSERT INTO `es_menu` VALUES ('111', '9', '菜单管理', null, 'menuManage', '/admin/systems/menus.*', '-1', ',9,111,', '2');
INSERT INTO `es_menu` VALUES ('112', '9', '文章', '', 'articleManage', '(/admin/system.*)|(/admin/pages.*)', '0', ',9,112,', '2');
INSERT INTO `es_menu` VALUES ('113', '112', '文章列表', null, 'articleList', '(/admin/systems/admin-users.*)|(/admin/pages.*)', '0', ',9,112,113,', '3');
INSERT INTO `es_menu` VALUES ('114', '112', '文章分类', null, 'articleCategory', '/admin/pages/article-categories.*', '0', ',9,112,114,', '3');
INSERT INTO `es_menu` VALUES ('115', '9', '日志分析', null, 'logAnalysis', '/admin/services.*', '-1', ',9,115,', '2');
INSERT INTO `es_menu` VALUES ('116', '132', '日志列表', null, 'systemLogs', '/admin/system.*', '0', ',9,132,116,', '3');
INSERT INTO `es_menu` VALUES ('117', '115', '日志详情', null, 'logDetails', '/admin/services.*', '-1', ',9,115,117,', '3');
INSERT INTO `es_menu` VALUES ('118', '0', '分销', '', 'distribution', '/admin/distribution.*', '0', ',118,', '1');
INSERT INTO `es_menu` VALUES ('119', '118', '提成模板', null, 'extractTpl', '/admin/distribution.*', '0', ',118,119,', '2');
INSERT INTO `es_menu` VALUES ('120', '119', '个人提成模板', null, 'perAccomplishmentTpl', '/admin/distribution.*', '0', ',118,119,120,', '3');
INSERT INTO `es_menu` VALUES ('121', '119', '升级日志', null, 'upgradeLogs', '/admin/distribution.*', '0', ',118,119,121,', '3');
INSERT INTO `es_menu` VALUES ('122', '118', '个人分销商', null, 'distributorList', '/admin/distribution.*', '0', ',118,122,', '2');
INSERT INTO `es_menu` VALUES ('123', '118', '业绩列表', null, 'achievementList', '/admin/distribution.*', '0', ',118,123,', '2');
INSERT INTO `es_menu` VALUES ('124', '118', '提现', null, 'putforwardManage', '/admin/distribution.*', '0', ',118,124,', '2');
INSERT INTO `es_menu` VALUES ('125', '124', '提现设置', null, 'putforwardSettings', '/admin/distribution.*', '0', ',118,124,125,', '3');
INSERT INTO `es_menu` VALUES ('126', '124', '提现申请', null, 'putforwardApply', '/admin/distribution.*', '0', ',118,124,126,', '3');
INSERT INTO `es_menu` VALUES ('127', '81', '篡改记录', '', 'falsifyRecord', '/admin.*', '0', ',8,81,127,', '3');
INSERT INTO `es_menu` VALUES ('128', '8', '身份验证', '', 'systemSettingsVerification', '/admin.*', '0', ',8,128,', '2');
INSERT INTO `es_menu` VALUES ('129', '9', '装修', null, 'page', '/admin/pages.*', '0', ',9,129,', '2');
INSERT INTO `es_menu` VALUES ('130', '9', '维护', null, 'maintain', 'admin/systems.*', '0', ',9,130,', '2');
INSERT INTO `es_menu` VALUES ('131', '9', '搜索', null, 'searchSettings', '/admin/goodssearch.*', '0', ',9,131,', '2');
INSERT INTO `es_menu` VALUES ('132', '9', '权限', null, 'authSettings', 'admin/system.*', '0', ',9,132,', '2');
INSERT INTO `es_menu` VALUES ('134', '130', '热门关键字', null, 'hotKeyword', '/admin/pages/hot-keywords.*', '0', ',9,130,134,', '3');
INSERT INTO `es_menu` VALUES ('135', '130', '敏感词', null, 'sensitiveWords', '/admin/sensitive-words.*', '0', ',9,130,135,', '3');
INSERT INTO `es_menu` VALUES ('136', '130', '菜单管理', '', 'menuManage', '/admin/systems/menus.*', '0', ',9,130,136,', '3');
INSERT INTO `es_menu` VALUES ('137', '131', '搜索分词', null, 'searchKeyword', '/admin/goodssearch.*', '0', ',9,131,137,', '3');
INSERT INTO `es_menu` VALUES ('138', '131', '搜索历史', null, 'searchHistory', '/admin/goodssearch.*', '0', ',9,131,138,', '3');
INSERT INTO `es_menu` VALUES ('139', '131', '搜索提示词', null, 'searchTips', '/admin/goodssearch.*', '0', ',9,131,139,', '3');
INSERT INTO `es_menu` VALUES ('140', '132', '管理员', null, 'administratorManage', '(/uploaders.*)|(/admin/systems/manager/admin-users.*)|(/admin/systems/roles.*)', '0', ',9,132,140,', '3');
INSERT INTO `es_menu` VALUES ('141', '132', '角色', null, 'roleManage', '(/admin/systems/roles.*)|(/admin/systems/menus.*)', '0', ',9,132,141,', '3');
INSERT INTO `es_menu` VALUES ('142', '0', '售前售后', null, 'sale', '/admin/aftersale.*', '0', ',142,', '1');
INSERT INTO `es_menu` VALUES ('143', '142', '咨询', null, 'goodsAskList', '/admin/members/asks.*', '0', ',142,143,', '2');
INSERT INTO `es_menu` VALUES ('144', '142', '评价', null, 'goodsCommentList', '/admin/members/comments.*', '0', ',142,144,', '2');
INSERT INTO `es_menu` VALUES ('145', '142', '投诉', null, 'complaint', '(/admin/trade/order-complains.*)|(/admin/systems/complain-topics.*)', '0', ',142,145,', '2');
INSERT INTO `es_menu` VALUES ('146', '145', '投诉列表', null, 'complaintList', '/admin/trade/order-complains.*', '0', ',142,145,146,', '3');
INSERT INTO `es_menu` VALUES ('147', '145', '投诉主题', null, 'subjectList', '/admin/systems/complain-topics.*', '0', ',142,145,147,', '3');
INSERT INTO `es_menu` VALUES ('148', '142', '站内消息', null, 'notificationHistory', '/admin/systems/messages.*', '0', ',142,148,', '2');
INSERT INTO `es_menu` VALUES ('149', '129', '移动端微页面', null, 'mobilePages', '/admin/pages.*', '0', ',9,129,149,', '3');
INSERT INTO `es_menu` VALUES ('150', '129', 'pc端微页面', null, 'pcPages', '/admin/page.*', '0', ',9,129,150,', '3');
INSERT INTO `es_menu` VALUES ('151', '129', 'PC端导航菜单', null, 'pcSiteMenu', '/admin/pages/site-navigations.*', '0', ',9,129,151,', '3');
INSERT INTO `es_menu` VALUES ('152', '130', '区域管理', null, 'regionalManagement', '/regions.*', '0', ',9,130,152,', '3');


-- 更新商家端菜单初始数据 add by syw 2021-12-01
INSERT INTO `es_shop_menu` VALUES ('1', '0', '商品', 'goods', '/seller/goods.*', '0', ',1,', '1');
INSERT INTO `es_shop_menu` VALUES ('2', '0', '订单', 'order', '/seller.*', '0', ',2,', '1');
INSERT INTO `es_shop_menu` VALUES ('3', '0', '装修', 'shop', '/shop.*', '0', ',3,', '1');
INSERT INTO `es_shop_menu` VALUES ('4', '0', '促销', 'promotions', '/promotions*', '0', ',4,', '1');
INSERT INTO `es_shop_menu` VALUES ('5', '0', '统计', 'statistics', '/statistics*', '0', ',5,', '1');
INSERT INTO `es_shop_menu` VALUES ('6', '0', '售前售后', 'customer', '/customer*', '0', ',6,', '1');
INSERT INTO `es_shop_menu` VALUES ('7', '0', '设置', 'setting', '/setting*', '0', ',7,', '1');
INSERT INTO `es_shop_menu` VALUES ('8', '1', '商品列表', 'goodsList', '(/seller/goods.*)|(/seller/shops/cats.*)|(/seller/shops.*)|(/seller/promotion/exchange-cats.*)|(/seller/distribution/setting.*)', '0', ',1,8,', '2');
INSERT INTO `es_shop_menu` VALUES ('9', '1', '草稿箱', 'draftList', '(/seller/goods.*)|(/seller/shops/cats.*)|(/seller/shops.*)', '0', ',1,9,', '2');
INSERT INTO `es_shop_menu` VALUES ('10', '1', '分组管理', 'categoryManage', '/seller/shops/cats*', '0', ',1,10,', '2');
INSERT INTO `es_shop_menu` VALUES ('11', '1', '标签管理', 'tagManage', '/seller/goods*', '0', ',1,11,', '2');
INSERT INTO `es_shop_menu` VALUES ('12', '1', '回收站', 'recycleStation', '/seller/goods*', '0', ',1,12,', '2');
INSERT INTO `es_shop_menu` VALUES ('13', '1', '预警货品', 'understock', '/seller/goods*', '0', ',1,13,', '2');
INSERT INTO `es_shop_menu` VALUES ('14', '2', '订单列表', 'orderList', '(/seller/trade/orders.*)|(/seller/shops/logi-companies.*)|(/seller/express.*)', '0', ',2,14,', '2');
INSERT INTO `es_shop_menu` VALUES ('16', '7', '物流管理', 'logisticsManage', '(/seller/shops/ship-templates.*)|(/regions/depth.*)|(/seller/shops/logi-companies.*)', '0', ',7,16,', '2');
INSERT INTO `es_shop_menu` VALUES ('17', '2', '评价管理', 'commentsManage', '/seller/members/comments*', '0', ',2,17,', '2');
INSERT INTO `es_shop_menu` VALUES ('18', '2', '结算管理', 'settlementManage', '/seller/order/bills*', '0', ',2,18,', '2');
INSERT INTO `es_shop_menu` VALUES ('19', '2', '开票历史', 'receiptHistory', '/seller/members/receipts*', '0', ',2,19,', '2');
INSERT INTO `es_shop_menu` VALUES ('20', '3', 'PC端微页面', 'pcPages', '/seller*', '0', ',2,20,', '2');
INSERT INTO `es_shop_menu` VALUES ('21', '3', '移动端微页面', 'mobilePages', '/seller/shops*', '0', ',3,21,', '2');
INSERT INTO `es_shop_menu` VALUES ('23', '3', 'PC端店铺导航', 'shopNav', '/seller/shops/navigations.*', '0', ',3,23,', '2');
INSERT INTO `es_shop_menu` VALUES ('24', '4', '满减满赠', 'fullCut', '(/seller/promotion/full-discounts.*)|(/seller/promotion/coupons.*)|(/seller/promotion/full-discount-gifts/all)', '0', ',4,24,', '2');
INSERT INTO `es_shop_menu` VALUES ('25', '4', '单品立减', 'singleCut', '/seller/promotion/minus.*', '0', ',4,25,', '2');
INSERT INTO `es_shop_menu` VALUES ('26', '4', '第二件半价', 'secondHalfPrice', '/seller/promotion/half-prices.*', '0', ',4,26,', '2');
INSERT INTO `es_shop_menu` VALUES ('27', '4', '优惠券', 'discountManager', '/seller/promotion/coupons.*', '0', ',4,27,', '2');
INSERT INTO `es_shop_menu` VALUES ('28', '4', '赠品', 'giftManager', '/seller/promotion/full-discount-gifts.*', '0', ',4,28,', '2');
INSERT INTO `es_shop_menu` VALUES ('29', '4', '团购', 'groupBuyManager', '/seller/promotion/group-buy-goods.*', '0', ',4,29,', '2');
INSERT INTO `es_shop_menu` VALUES ('30', '4', '限时抢购', 'timeLimit', '/seller/promotion/seckill-applys.*', '0', ',4,30,', '2');
INSERT INTO `es_shop_menu` VALUES ('31', '5', '店铺概况', 'generalityOverview', '/seller/statistics/shop_profile.*', '0', ',5,31,', '2');
INSERT INTO `es_shop_menu` VALUES ('32', '5', '商品分析', 'goodsAnalysis', '/seller/statistics.*', '0', ',5,32,', '2');
INSERT INTO `es_shop_menu` VALUES ('33', '5', '运营报告', 'operateReport', '/operateReport.*', '0', ',5,33,', '2');
INSERT INTO `es_shop_menu` VALUES ('34', '5', '流量统计', 'trafficStatistics', '/seller/statistics/page_view/shop.*', '0', ',5,34,', '2');
INSERT INTO `es_shop_menu` VALUES ('35', '5', '收藏统计', 'collectStatistics', 'seller/statistics/collect.*', '0', ',5,35,', '2');
INSERT INTO `es_shop_menu` VALUES ('36', '32', '商品详情', 'goodsDetailsAnalysis', '(/seller/statistics/goods/goods_detail.*)|(/seller/goods/category/seller/children.*)', '0', ',5,32,36,', '3');
INSERT INTO `es_shop_menu` VALUES ('37', '32', '价格销量', 'goodsPriceSales', '(/seller/statistics/goods/price_sales.*)|(/seller/goods/category/seller/children.*)', '0', ',5,32,37,', '3');
INSERT INTO `es_shop_menu` VALUES ('38', '32', '热卖商品', 'hotSellingGoods', '(/seller/statistics/goods/order_price.*)|(/seller/statistics/goods/order_price_page.*)', '0', ',5,32,38,', '3');
INSERT INTO `es_shop_menu` VALUES ('39', '33', '区域分析', 'regionalAnalysis', '/seller/statistics/reports/regions.*', '0', ',5,33,39,', '3');
INSERT INTO `es_shop_menu` VALUES ('40', '33', '销售统计', 'salesStatistics', '/seller/statistics/reports.*', '0', ',5,33,40,', '3');
INSERT INTO `es_shop_menu` VALUES ('41', '33', '购买分析', 'buyAnalysis', '(/seller/statistics/reports/purchase/period.*)|(/seller/statistics/reports/purchase/ranges.*)', '0', ',5,33,41,', '3');
INSERT INTO `es_shop_menu` VALUES ('42', '6', '咨询', 'consultation', '/seller/members/asks.*', '0', ',6,42,', '2');
INSERT INTO `es_shop_menu` VALUES ('43', '6', '消息', 'message', '/seller/shops/shop-notice-logs.*', '0', ',6,43,', '2');
INSERT INTO `es_shop_menu` VALUES ('44', '7', '店铺设置', 'shopSettings', '/seller/shops.*', '0', ',7,44,', '2');
INSERT INTO `es_shop_menu` VALUES ('45', '7', '货品预警', 'goodsWarning', '/seller/shops.*', '0', ',7,45,', '2');
INSERT INTO `es_shop_menu` VALUES ('48', '7', '权限', 'shopAuth', '/shopAuth.*', '0', ',7,48,', '2');
INSERT INTO `es_shop_menu` VALUES ('49', '48', '店员', 'shopAssistant', '(/seller/shops/clerks.*)|(/seller/shops/roles.*)', '0', ',7,48,49,', '3');
INSERT INTO `es_shop_menu` VALUES ('50', '48', '角色', 'roleManage', '/seller/shops/roles.*', '0', ',7,48,50,', '3');
INSERT INTO `es_shop_menu` VALUES ('51', '4', '拼团', 'assembleManager', '/seller/promotion/pintuan.*', '0', ',4,51,', '2');
INSERT INTO `es_shop_menu` VALUES ('52', '7', '发票设置', 'invoiceSettings', '/seller/shops.*', '0', ',7,52', '2');
INSERT INTO `es_shop_menu` VALUES ('53', '1', '待审核商品', 'authGoodsList', '/seller/goods.*', '0', ',1,53', '2');
INSERT INTO `es_shop_menu` VALUES ('54', '6', '售后', 'afterSale', '/seller/after-sales.*', '0', ',6,54', '2');
INSERT INTO `es_shop_menu` VALUES ('55', '54', '售后服务单', 'serviceList', '/seller/after-sales.*', '0', ',6,54,55', '3');
INSERT INTO `es_shop_menu` VALUES ('56', '54', '退款单', 'refundList', '/seller/after-sales/refund.*', '0', ',6,54,56', '3');
INSERT INTO `es_shop_menu` VALUES ('57', '6', '交易投诉', 'complaintList', '/seller/trade/order-complains.*', '0', ',6,57,', '2');
INSERT INTO `es_shop_menu` VALUES ('58', '0', '在线客服', 'chat', '(/seller/trade/orders.*)|(/seller/goods.*)', '0', ',58,', '1');