/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.shop.trade.consumer;

import cn.shoptnt.client.trade.OrderOperateClient;
import cn.shoptnt.consumer.core.event.OrderStatusChangeEvent;
import cn.shoptnt.model.base.message.OrderStatusChangeMsg;
import cn.shoptnt.model.trade.order.dos.OrderDO;
import cn.shoptnt.model.trade.order.enums.OrderStatusEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 生成商品交易快照
 * @author Snow create in 2018/5/22
 * @version v2.0
 * @since v7.0.0
 */
@Component
public class SnapshotCreateConsumer implements OrderStatusChangeEvent {

    @Autowired
    private OrderOperateClient orderOperateClient;

    @Override
    public void orderChange(OrderStatusChangeMsg orderMessage) {
        //判断订单为新订单
        if(orderMessage.getNewStatus().equals(OrderStatusEnum.CONFIRM)){
            OrderDO orderDO = orderMessage.getOrderDO();

            this.orderOperateClient.addGoodsSnapshot(orderDO);
        }
    }


}
