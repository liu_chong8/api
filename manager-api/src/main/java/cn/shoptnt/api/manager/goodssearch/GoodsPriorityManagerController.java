/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.goodssearch;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.goods.dto.GoodsQueryParam;
import cn.shoptnt.model.goods.vo.GoodsPriorityVO;
import cn.shoptnt.service.goods.GoodsManager;
import cn.shoptnt.service.goods.GoodsQueryManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
* @author liuyulei
 * @version 1.0
 * @Description:  商品优先级相关API
 * @date 2019/6/10 14:49
 * @since v7.0
 */
@RestController
@RequestMapping("/admin/goodssearch/priority")
@Api(tags= "商品优先级相关API")
public class GoodsPriorityManagerController {


    @Autowired
    private GoodsQueryManager goodsQueryManager;

    @Autowired
    private GoodsManager goodsManager;

    @ApiOperation(value = "查询商品优先级列表", response = GoodsPriorityVO.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_no", value = "页码", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "query"),
            @ApiImplicitParam(name = "page_size", value = "每页显示数量", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "query"),
            @ApiImplicitParam(name = "keywords", value = "关键字", required = false, dataType = "String", dataTypeClass = String.class, paramType = "query")
    })
    @GetMapping
    public WebPage list(@ApiIgnore Long pageNo, @ApiIgnore Long pageSize, @ApiIgnore String keywords) {
        GoodsQueryParam goodsQueryParam = new GoodsQueryParam();
        goodsQueryParam.setPageNo(pageNo);
        goodsQueryParam.setPageSize(pageSize);
        goodsQueryParam.setKeyword(keywords);
        return this.goodsQueryManager.goodsPriorityList(goodsQueryParam);
    }

    @ApiOperation(value = "修改商品优先级")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "goods_id", value = "商品id", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "query"),
            @ApiImplicitParam(name = "priority", value = "优先级", required = true, dataType = "int", dataTypeClass = Integer.class, paramType = "query",allowableValues = "1,2,3",
            example = "高(3),中(2),低(1)")
    })
    @PutMapping
    public void updatePriority(@ApiIgnore Long goodsId, @ApiIgnore Integer priority) {
        this.goodsManager.updatePriority(goodsId,priority);
    }

}
