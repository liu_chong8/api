/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.receiver;

import java.util.List;

import cn.shoptnt.consumer.core.event.ShopCollectionEvent;
import cn.shoptnt.model.base.rabbitmq.AmqpExchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * 店铺变更消费者
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018年3月23日 上午10:32:08
 */
@Component
public class ShopCollectionReceiver {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired(required=false)
	private List<ShopCollectionEvent> events;
	/**
	 * 店铺变更
	 *
	 * @param shopId
	 */
	@RabbitListener(bindings = @QueueBinding(
			value = @Queue(value = AmqpExchange.SHOP_CHANGE_REGISTER + "_QUEUE"),
			exchange = @Exchange(value = AmqpExchange.SHOP_CHANGE_REGISTER, type = ExchangeTypes.FANOUT)
	))
	public void shopChange(Long shopId) {

		if (events != null) {
			for (ShopCollectionEvent event : events) {
				try {
					event.shopChange(shopId);
				} catch (Exception e) {
					logger.error("店铺变更消息出错", e);
				}
			}
		}

	}

}
